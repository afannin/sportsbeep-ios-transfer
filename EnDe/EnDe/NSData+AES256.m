//
//  NSData+AES256.m
//  EnDe
//
//  Created by Demo on 14/08/15.
//  Copyright (c) 2015 TO. All rights reserved.
//

#import "NSData+AES256.h"


#import <CommonCrypto/CommonCryptor.h>

@implementation NSData (AES256)

- (NSData *)AES256EncryptedDataWithKey:(NSString *)key
{
    return [self AES256EncryptedDataWithKey:key iv:nil];
}

- (NSData *)AES256DecryptedDataWithKey:(NSString *)key
{
    return [self AES256DecryptedDataWithKey:key iv:nil];
}

- (NSData *)AES256EncryptedDataWithKey:(NSString *)key iv:(NSString *)iv
{
    return [self AES256Operation:kCCEncrypt key:key iv:iv];
}

- (NSData *)AES256DecryptedDataWithKey:(NSString *)key iv:(NSString *)iv
{
    return [self AES256Operation:kCCDecrypt key:key iv:iv];
}

- (NSData *)AES256Operation:(CCOperation)operation key:(NSString *)key iv:(NSString *)iv
{
    char keyPtr[kCCKeySizeAES128 + 1];
    bzero(keyPtr, sizeof(keyPtr));
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    
    char ivPtr[kCCBlockSizeAES128 + 1];
    bzero(ivPtr, sizeof(ivPtr));
    if (iv) {
        [iv getCString:ivPtr maxLength:sizeof(ivPtr) encoding:NSUTF8StringEncoding];
    }
    
    NSUInteger dataLength = [self length];
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(operation,
                                          kCCAlgorithmAES128,
                                          kCCOptionPKCS7Padding | kCCOptionECBMode,
                                          keyPtr,
                                          kCCBlockSizeAES128,
                                          ivPtr,
                                          [self bytes],
                                          dataLength,
                                          buffer,
                                          bufferSize,
                                          &numBytesEncrypted);
    if (cryptStatus == kCCSuccess) {
        return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
    }
    free(buffer);
    return nil;
}

@end