//
//  NSData+AES256.h
//  EnDe
//
//  Created by Demo on 14/08/15.
//  Copyright (c) 2015 TO. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSData (AES256)
- (NSData *)AES256EncryptedDataWithKey:(NSString *)key;
- (NSData *)AES256DecryptedDataWithKey:(NSString *)key;
- (NSData *)AES256EncryptedDataWithKey:(NSString *)key iv:(NSString *)iv;
- (NSData *)AES256DecryptedDataWithKey:(NSString *)key iv:(NSString *)iv;
@end
