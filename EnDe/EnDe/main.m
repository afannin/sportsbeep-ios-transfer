//
//  main.m
//  EnDe
//
//  Created by Demo on 14/08/15.
//  Copyright (c) 2015 TO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSData+AES256.h"
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSString *plainText = @"Hello World";
        NSData *plainData = [plainText dataUsingEncoding:NSUTF8StringEncoding];
        NSData *encryptedData = [plainData AES256EncryptedDataWithKey:@"key1"];
        
        

        NSData *decodedData = [encryptedData AES256DecryptedDataWithKey:@"key1"];
        NSString *decodedString = [[NSString alloc] initWithData:decodedData  encoding:NSUTF8StringEncoding];
        NSLog(@"%@\n",decodedString);
        
        plainText = @"****How are you*****";
        plainData= [plainText dataUsingEncoding:NSUTF8StringEncoding];
        encryptedData = [plainData AES256EncryptedDataWithKey:@"itsalongkey" iv:@"itsalongiv"];
        
        
        
        decodedData = [encryptedData AES256DecryptedDataWithKey:@"itsalongkey" iv:@"itsalongiv"];
        decodedString = [[NSString alloc] initWithData:decodedData  encoding:NSUTF8StringEncoding];
        NSLog(@"%@\n",decodedString);
        
        
    }
    return 0;
}


