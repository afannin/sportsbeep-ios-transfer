//
//  sports.m
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/1/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import "sports.h"

@implementation sports





-(NSString *)getSportsType:(NSInteger)type
{
    NSArray *types = [[NSArray alloc]init];
    
    types = @[@"college",@"professional"];
    
    return [types objectAtIndex:type];
    
}



-(NSString *)getSport:(NSInteger)sport
{
    NSArray *types = [[NSArray alloc]init];
    
    types = @[@"mlb",@"nfl",@"hockey"];
    
    return [types objectAtIndex:sport];
    
    
}


-(NSString *)getIsUpcomingGames:(NSInteger)isFuture
{
    
    NSArray *gameTimeTypes = [[NSArray alloc]init];
    
    gameTimeTypes = @[@"upcoming",@"abcd"];
    
    
    return [gameTimeTypes objectAtIndex:isFuture];
}



@end
