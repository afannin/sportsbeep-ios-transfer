//
//  liveContestsViewController.m
//  SportsBeep
//
//  Created by iOSDev on 5/22/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "liveContestsViewController.h"
#import "individualGame.h"
#import "specificContestViewController.h"

@interface liveContestsViewController (){
    getMyContests *getContests;
    NSMutableArray *contestsHolder;
    CGFloat cellWidth;
}

@end

@implementation liveContestsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    
    contestsHolder = [[NSMutableArray alloc]init];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"contestsCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    
    self.tableView.separatorColor = [UIColor clearColor];
    cellWidth = self.view.frame.size.width;

    [self showHud];
    [self getLiveContests];
    
}

-(void)viewDidAppear:(BOOL)animated
{

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)getLiveContests
{
    NSString *locStatus = self.gameStatus;
    getContests = [[getMyContests alloc]init];
    [getContests setDelegate:self];
    [getContests getContests:locStatus];

    
}

//get contests protocol methods

-(void)getContestsHasStarted
{
    //progress spinner
}
-(void)getContestsDidSucceed:(NSDictionary*)result
{
    //end spinner
    
    if(self.position >1)
    {
        NSLog(@"%@ the contests",result);
    }
    
    NSArray *contests = [result objectForKey:@"contests"];
    
    for(NSDictionary *dict in contests)
    {
    
        
      
        
        individualGame   * g = [[individualGame alloc]init];
   
        
        NSDictionary *game = [dict objectForKey:@"game"];
        
        g.homeTeamObject = [game objectForKey:@"home_team"];
        g.awayTeamObject = [game objectForKey:@"away_team"];
        
        NSDictionary *tik = [dict objectForKey:@"ticket"];
        
 
        g.pool = [game objectForKey:@"pool"];
        g.ticket = [game objectForKey:@"ticket"];
        g.date = [game objectForKey:@"start_time"];
        g.gameId = [game objectForKey:@"id"];
        g.contestID = [dict objectForKey:@"id"];
        
        
        g.placingString = [self addTopString: [tik objectForKey:@"placing"]  : [tik objectForKey:@"prize_pool"] : [tik objectForKey:@"score"]];
        
        NSString *myDate = [game objectForKey:@"start_time"];
        
        NSDateFormatter *dateFormatter  =   [[NSDateFormatter alloc]init];
        
        NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [dateFormatter setTimeZone:gmt];
        
        
        dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
        NSDate *date = [dateFormatter dateFromString:myDate];
        
        NSDate *yourDate   =  [dateFormatter dateFromString:myDate];
        
        if(yourDate != nil)
        {
            [g changeDateToRightDate:date];
            
        }
        
        [contestsHolder addObject:g];
    
        
        
        
    }
    
    if([contestsHolder count] == 0)
    {
        UILabel *l = [[UILabel alloc]initWithFrame:self.tableView.bounds];
        [self.tableView addSubview:l];
        [l setText:@"No Contests"];
        [l setTextColor:[UIColor whiteColor]];
        [l setTextAlignment:NSTextAlignmentCenter];
    }
    
    [self.tableView reloadData];
    [self hideHud];
}
-(void)getContestsDidFail:(NSString *)error
{
    //end spinner
}



//end




//tbale view delegatges

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [contestsHolder count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    

    individualGame  * g = [contestsHolder objectAtIndex:indexPath.row];


    NSString *CellIdentifier = @"cell";

    contestsCell *cell =  (contestsCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
         cell = [[contestsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
    }
    else
    {
      //  [cell changeTriangleColors:g.awayTeamObject :g.homeTeamObject :g.gameDay :g.gameTime];
    }
    
       [cell addGameTriangles:g.awayTeamObject :g.homeTeamObject:cellWidth:g.gameTime:g.gameDay];
    
    
    [cell.bottomView setAttributedText:g.placingString];
    
    
    NSString *place = [g.ticket objectForKey:@"placing"];
    NSString *prize_pool = [g.ticket objectForKey:@"prize_pool"];
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    individualGame *g = [contestsHolder objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.delegate contestWasSelected:g];
    
}


-(void)showHud
{
    KVNProgressConfiguration *configuration = [[KVNProgressConfiguration alloc] init];
    
    
    //  configuration.statusColor = [UIColor whiteColor];
    configuration.statusFont = [UIFont fontWithName:@"HelveticaNeue-Thin" size:15.0f];
    configuration.circleStrokeForegroundColor = [UIColor darkGrayColor];
    configuration.circleStrokeBackgroundColor = [UIColor clearColor];
    configuration.circleFillBackgroundColor = [UIColor clearColor];
    configuration.backgroundFillColor = [UIColor clearColor];
    configuration.backgroundTintColor = [UIColor clearColor];
    configuration.successColor = [UIColor whiteColor];
    configuration.errorColor = [UIColor whiteColor];
    configuration.circleSize = 50.0f;
    configuration.lineWidth = 2.0f;
    configuration.fullScreen = NO;
    
    
    configuration.tapBlock = ^(KVNProgress *progressView) {
        // Do something you want to do when the user tap on the HUD
        // Does nothing by default
    };
    
    // You can allow user interaction for behind views but you will losse the tapBlock functionnality just above
    // Does not work with fullscreen mode
    // Default is NO
    configuration.allowUserInteraction = NO;
    
    [KVNProgress setConfiguration:configuration];
    [KVNProgress showWithStatus:@""
                         onView:self.view];
    
}

-(void)hideHud
{
    [KVNProgress dismiss];
    
}




-(NSAttributedString *)addTopString:(NSString *)place:(NSString *)pool:(NSString *)score
{
    CGFloat locScore = [score floatValue];
  
    //  [l setText:[NSString stringWithFormat:@"%@ place in a %@ pool with  %.02f POINTS",place,pool,locScore]];
    //  [l setTextColor:[UIColor whiteColor]];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]init];
    [string appendAttributedString:[self placeString:[NSString stringWithFormat:@"%@",place]:[NSString stringWithFormat:@"%@",pool] ]];
    [string appendAttributedString:[self pointsString:[NSString stringWithFormat:@"%@",score]]];
    
    
    
    return  string;

}



-(NSAttributedString *)pointsString:(NSString *)points
{
    
    
    
    
    NSString *fullString = [NSString stringWithFormat:@" %@ POINTS",points];
    
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:fullString];
    [hogan addAttribute:NSFontAttributeName
                  value: [UIFont fontWithName:@"BebasNeueBold" size:20.0]
                  range:NSMakeRange(0,fullString.length)];
    
    [hogan addAttribute:NSForegroundColorAttributeName value:GREEN range:NSMakeRange(0, fullString.length)];
    
    
    
    return hogan;
    
    
}


-(NSAttributedString *)placeString:(NSString*)place:(NSString *)pool
{
    
    
    NSString *suffix = @"";
    
    if([place isEqualToString:@"1"])
    {
        suffix = @"st";
    }
    else if([place isEqualToString:@"2"])
    {
        suffix = @"nd";
    }
    else if([place isEqualToString:@"3"])
    {
        suffix = @"rd";
    }
    else
    {
        suffix = @"th";
    }
    
    
    NSString *fullString = [NSString stringWithFormat:@"%@%@ place in a %@ pool with",place,suffix,[self getPool:pool]];
    
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:fullString];
    [hogan addAttribute:NSFontAttributeName
                  value:[UIFont systemFontOfSize:14]
                  range:NSMakeRange(0, fullString.length)];
    [hogan addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, fullString.length)];
    
    
    return hogan;
}



-(NSString *)getPool:(NSString *)pool
{
    
    NSArray *pools = [[NSArray alloc]init];
    
    pools = @[@"Free Pool",@"10 Dollar Pool",@"20 Dollar Pool",@"50 Dollar Pool",@"100 Dollar Pool",@"250 Dollar Pool",@"500 Dollar Pool"];
    
    
    return [pools objectAtIndex:[pool intValue]];
    
    
}

 

@end
