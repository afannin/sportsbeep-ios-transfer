//
//  MyContestsToggleProtocol.h
//  SportsBeep
//
//  Created by Rafi Chehirian on 6/23/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MyContestsToggleProtocol <NSObject>
-(void)buttonWasSelected:(NSInteger)index;
@end
