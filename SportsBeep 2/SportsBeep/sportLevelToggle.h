//
//  sportLevelToggle.h
//  SportsBeep
//
//  Created by iOSDev on 5/21/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface sportLevelToggle : UIView

@property(nonatomic,strong)UIButton *selectedButton;
@property(nonatomic,assign)NSInteger selectedIndex;
@property(nonatomic,strong)NSMutableArray *toggleButtons;
@end
