//
//  getPlayersForGame.m
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/26/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import "getPlayersForGame.h"

@implementation getPlayersForGame



-(void)getPlayers :(NSString*)gameId :(NSString*)sport
{
    
    [self.delegate getPlayersHasStarted];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults stringForKey:@"authentication_token"];
    NSString *tokenString = [NSString stringWithFormat:@"Token token=%@", token];
    
    
    NSString *url = [NSString stringWithFormat:@"http://sportsbeep.com/api/mlb/games/%@/players",gameId];
    
    NSDictionary *dict =@{
                          //  @"status":[NSString stringWithFormat:self.gameTimeType],
                          @"game_id":gameId,
                          };
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:tokenString forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer = requestSerializer;
    
    [manager GET:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
     //   NSLog(@"this is the respoonse %@",responseObject);
        [self.delegate getPlayersSuccess:responseObject];
        
        
        
        
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorMessage = operation.responseObject[@"error"];
        
        
        
        [self.delegate getPLayersHasFailed:errorMessage];
    }];
    
    
}

@end