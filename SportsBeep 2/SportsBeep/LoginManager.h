//
//  LoginManager.h
//  h6
//
//  Created by Daniel Nasello on 10/9/14.
//  Copyright (c) 2014 Combustion Innovation Group. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "SocialLoginDelegate.h"
#import "MBProgressHud.h"


@interface LoginManager : NSObject {id<SocialLoginDelegate>delegate;}
@property (nonatomic,weak)id delegate;
-(void)login:(NSString*)username withPassword:(NSString *)password;
-(void)logout;
-(void)forgotPassword:(NSString*)username;
-(void)forgotPasswordError: (NSString*) errorMessage;
-(void)forgotPasswordSuccess;
-(void)createAccount :(NSString *)username withEmail:(NSString *)email withPassword:(NSString *)password;


@end
