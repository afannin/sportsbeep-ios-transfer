//
//  liveContestsProtocol.h
//  SportsBeep
//
//  Created by Daniel Nasello on 6/1/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//


#import <Foundation/Foundation.h>
@class individualGame;
@protocol liveContestsProtocol <NSObject>

-(void)contestWasSelected:(individualGame *)g;
@end
