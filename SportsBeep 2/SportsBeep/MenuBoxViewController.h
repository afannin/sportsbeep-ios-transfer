//
//  MenuBoxViewController.h
//  SportsBeep
//
//  Created by iOSDev on 3/30/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuBoxViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *menuBoxButton;
@property (weak, nonatomic) IBOutlet UIButton *pickGameButton;
@property (weak, nonatomic) IBOutlet UIButton *myContestsButton;
@property (weak, nonatomic) IBOutlet UIButton *changeSportButton;
@property (weak, nonatomic) IBOutlet UIButton *profileButton;
@property (weak, nonatomic) IBOutlet UIButton *howToPlayButton;
@property (weak, nonatomic) IBOutlet UIButton *logOutButton;

@end
