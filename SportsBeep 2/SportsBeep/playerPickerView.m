//
//  playerPickerView.m
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/27/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//


#import "playerPickerView.h"
#import "playerPlayerView.h"
#import "gamePlayer.h"
#import "Colors.h"

@implementation playerPickerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.playerViews = [[NSMutableArray alloc]init];
        [self createLinear];
        
        
    }
    return self;
}





-(void)createLinear
{
    self.pHolders = [[NSMutableArray alloc]init];
    self.linearLayout = [[CSLinearLayoutView alloc]initWithFrame:CGRectMake(0, 85, self.frame.size.width, self.frame.size.height-85)];
    [self addSubview:self.linearLayout];
    
    [self setBackgroundColor:[UIColor clearColor]];
    [self.linearLayout setBackgroundColor:[UIColor clearColor]];
}

-(void)createHeader:(NSString *)header
{
    
    UILabel *l = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, self.frame.size.width, 50)];
    [l setBackgroundColor:DARK_BLUE];
    [l setTextColor:[UIColor whiteColor]];
    [l  setFont: [UIFont fontWithName:@"BebasNeueBold" size:26.0]];
    [l  setTextAlignment:NSTextAlignmentCenter];
    [l setText:header];
    [self addSubview:l];
    
}


-(void)addPlayers:(NSMutableArray *)players
{
    int ind=0;
    for(gamePlayer *d in players)
    {
        //UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0,299, 50)];
        
        playerPlayerView *v = [[playerPlayerView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 50)];
        
        NSDictionary *to = d.team_object;
        
        NSString *mainColor = [to objectForKey:@"primary_color"];
        NSString *altColor = [to objectForKey:@"secondary_color"];
        
         v.playerId = [NSString stringWithFormat:@"%@",d.player_id ];
        [v.playerNumber setText:[NSString stringWithFormat:@"%@",d.player_number]];
        [v.playerName setText:d.player_name ];
        [v.playerPosition setText:d.player_position];
        [v.playerTeamAbbr setText:d.team_abbr];
        
        [v setStats:d.stats];
        
        v.gPlayer = d;
        [v setBG:mainColor];
        [v setLabelColors:altColor];
        v.tag = ind;
        
        [self.playerViews addObject:v];
        
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(playerWasTapped:)];
        [v addGestureRecognizer:singleFingerTap];
        
        [self.linearLayout setBackgroundColor:[UIColor clearColor]];
        CSLinearLayoutItem *i = [CSLinearLayoutItem layoutItemForView:v];
        [self.linearLayout addItem:i];
        
        ind++;
    }
    
}




-(void)playerWasTapped:(UITapGestureRecognizer *)r
{
    
    NSLog(@"tapped");
    
    
    
    int tag = r.view.tag;
    
    playerPlayerView *v = [self.playerViews objectAtIndex:tag];
    
    NSLog(@"%@",v);
    
    
    [self.delegate playerPositionWasChosen:self:v:self.tag:self.positionView];
    
    
    NSLog(self.positionView);
    
}





@end
