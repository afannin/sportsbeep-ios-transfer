//
//  thirdVC.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 4/17/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "childVcDelegate.h"
#import "individualGame.h"
#import "CSLinearLayoutView.h"

@interface thirdVC : UIViewController
{
    id<childVcDelegate>delegate;
}
-(void)replaceLinear:(UIView *)v :(int)pos;
@property(nonatomic,strong) individualGame *iGame;
@property(nonatomic,weak)id delegate;
@property(nonatomic,strong)CSLinearLayoutView *linearLayout;
@property(nonatomic,strong)UIButton *submitTeam;
@property(nonatomic,assign)BOOL buttonShouldBeEnabled;
@property (weak, nonatomic) IBOutlet UILabel *thirdML;
@end
