//
//  PickPoolViewController.h
//  SportsBeep
//
//  Created by iOSDev on 4/3/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuBox.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "individualGame.h"
#import "AFHTTPRequestOperationManager.h"
@interface PickPoolViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *pickPoolHeadLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickPoolTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyTextLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *pickPoolScrollView;

@property (weak, nonatomic) IBOutlet UIView *menuBoxContainerView;
@property (strong, nonatomic)  UIButton *cancelBtn;
@property (strong, nonatomic)  UIButton *yesBtn;

@property (strong, nonatomic)  UIView *cover;
@property (strong, nonatomic)  UIView *poolConfirmBox;

@property(nonatomic,strong)MenuBox *mSlideUpMenu;


@property(nonatomic,strong)NSString *game_id;
@property(nonatomic,strong)NSMutableArray *playersPickedForPool;

@property(nonatomic,strong)NSDictionary *homeTeamObject;
@property(nonatomic,strong)NSDictionary *awayTeamObject;

@property(nonatomic,strong)individualGame *indieGame;

-(void)getCurrentPoolsForGame:(NSString *)game;
-(void)initializeView;

@end
 