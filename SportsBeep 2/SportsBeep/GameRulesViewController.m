//
//  GameRulesViewController.m
//  SportsBeep
//
//  Created by iOSDev on 3/27/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "GameRulesViewController.h"
#import "Colors.h"

@interface GameRulesViewController (){
    
}

@end

@implementation GameRulesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // close rules button
    self.closeRulesButton.backgroundColor = PANEL_BG_BLUE;
    
    // TODO - close rules
    
    
    // the webview
    NSURL *url = [NSURL URLWithString: @"http://www.sportsbeep.com/mobile/pages/how_it_works"];
    NSURLRequest *requestURL = [NSURLRequest requestWithURL:url];
    self.rulesWebView.delegate =self;
    [self.rulesWebView loadRequest:requestURL];
    
    
 
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)pressedCloseButton:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end
