//
//  playerPickerView.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/27/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "playerPositionViewDelegate.h"
#import "CSLinearLayoutView.h"
#import "playerPlayerView.h"
@interface playerPickerView : UIView{
    id<playerPositionViewDelegate>delegate;
}
-(void)setPlayers:(NSArray*)players;
@property(nonatomic,strong)NSString *positionView;
@property(nonatomic,weak)id delegate;
@property(nonatomic,strong)CSLinearLayoutView *linearLayout;
@property(nonatomic,strong)NSMutableArray *pHolders;
-(void)createHeader:(NSString *)header;
-(void)addPlayers:(NSMutableArray *)players;


@property(nonatomic,strong)NSDictionary *teamObj;
@property(nonatomic,strong)NSMutableArray *playerViews;
@end
