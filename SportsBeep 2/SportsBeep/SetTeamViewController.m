//
//  SetTeamViewController.m
//  SportsBeep
//
//  Created by iOSDev on 4/3/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "SetTeamViewController.h"
#import "Colors.h"

@interface SetTeamViewController ()

@end

@implementation SetTeamViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // menu box
    self.menuBox.backgroundColor = GREEN;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
