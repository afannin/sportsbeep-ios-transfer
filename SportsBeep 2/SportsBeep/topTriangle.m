//
//  topTriangle.m
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/1/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import "topTriangle.h"

@implementation topTriangle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self clip];
    }
    return self;
}


-(void)addLabels:(NSString *)state:(NSString *)team
{
    self.subLabel = [[UILabel alloc]initWithFrame:CGRectMake(8,2, self.frame.size.width, 14)];
    [self.subLabel setTextColor:self.textColor];
    [self.subLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:14.0]];
    [self.subLabel setText:state];
    [self addSubview:self.subLabel];
    
    self.mainLabel = [[UILabel alloc]initWithFrame:CGRectMake(8,15, self.frame.size.width, 18)];
    [self.mainLabel setTextColor:self.textColor];
    [self.mainLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:20.0]];
    [self.mainLabel setText:team];
    [self addSubview:self.mainLabel];
    
}


-(void)changeLabelText:(NSString *)state:(NSString *)team
{
    [self.subLabel setText:state];
    [self.mainLabel setText:team];
    
    [self.subLabel setTextColor:self.textColor];
    [self.mainLabel setTextColor:self.textColor];
}

-(void)clip
{
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    
    // Build a triangular path
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){0, 0}];
    [path addLineToPoint:(CGPoint){width,0}];
    [path addLineToPoint:(CGPoint){0,height}];
    
    
    [path closePath];
    
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask = [CAShapeLayer new];
    mask.frame = self.bounds;
    mask.path = path.CGPath;
    [mask setBackgroundColor:self.backgroundColor.CGColor];
    
    // Mask the imageView's layer with this shape
    self.layer.mask = mask;
    
    
    /*
     UIBezierPath *path = [UIBezierPath new];
     [path moveToPoint:(CGPoint){0, 0}];
     [path addLineToPoint:(CGPoint){width,0}];
     [path addLineToPoint:(CGPoint){0,height}];
     [path addLineToPoint:(CGPoint){0, height}];
     */
}

@end
