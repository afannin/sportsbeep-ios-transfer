//
//  ToastMessage.m
//  SportsBeep
//
//  Created by iOSDev on 4/10/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "ToastMessage.h"

@implementation ToastMessage


// constructor
// receives: NSString* aErrorMessage - the error message to be displayed
//           UIView* aRequestingView - the view that the message will be displayed onto
// returns: void
- (id)initWithMessage: (NSString*)aErrorMessage andWithRequestingView: (UIView*)aRequestingView;
{
    self = [super init];
    if (self)
    {
        // initialize properties
        self.mErrorMessage = aErrorMessage;
        self.mRequestingView = aRequestingView;
        
    }
    return self;
}

// method displays the toast error message
// receives: no arguments
// returns: void
-(void)displayToastMessage {
    self.hud = [MBProgressHUD showHUDAddedTo:self.mRequestingView animated:YES];
    self.hud.color = DARK_BLUE;
    self.hud.detailsLabelText = self.mErrorMessage;
    self.hud.mode = MBProgressHUDModeText;
    [self.hud hide:YES afterDelay:3];
    self.hud.yOffset = 0;
    self.hud.xOffset = 0;
    self.hud.removeFromSuperViewOnHide = YES;
}

@end
