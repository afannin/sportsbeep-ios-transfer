//
//  gameInfoHolder.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/1/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "bottomTriangle.h"
#import "topTriangle.h"
#import "vsCircle.h"
#import "UIColor+Expanded.h"

@interface gameInfoHolder : UIView


@property(nonatomic,strong)bottomTriangle *bottomT ;
@property(nonatomic,strong)topTriangle *topT ;
@property(nonatomic,strong)UILabel *dateLabel;
@property(nonatomic,strong)UILabel *timeLabel;
@property(nonatomic,strong)UIImageView *im;

-(void)addGameTriangles :(NSDictionary*)homeTeam :(NSDictionary *)awayTeam:(CGFloat)cellWidth:(NSString *)date:(NSString*)time;
-(void)changeTriangleColors:(NSDictionary*)homeTeam :(NSDictionary *)awayTeam:(NSString *)date:(NSString*)time;

@end

