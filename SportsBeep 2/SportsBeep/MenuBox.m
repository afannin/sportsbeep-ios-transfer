//
//  MenuBox.m
//  SportsBeep
//
//  Created by iOSDev on 5/29/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "MenuBox.h"
#import "Colors.h"


@implementation MenuBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.buttonArray = [[NSMutableArray alloc]init];
        
        UIColor *co = [[UIColor alloc]initWithCGColor:[UIColor colorWithRed:0.302 green:0.361 blue:0.49 alpha:1].CGColor];
        
        //if the toggle is open
        self.isOpen = NO;
        self.backgroundColor =  [UIColor colorWithRed:0.18 green:0.369 blue:0.651 alpha:1] ;
        self.highlightedColor =co;
        self.disabledColor = [UIColor darkGrayColor];
        self.selectedColor = [UIColor purpleColor];
        self.canClick = YES;
        
        self.originalFrame = frame;
        
    }
    return self;
}


-(void)addButtons
{
    
    //I know how many buttons there are based on the number of labels.
    //now i can split the size equally in the view
    
    
    //width of the button
    CGFloat lWidthOfButton = self.frame.size.width;
    //height of the button should be size of frame... so if you want button height to change, change frame hieght
    CGFloat lHeightButton = 60.0f;
    //set tags + offset for button position
    int i = 0;
    UIColor *co = [[UIColor alloc]initWithCGColor:[UIColor colorWithRed:0.302 green:0.361 blue:0.49 alpha:1].CGColor];
    //add the back button
    self.mdragButton= [DragButton buttonWithType:UIButtonTypeCustom];
    self.mdragButton.titleLabel.font = [UIFont fontWithName:@"BebasNeueBold" size:30.0];
    [self.mdragButton setFrame:CGRectMake(0, 0, lWidthOfButton, lHeightButton)];
    [self.mdragButton setBackgroundImage:[self imageWithColor: [UIColor colorWithRed:0.525 green:0.671 blue:0.4 alpha:1]] forState:UIControlStateNormal];
    [self.mdragButton setBackgroundImage:[self imageWithColor:co] forState:UIControlStateHighlighted];
    [self.mdragButton setBackgroundImage:[self imageWithColor:self.selectedColor] forState:UIControlStateSelected];
    [self.mdragButton setBackgroundImage:[self imageWithColor:self.disabledColor] forState:UIControlStateDisabled];
    [self.mdragButton setTitle:@"MENU BOX" forState:UIControlStateNormal];
    [self.mdragButton setTag:i];
    [self.mdragButton setImage: [self scaleAndRotateImage:[UIImage imageNamed:@"menu"]] forState:UIControlStateNormal];
    [self.mdragButton addTarget:self action:@selector(cancelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonArray addObject:self.mdragButton];
    [self.mdragButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [self.mdragButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 110.0f, 0.0f, 0.0f)];
    [self.mdragButton setImageEdgeInsets:UIEdgeInsetsMake(0.0f, 115.0f, 15.0f, 0.0f)];
    [self.mdragButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.mdragButton setDelegate:self];
    [self addSubview:self.mdragButton];
    [self.mdragButton addRecognizer];
    
    //add the rest of the buttons
    
    NSArray *icons = [[NSArray alloc]init];
    icons = @[
              @"calendar",
              @"chart",
              @"star",
              @"user",
              @"questionMark",
              @"share",
              @"gear",
         
              
              ];
    for(NSString *string in self.buttonTitles)
    {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(0, lHeightButton * (i+1), lWidthOfButton, lHeightButton)];
        [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        button.titleLabel.font = [UIFont fontWithName:@"BebasNeueBold" size:25.0];
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 15.0f, 0.0f, 0.0f)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(0.0f, 10.0f, 0.0f, 0.0f)];
        [button.imageView setContentMode:UIViewContentModeCenter];
        [button.imageView setFrame:CGRectMake(button.imageView.frame.origin.x, button.imageView.frame.origin.y, 20, self.frame.size.height)];
        [button setBackgroundImage:[self imageWithColor:PANEL_BG_BLUE] forState:UIControlStateNormal];
        [button setBackgroundImage:[self imageWithColor:co] forState:UIControlStateHighlighted];
        [button setBackgroundImage:[self imageWithColor:self.selectedColor] forState:UIControlStateSelected];
        [button setBackgroundImage:[self imageWithColor:self.disabledColor] forState:UIControlStateDisabled];
        [button setTitle:string forState:UIControlStateNormal];
        [button setTag:i+1];
        [button setImage:[self scaleAndRotateImage:[UIImage imageNamed:[icons objectAtIndex:i]]] forState:UIControlStateNormal];
    
        [button addTarget:self action:@selector(actionButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.buttonArray addObject:button];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, button.frame.size.width, 1)];
        lineView.backgroundColor = [UIColor blackColor];
        [button addSubview:lineView];
        [self addSubview:button];
        
        i++;
        
        
    }
    
    
    
    self.upOffset = ([self.buttonArray count]-1) * lHeightButton;
    
    NSLog(@"%ld",(long)self.upOffset);
    
}


//if the drawer is open close it if not open it
-(void)cancelButtonPressed:(id)sender
{
    if(self.isOpen)
    {
        self.isOpen = NO;
        [self closeDrawer];
    }
    else
    {
        self.isOpen = YES;
        [self openDrawer];
    }
    
}


//open drawer
-(void)openDrawer
{
    [self.delegate drawerWillOpen];
    [UIView animateWithDuration:.31 delay:0
     
                        options:0 animations:^{
                            
                            [self setFrame:CGRectMake(0, self.originalFrame.origin.y- self.upOffset, self.originalFrame.size.width, self.originalFrame.size.height)];
                        } completion:^(BOOL finished) {
                            
                            [self.delegate drawerDidOpen];
                            
                        }];
    
}



//close drawer
-(void)closeDrawer
{
    [self.delegate drawerWillClose];
    [UIView animateWithDuration:0.31 delay:0
 
                        options:0 animations:^{
                            
                            [self setFrame:self.originalFrame];
                        } completion:^(BOOL finished) {
                            
                            [self.delegate drawerDidClose];
                            self.canClick = YES;
                            self.isOpen = NO;
                            
                        }];
    
}

-(void)closeDrawerNoAnimation
{
    [self.delegate drawerWillClose];
    [UIView animateWithDuration:0.0 delay:0
     
                        options:0 animations:^{
                            
                            [self setFrame:self.originalFrame];
                        } completion:^(BOOL finished) {
                            
                            [self.delegate drawerDidClose];
                            self.canClick = YES;
                            self.isOpen = NO;
                            
                        }];
    
}



//no springing. used for when we drag
-(void)openDrawerAlternate:(CGPoint)point
{
    
    //the only thing to add to this is MATH to speed up or slow down the animation depeding on how far you have to go. if you have to go half the drawer size
    //animation should be a bit longer than if it is almost all the way up
    
    NSLog(@"%f",self.originalFrame.origin.y - point.y);
    NSLog(@"%f",point.y - self.originalFrame.origin.y);
    //open drawer manually.
    self.isOpen = YES;
    [self.delegate drawerWillOpen];
    [UIView animateWithDuration:.5 delay:0
                        options:0 animations:^{
                            
                            [self setFrame:CGRectMake(0,self.originalFrame.origin.y- self.upOffset, self.originalFrame.size.width, self.originalFrame.size.height)];
                        } completion:^(BOOL finished) {
                            
                            [self.delegate drawerDidOpen];
                            
                        }];
    
}



//close drawer//no springing. used for when we drag
-(void)closeDrawerAlternate:(CGPoint)point
{
    
    //the only thing to add to this is MATH to speed up or slow down the animation depeding on how far you have to go. if you have to go half the drawer size
    //animation should be a bit longer than if it is almost all the way down
    
    NSLog(@"%f",self.originalFrame.origin.y - point.y);
    
    
    //close drawer manually
    self.isOpen = NO;
    
    [self.delegate drawerWillClose];
    [UIView animateWithDuration:.5 delay:0
                        options:0 animations:^{
                            [self setFrame:self.originalFrame];
                        } completion:^(BOOL finished) {
                            
                            [self.delegate drawerDidClose];
                            self.canClick = YES;
                            
                        }];
    
}


/// when one of the regular buttons are pressed
-(void)actionButtonPressed:(id)sender
{
    UIButton *b = sender;
    
    if(self.canClick)
    {
       if(b.tag != 6)
       {
           self.canClick = NO;
           [self.delegate slideUpMenuButtonPressed:b];
            [self closeDrawerNoAnimation];
       }
        else
        {
            [self shareSheetWasPressed];
            [self closeDrawerNoAnimation];
        }
        
    }
}


-(void)shareSheetWasPressed
{
    
    
    
    NSArray *activityItems = @[[NSString stringWithFormat:@"Check out"]];
                                
                                UIActivityViewController *activityController =
                                [[UIActivityViewController alloc]
                                 initWithActivityItems:activityItems
                                 applicationActivities:nil];
                            UIViewController *c =(UIViewController*) self.superview.nextResponder;
                                [c presentViewController:activityController
                                                   animated:YES completion:nil];
}
//image from a color
-(UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


//pull button delegate
-(void)gestureHasMoved:(CGPoint)point
{
    //the offset between the current frame and the point of dragging
    CGFloat pointOffset = (self.currentFrame.origin.y-25) + point.y;
    //dont let the frame go below the max or above the min
    if(pointOffset >= self.originalFrame.origin.y)
    {
        pointOffset = self.originalFrame.origin.y;
    }
    
    if(pointOffset<=self.upOffset-235)
    {
        pointOffset = self.upOffset-230;
    }
    
    //change the frame
    self.offsetFrame = CGRectMake(0,pointOffset, self.currentFrame.size.width, self.currentFrame.size.height);
    //set the frame
    [self setFrame:self.offsetFrame];
    
}
-(void)gestureHasEnded:(CGPoint)point
{
    
    //the gesuter has ended (delegate from the drag buggon
    
    
    //originating frame is the frame from where we had set it last from last resting point
    CGFloat originator = self.currentFrame.origin.y;
    //ending is where the drag has taken the frame
    CGFloat ending = self.offsetFrame.origin.y;
    self.canClick = NO;
    
    //if the drawer is open
    if(self.isOpen)
    {
        //if the difference between resting and ending is -50px then close it if not, bring it back up
        CGFloat diff = originator - ending;
        
        if(diff < -51)
        {
            [self closeDrawerAlternate:point];
        }
        else
        {
            [self openDrawerAlternate:point];
        }
        
    }
    else
    {
        //difference between drag and last resting
        CGFloat diff = originator - ending;
        //if the difference between the two is at least sixty open it if not re close
        if(diff > 60)
        {
            [self openDrawerAlternate:point];
        }
        else
        {
            [self closeDrawerAlternate:point];
        }
        
    }
    
    
}
-(void)gesturHasStarted:(CGPoint)point
{
    //the gesture has started so lets get the current resting state and
    //tell the vc that the drawer is about to be dragged
    [self.delegate DrawerWillDrag];
    self.currentFrame = self.frame;
    
    
}

+(void)goToSelectedOption : (NSString *)option calledByVC: (UIViewController *) calledByVC
{
    
    //myContestsController
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UIViewController * newVC = [storyboard instantiateViewControllerWithIdentifier:option];
    newVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [calledByVC presentViewController:newVC animated:YES completion:nil];
    
}



- (UIImage *)scaleAndRotateImage:(UIImage *)image {
    
    int kMaxResolution = 35; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

@end
