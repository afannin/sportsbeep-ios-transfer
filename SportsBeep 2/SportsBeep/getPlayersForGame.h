//
//  getPlayersForGame.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/26/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "getPlayersDelegate.h"
#import "AFNetworking.h"
@interface getPlayersForGame : NSObject{
    id<getPlayersDelegate>delegate;
}

@property(nonatomic,weak)id delegate;

-(void)getPlayers:(NSString*)gameId :(NSString*)sport;


@end
