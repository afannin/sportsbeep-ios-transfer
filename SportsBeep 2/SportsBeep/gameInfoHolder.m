//
//  gameInfoHolder.m
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/1/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import "gameInfoHolder.h"


@implementation gameInfoHolder


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //     NSLog(@"%f",self.frame.size.width);
        
    }
    return self;
}




-(void)addGameTriangles :(NSDictionary*)homeTeam :(NSDictionary *)awayTeam :(CGFloat)cellWidth :(NSString *)date :(NSString *)time{
    
    
    if(!self.bottomT)
    {
        CGFloat triangleHeight =  self.frame.size.height;
        CGFloat triangleWidth  = cellWidth +15;
        
        self.bottomT = nil;
        self.topT = nil;
        UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, triangleWidth, 20)];
        [v setBackgroundColor:[UIColor blackColor]];
        [self addSubview:v];
        
        
        self.dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, (triangleWidth/2) + 20, 20)];
        [self.dateLabel setTextColor:[UIColor whiteColor]];
        //[self.dateLabel setBackgroundColor:[UIColor blackColor]];
        [self.dateLabel setText:date];
        [self.dateLabel setFont:[UIFont systemFontOfSize:10]];
        
        
        self.timeLabel = [[UILabel alloc]initWithFrame:CGRectMake((triangleWidth-23)/2, 0, triangleWidth/2, 20)];
        [self.timeLabel setTextColor:[UIColor whiteColor]];
        // [self.timeLabel setBackgroundColor:[UIColor blackColor]];
        [self.timeLabel setText:time];
        [self.timeLabel setFont:[UIFont systemFontOfSize:10]];
        [self.timeLabel setTextAlignment:NSTextAlignmentRight];
        [v addSubview:self.timeLabel];
        //  [self.timeLabel sizeToFit];
        [v addSubview:self.dateLabel];
        //  [self.dateLabel sizeToFit];
        
        
        UIColor *pHomeColor = [UIColor colorWithHexString:[homeTeam objectForKey:@"primary_color"]];
        UIColor *aHomeColor = [UIColor colorWithHexString:[homeTeam objectForKey:@"secondary_color"]];
        
        UIColor *aAwayColor = [UIColor colorWithHexString:[awayTeam objectForKey:@"secondary_color"]];
        UIColor *pAwayColor = [UIColor colorWithHexString:[awayTeam objectForKey:@"primary_color"]];
        
        
        
        //    NSLog([awayTeam objectForKey:@"primary_color"]);
        //    NSLog([awayTeam objectForKey:@"secondary_color"]);
        
        //   NSLog([homeTeam objectForKey:@"primary_color"]);
        //  NSLog([homeTeam objectForKey:@"secondary_color"]);
        
        
        self.bottomT = [[bottomTriangle alloc]initWithFrame:CGRectMake(0, 20,triangleWidth-5 ,triangleHeight-5)];
        [self.bottomT setBackgroundColor:pAwayColor];
        self.bottomT.textColor = aAwayColor;
        [self.bottomT addLabels:[awayTeam objectForKey:@"market"] :[awayTeam objectForKey:@"name"]];
        
        
        
        self.topT = [[topTriangle alloc]initWithFrame:CGRectMake(0,20,triangleWidth-5,triangleHeight-5)];
        [self.topT setBackgroundColor:pHomeColor];
        
        self.topT.textColor = aHomeColor;
        [self.topT addLabels:[homeTeam objectForKey:@"market"] :[homeTeam objectForKey:@"name"]];
        
        
        [self addSubview:self.bottomT];
        [self addSubview:self.topT];
        [self bringSubviewToFront:v];
        
        
        self.im = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
        
        [self.im    setCenter:CGPointMake(triangleWidth/2, 60)];
        [self.im   setImage:[UIImage imageNamed:@"at"]];
        [self addSubview:self.im];
        
    }
    else
    {
        [self changeTriangleColors:homeTeam :awayTeam:date:time];
        
    }
}



-(void)changeTriangleColors :(NSDictionary*)homeTeam :(NSDictionary *)awayTeam :(NSString *)date :(NSString *)time
{
    
    
    UIColor *pHomeColor = [UIColor colorWithHexString:[homeTeam objectForKey:@"primary_color"]];
    UIColor *aHomeColor = [UIColor colorWithHexString:[homeTeam objectForKey:@"secondary_color"]];
    
    UIColor *aAwayColor = [UIColor colorWithHexString:[awayTeam objectForKey:@"secondary_color"]];
    UIColor *pAwayColor = [UIColor colorWithHexString:[awayTeam objectForKey:@"primary_color"]];
    
    
    // NSLog([awayTeam objectForKey:@"primary_color"]);
    // NSLog([awayTeam objectForKey:@"secondary_color"]);
    
    // NSLog([homeTeam objectForKey:@"primary_color"]);
    // NSLog([homeTeam objectForKey:@"secondary_color"]);
    //
    if(!aAwayColor)
    {
        NSLog(@"couldnt get color");
    }
    
    
    [self.dateLabel setText:date];
    [self.timeLabel setText:time];
    
    self.bottomT.textColor = aAwayColor;
    [self.bottomT changeLabelText:[awayTeam objectForKey:@"market"] :[awayTeam objectForKey:@"name"]];
    
    [self.bottomT setBackgroundColor: pAwayColor];
    
    
    
    
    [self.topT setBackgroundColor:pHomeColor];
    self.topT.textColor = aHomeColor;
    [self.topT changeLabelText:[homeTeam objectForKey:@"market"] :[homeTeam objectForKey:@"name"]];
    
    
}


@end
