//
//  ToggleButtons.h
//  SportsBeep
//
//  Created by iOSDev on 3/26/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ToggleButtonsDelegate.h"

@interface toggleButtons : UIView{
    //id<toggleButtonDelegate>delegate;
}

@property (nonatomic,weak)id delegate;
@property(nonatomic,retain)NSMutableArray *toggleButtons;
@property(nonatomic,assign)int currentSelected;
-(void)toggleMyButton:(NSInteger)selectedValue;

@end
