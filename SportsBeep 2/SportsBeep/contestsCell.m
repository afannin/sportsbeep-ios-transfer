//
//  contestsCell.m
//  SportsBeep
//
//  Created by iOSDev on 5/30/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "contestsCell.h"


@implementation contestsCell

- (void)awakeFromNib {
    // Initialization code
    [self makeSubViews];
}


-(void)makeSubViews
{

    if(!self.triangleHolder)
    {
        self.triangleHolder = [[bigTriangleHolder alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 150)];
        [self addSubview:self.triangleHolder];
   
    }
    


}

-(void)addGameTriangles :(NSDictionary*)homeTeam :(NSDictionary *)awayTeam:(CGFloat)cellWidth:(NSString *)date:(NSString*)time
{
    [self.triangleHolder addGameTriangles:homeTeam :awayTeam :cellWidth :date :time];
    
    [self setBackgroundColor:[UIColor clearColor]];
}

-(void)changeTriangleColors:(NSDictionary*)homeTeam :(NSDictionary *)awayTeam:(NSString *)date:(NSString*)time
{
    
    [self.triangleHolder changeTriangleColors:homeTeam :awayTeam :date :time];
 
}
-(void)changeTriangleInfo
{
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
