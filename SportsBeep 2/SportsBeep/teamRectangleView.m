//
//  teamRectangleView.m
//  SportsBeep
//
//  Created by Daniel Nasello on 6/1/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "teamRectangleView.h"
#import "UIColor+Expanded.h"
@implementation teamRectangleView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //     NSLog(@"%f",self.frame.size.width);
        
    }
    return self;
}






-(void)addGameRectanges :(NSDictionary *)homeTeam :(NSDictionary *)awayTeam :(CGFloat)cellWidth :(NSString *)date :(NSString *)time{
    
    
    
    
    UIColor *pHomeColor = [UIColor colorWithHexString:[homeTeam objectForKey:@"primary_color"]];
    UIColor *aHomeColor = [UIColor colorWithHexString:[homeTeam objectForKey:@"secondary_color"]];
    
    UIColor *aAwayColor = [UIColor colorWithHexString:[awayTeam objectForKey:@"secondary_color"]];
    UIColor *pAwayColor = [UIColor colorWithHexString:[awayTeam objectForKey:@"primary_color"]];
    
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 20)];
    [self setBackgroundColor:[UIColor blackColor]];
    [self addSubview:topView];
    
    
    
    UILabel  *gameDate = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, topView.frame.size.width,topView.frame.size.height)];
    [gameDate setTextColor:[UIColor whiteColor]];
    [topView addSubview:gameDate];
    [gameDate setText:date];
    [gameDate setFont: [UIFont fontWithName:@"BebasNeueBold" size:15.0]];
    
    UILabel  *gameTime = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, topView.frame.size.width,topView.frame.size.height)];
    [gameTime setTextColor:[UIColor whiteColor]];
    [topView addSubview:gameTime];
    [gameTime setTextAlignment:NSTextAlignmentRight];
    [gameTime setText:time];
    [gameTime setFont: [UIFont fontWithName:@"BebasNeueBold" size:15.0]];
    
    
    
    UIView *homeRectangle = [[UIView alloc]initWithFrame:CGRectMake(0, 20, self.frame.size.width/2, self.frame.size.height-20)];
    [homeRectangle setBackgroundColor:pHomeColor];
    [self addSubview:homeRectangle];
    
    
    //[awayTeam objectForKey:@"market"] :[awayTeam objectForKey:@"name"]
    UILabel *homeTeamPlace = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width/2, 18)];
    [homeTeamPlace setTextColor:aHomeColor];
    [homeTeamPlace setText: [homeTeam objectForKey:@"market"]];
    [homeRectangle addSubview:homeTeamPlace];
    [homeTeamPlace setFont: [UIFont fontWithName:@"BebasNeueBold" size:18.0]];
    
    UILabel *homeTeamName = [[UILabel alloc]initWithFrame:CGRectMake(0, 13, self.frame.size.width/2, 29)];
    [homeTeamName setTextColor:aHomeColor];
    [homeTeamName setText: [homeTeam objectForKey:@"name"]];
    [homeRectangle addSubview:homeTeamName];
    [homeTeamName setFont: [UIFont fontWithName:@"BebasNeueBold" size:26.0]];
    
    
    
    
    
    UIView *awayRectangle = [[UIView alloc]initWithFrame:CGRectMake(self.frame.size.width/2, 20, self.frame.size.width/2, self.frame.size.height-20)];
    [awayRectangle setBackgroundColor:pAwayColor];
    [self addSubview:awayRectangle];
    
    
    //[awayTeam objectForKey:@"market"] :[awayTeam objectForKey:@"name"]
    UILabel *awayTeamPlace = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width/2, 18)];
    [awayTeamPlace setTextColor:aAwayColor];
    [awayTeamPlace setText: [awayTeam objectForKey:@"market"]];
    [awayRectangle addSubview:awayTeamPlace];
    [awayTeamPlace setTextAlignment:NSTextAlignmentRight];
      [awayTeamPlace setFont: [UIFont fontWithName:@"BebasNeueBold" size:18.0]];
    UILabel *awayTeamName = [[UILabel alloc]initWithFrame:CGRectMake(0, 13, self.frame.size.width/2, 29)];
    [awayTeamName setTextColor:aAwayColor];
    [awayTeamName setText: [awayTeam objectForKey:@"name"]];
    [awayRectangle addSubview:awayTeamName];
    [awayTeamName setTextAlignment:NSTextAlignmentRight];
      [awayTeamName setFont: [UIFont fontWithName:@"BebasNeueBold" size:26.0]];
   UIImageView *im = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    
    [im   setCenter:CGPointMake(self.frame.size.width/2,(self.frame.size.height/2)+10)];
    [im  setImage:[UIImage imageNamed:@"at"]];
    [self addSubview:im];

    
}

@end
