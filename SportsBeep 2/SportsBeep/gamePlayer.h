//
//  gamePlayer.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/26/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface gamePlayer : NSObject


@property(nonatomic,strong)NSArray *stats;
@property(nonatomic,strong)NSString *player_id;
@property(nonatomic,strong)NSString *player_name;
@property(nonatomic,strong)NSString *player_number;
@property(nonatomic,strong)NSString *player_position;
@property(nonatomic,strong)NSString *player_headshot;
@property(nonatomic,strong)NSString *team_id;
@property(nonatomic,retain)NSDictionary *team_object;
@property(nonatomic,strong)NSString *team_abbr;
@property(nonatomic,strong)NSString *fakePos;

@end
