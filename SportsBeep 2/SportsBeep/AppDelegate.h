//
//  AppDelegate.h
//  SportsBeep
//
//  Created by iOSDev on 3/24/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

