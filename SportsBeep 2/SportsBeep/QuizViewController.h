//
//  QuizViewController.h
//  SportsBeep
//
//  Created by iOSDev on 5/26/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KAProgressLabel/KAProgressLabel.h>
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "individualGame.h"
#import "AFHTTPRequestOperationManager.h"
@interface QuizViewController : UIViewController<UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *headLabel;
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (strong, nonatomic) UILabel *timerLabel;
@property (strong, nonatomic) KAProgressLabel *progressLabel;
@property(nonatomic,strong)NSArray *quizQuestions;
@property (strong, nonatomic) NSTimer *timer;

- (void) initializeScene;
@property(nonatomic,strong)individualGame *indieGame;

@property(nonatomic,strong)NSDictionary *homeTeamObject;
@property(nonatomic,strong)NSDictionary *awayTeamObject;

@property(nonatomic,strong)NSString *ticket_id;
@property (strong, nonatomic) IBOutlet UILabel *bestsLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionProgressLabel;

-(void)startQuiz;

-(void)getQuizQuestions:(NSString *)ticket_id;
-(void)addGameTriangle:(individualGame *)indieGame;
@property (strong, nonatomic) IBOutlet UIView *triangleHolder;

@end
