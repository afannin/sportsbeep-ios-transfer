//
//  HighlightableTextField.h
//  SportsBeep
//
//  Created by iOSDev on 3/31/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HighlightableTextField : UITextField

@end
