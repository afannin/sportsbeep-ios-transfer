//
//  QuizViewController.m
//  SportsBeep
//
//  Created by iOSDev on 5/26/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "QuizViewController.h"
#import "KAProgressLabel.h"
#import "Colors.h"
#import "bigTriangleHolder.h"

@interface QuizViewController (){
    NSInteger answeredIndex;
    NSMutableArray *answerKey;
    bigTriangleHolder *bigT;
    BOOL quizDone;
}
@property CGFloat timerDuration;
@end

@implementation QuizViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //index of the questions answered
    answeredIndex = -1;
    
    // Do any additional setup after loading the view.
    

    quizDone = NO;
    
}


-(void)startQuiz
{
    [self initializeScene];
    [self startTimer];
    [self changeQuestionIndex];
}


-(void)changeQuestionIndex
{
     answeredIndex++;
    
    BOOL isDone = [self.quizQuestions count] <= answeredIndex;
    
    if(!isDone)
    {
        //question from the questions of array contains the id of the question
        //if its best or bust and the actual question
        NSDictionary *questionDictionary = [self.quizQuestions objectAtIndex:answeredIndex];
        
        NSString *bestBust  = [questionDictionary objectForKey:@"type"];
        NSString *questionId = [NSString stringWithFormat:@"%@",[questionDictionary objectForKey:@"id"]];
        NSString *questionText = [questionDictionary objectForKey:@"text"];
        
        [self.questionLabel setText:questionText];
        
        [self.bestsLabel setText:[NSString stringWithFormat:@"THE %@!",bestBust]];
        
        [self.questionProgressLabel setText:[NSString stringWithFormat:@"Question %ld of %lu",answeredIndex + 1,(unsigned long)[self.quizQuestions count]]];
    }
    else
    {
   
 
        [self processQuiz];
        NSLog(@"done");
        
    
 
    }
    
    
    
}



-(void)homeTeamHit
{
    
   // NSDictionary
    
    NSLog(@"tapped home");
    
    if(!quizDone)
    {
        

        NSDictionary *questionDictionary = [self.quizQuestions objectAtIndex:answeredIndex];
        NSString *questionId = [NSString stringWithFormat:@"%@",[questionDictionary objectForKey:@"id"]];
    
        NSString *tId = [self.indieGame.homeTeamObject objectForKey:@"id"];
    
    
        NSDictionary *dictToAadd = [NSDictionary dictionaryWithObjectsAndKeys:
                                questionId,@"question_id",
                                tId,@"answer_id"
                                , nil];
        [self changeQuestionIndex];
    
        NSLog(@"the team id %@",tId);
    
    }
}

-(void)awayTeamHit
{
    
    NSLog(@"tapped away");
    if(!quizDone)
    {
    
    NSDictionary *questionDictionary = [self.quizQuestions objectAtIndex:answeredIndex];
    NSString *questionId = [NSString stringWithFormat:@"%@",[questionDictionary objectForKey:@"id"]];
    NSString *teamId = [self.awayTeamObject objectForKey:@"id"];
    
    
    
    NSDictionary *dictToAadd = [NSDictionary dictionaryWithObjectsAndKeys:
                                questionId,@"question_id",
                                teamId,@"answer_id"
                                , nil];
    
        [self changeQuestionIndex];
        NSLog(@"the team id %@",teamId);
    }
 
}



-(void)processQuiz
{
    quizDone = YES;
    
    [self endTimer];
    
    
    [self.progressLabel stopAnimations];
    
}



- (void) startTimer {
    self.timerDuration = 14.0;
    self.timerLabel.text = [NSString stringWithFormat: @"%f", self.timerDuration];
    self.timer = [NSTimer scheduledTimerWithTimeInterval: 0.1
                                                  target: self
                                                selector:@selector(countDown)
                                                userInfo: nil repeats:YES];
}

- (void) countDown {
    
    self.timerDuration -= 0.1;
    self.timerLabel.text = [NSString stringWithFormat: @"%.1f", self.timerDuration];
    
    if (self.timerDuration <= 0) {
        [self.timer invalidate];
        self.timerLabel.text = @"done";
    }
    
}


-(void)endTimer
{
    [self.timer invalidate];
    self.timerLabel.text = @"done";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initializeScene {
    
    self.headLabel.backgroundColor = RED;
    //self.headLabel.frame.size.width = self.view.frame.size.width;
    self.questionLabel.backgroundColor = SKY_BLUE;
    
    self.progressLabel = [[KAProgressLabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width - 65, 8, 50, 50)];
    [self.progressLabel setProgress:1.0 timing:TPPropertyAnimationTimingEaseOut duration:15.0 delay:0];
    [self.progressLabel setProgressColor:RED];
    [self.progressLabel setTrackColor: [UIColor lightGrayColor]];
    
    self.timerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    self.timerLabel.textColor = [UIColor whiteColor];
    self.timerLabel.textAlignment = NSTextAlignmentCenter;
    [[self timerLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
    
    // add subviews
    [self.headLabel addSubview:self.progressLabel];
    [self.progressLabel addSubview:self.timerLabel];

}

-(void)addGameTriangle:(individualGame *)indieGame
{
    bigT  = [[bigTriangleHolder alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 220)];
    
    [bigT addGameTriangles:indieGame.awayTeamObject :indieGame.homeTeamObject:self.view.frame.size.width:indieGame.gameTime:indieGame.gameDay];
    
    [self.triangleHolder addSubview:bigT];
    

    [self.view bringSubviewToFront:self.triangleHolder];
    
    [self.triangleHolder bringSubviewToFront:bigT];
    
    
    
    UITapGestureRecognizer *homeRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(homeTeamHit)];
    
    UITapGestureRecognizer *awayRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(awayTeamHit)];
    


    homeRec.delegate = self;
    awayRec.delegate  = self;
    
    UIView *tapView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [tapView setBackgroundColor:[UIColor clearColor]];
    [bigT.topT addSubview:tapView];
    [tapView addGestureRecognizer:homeRec];
    [bigT.topT setClipsToBounds:YES];
    
    
    UIView *tapView2 = [[UIView alloc]initWithFrame:CGRectMake((bigT.frame.size.width/2), (-bigT.frame.size.height/2)+20, self.view.frame.size.width-20, self.view.frame.size.height-20)];
    [tapView2 setBackgroundColor:[UIColor clearColor]];
    [bigT addSubview:tapView2];
    [tapView2 addGestureRecognizer:awayRec];
    tapView2.transform = CGAffineTransformMakeRotation(45);
    
    [bigT.bottomT.subLabel setFrame:CGRectMake(bigT.bottomT.subLabel.frame.origin.x, bigT.bottomT.subLabel.frame.origin.y-20,bigT.bottomT.subLabel.frame.size.width, bigT.bottomT.subLabel.frame.size.height)];
    [bigT.bottomT.mainLabel setFrame:CGRectMake(bigT.bottomT.mainLabel.frame.origin.x, bigT.bottomT.mainLabel.frame.origin.y-20, bigT.bottomT.mainLabel.frame.size.width, bigT.bottomT.mainLabel.frame.size.height)];
   // [bigT.bottomT addGestureRecognizer:homeRec];
    [bigT setClipsToBounds:YES];
  
  
}




- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return YES;
}


@end
