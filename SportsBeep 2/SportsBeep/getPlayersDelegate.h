//
//  getPlayersDelegate.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/26/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol getPlayersDelegate <NSObject>


-(void)getPlayersHasStarted;
-(void)getPlayersSuccess:(NSDictionary *)response;
-(void)getPLayersHasFailed:(NSString*)err;


@end