//
//  individualGame.m
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/1/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import "individualGame.h"

@implementation individualGame




-(void)changeDateToRightDate:(NSDate *)date
{
    TimeConversion *t = [[TimeConversion alloc]init];
    
    t.date = date;
    
    NSString *dayOfWeek = [t getWeekDayAbbr];
    NSString *month = [t getMonthThreeDigit:date];
    NSString *dayOfMonth = [t getDayOfMonth];
    NSString *getYear = [t getYear];
    
    
    
    
    self.gameDay = [NSString stringWithFormat:@"%@,%@ %@,%@",dayOfWeek,month,dayOfMonth,getYear];
    
    
    
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM";
    NSDate *d = date;
    
    NSString *dateString = [dateFormatter stringFromDate: d];
    
    NSLog(dateString);
    
    
    dateFormatter.dateFormat = @"hh:mm a";
    NSString *pmamDateString = [dateFormatter stringFromDate:d];
    NSString *timeZoneName = [[NSTimeZone localTimeZone] abbreviation];
    
    
    
    self.gameTime = [NSString stringWithFormat:@"%@ %@",pmamDateString,timeZoneName];
    
}

@end
