//
//  topTriangle.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/1/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface topTriangle : UIView

@property(nonatomic,strong)UILabel *mainLabel;
@property(nonatomic,strong)UILabel *subLabel;
@property(nonatomic,assign)UIColor *textColor;
-(void)addLabels:(NSString *)state:(NSString *)team;
-(void)changeLabelText:(NSString *)state:(NSString *)team;
@end
