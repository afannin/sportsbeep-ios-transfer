//
//  secondVC.m
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 4/17/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import "secondVC.h"
#import "MyContestsViewController.h"
#import "DarkCover.h"
#import "SBApiAccess.h"


@interface secondVC (){
    DarkCover *cover;
    getGames *gG;
    NSMutableArray *gamesArray;
    CGFloat cellWidth;
}

@end

@implementation secondVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self addCoverAndMenuBox];
    
    self.view.backgroundColor = PANEL_BG_BLUE;
    
    //define the cell width up here so the phone does not have to continously keep making calculations
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    cellWidth = screenRect.size.width/2-2;
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.backgroundColor = PANEL_BG_BLUE;
    
    
    
    [self.mLabel setFont:[UIFont fontWithName:@"BebasNeueBold" size:32.0]];
    self.mLabel.backgroundColor = DARK_BLUE;
    self.mLabel.textAlignment = NSTextAlignmentCenter;

    
    [self.mCtpLabel setFont:[UIFont fontWithName:@"BebasNeueBold" size:14.0]];
    gamesArray = [[NSMutableArray alloc]init];
    
    gG = [[getGames alloc]init];
    [gG setDelegate:self];
    
    
    //set the sport, set the level, and all/just future events
    [gG setSPortAndType:0:0:1];
    [gG getGames];
    
    //holds the games;  
    
}

//get games delegation methods


//the games has started to fetch the web data
-(void)getGamesStartedToFetchGames
{
    [self showHud];
    NSLog(@"@showing");
}

//get games has failed
-(void)getGamesFailedWithError:(NSString *)error
{
    [self hideHud];
}

//brings back the type of game, the level of game, and the object that has games in it.
-(void)getGamesHasFetchedGames :(NSInteger)gameType :(NSInteger)gameLevel :(NSDictionary*)gameObject
{
    [self hideHud];
    
    NSArray *games = [gameObject objectForKey:@"games"];
    
    for(NSDictionary *dict in games)
    {
        
      
 
        individualGame *g = [[individualGame alloc]init];
        
        NSDictionary *outerG = dict;
        
        
        g.awayTeamObject = [outerG objectForKey:@"away_team"];
        g.homeTeamObject = [outerG objectForKey:@"home_team"];
        g.pool = [outerG objectForKey:@"pool"];
        g.ticket = [outerG objectForKey:@"ticket"];
        g.date = [outerG objectForKey:@"start_time"];
        g.gameId = [outerG objectForKey:@"id"];
        
        NSString *myDate = [NSString stringWithFormat:@"%@",[outerG objectForKey:@"start_time"]];
        
        NSDateFormatter *dateFormatter  =   [[NSDateFormatter alloc]init];
        
        NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [dateFormatter setTimeZone:gmt];
        
        NSLog(@"the date is %@",myDate);
        
        
     //   [dateFormatter setDateFormat:@"yyyy-MM-DD'T'HH:mm:SSSS'Z'"];
       
        dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
        NSDate *date = [dateFormatter dateFromString:myDate];
        
        NSDate *yourDate   =  [dateFormatter dateFromString:myDate];
        
        if(yourDate != nil)
        {
            [g changeDateToRightDate:date];
            
        }
        
        // add a game object;
        [gamesArray addObject:g];
        
    }

    [self.collectionView reloadData];
    
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}




- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [gamesArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    gameInfoHolder *gameInfo;
    gameInfo =  (gameInfoHolder *)[cell viewWithTag:1];
    // [gameInfo setBackgroundColor:[UIColor greenColor]];
    
    
    individualGame *g = [gamesArray objectAtIndex:indexPath.row];
    
    
    
    [gameInfo addGameTriangles:g.awayTeamObject :g.homeTeamObject:cellWidth:g.gameDay:g.gameTime];
    
    
    
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(cellWidth, 100);
}




-(void)showHud
{
    KVNProgressConfiguration *configuration = [[KVNProgressConfiguration alloc] init];


    //  configuration.statusColor = [UIColor whiteColor];
    configuration.statusFont = [UIFont fontWithName:@"HelveticaNeue-Thin" size:15.0f];
    configuration.circleStrokeForegroundColor = [UIColor darkGrayColor];
    configuration.circleStrokeBackgroundColor = [UIColor clearColor];
    configuration.circleFillBackgroundColor = [UIColor clearColor];
    configuration.backgroundFillColor = [UIColor clearColor];
    configuration.backgroundTintColor = [UIColor clearColor];
    configuration.successColor = [UIColor whiteColor];
    configuration.errorColor = [UIColor whiteColor];
    configuration.circleSize = 50.0f;
    configuration.lineWidth = 2.0f;
    configuration.fullScreen = NO;
    
    
    configuration.tapBlock = ^(KVNProgress *progressView) {
        // Do something you want to do when the user tap on the HUD
        // Does nothing by default
    };
    
    // You can allow user interaction for behind views but you will losse the tapBlock functionnality just above
    // Does not work with fullscreen mode
    // Default is NO
    configuration.allowUserInteraction = NO;
    
    [KVNProgress setConfiguration:configuration];
    [KVNProgress showWithStatus:@""
                         onView:self.view];
    
}

-(void)hideHud
{
    [KVNProgress dismiss];
    
}




-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"%ld",(long)indexPath.row);
    [self goToSwipe:indexPath.row];
}



-(void)goToSwipe:(CGFloat)index
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    PickGameViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"vc"];
    individualGame   *g = [gamesArray objectAtIndex:index];
    vc.indieGame = g;
    [self presentViewController:vc animated:YES completion:nil];

}



//delegate methods slide up menu
-(void) addCoverAndMenuBox {
    
    //cover
    CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    self->cover = [[DarkCover alloc] initWithFrame:frame];
    self->cover.hidden = YES;
    [self.view addSubview:self->cover];
    
    NSArray *titles = [[NSArray alloc]init];
    titles = @[@"PICK A GAME",@"MY CONTESTS",@"CHANGE SPORT",@"PROFILE",@"HOW TO PLAY",@"SHARE",@"LOGOUT"];
    
    
    CGFloat btnHeight = 60;
    self.mSlideUpMenu = [[MenuBox alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - btnHeight, self.view.frame.size.width,btnHeight * ([titles count] + 1))];
    
    [self.mSlideUpMenu setDelegate:self];
    [self.view setClipsToBounds:YES];
    
    [self.view addSubview:self.mSlideUpMenu];
    
    [self.mSlideUpMenu setBackgroundColor: [UIColor colorWithRed:0.18 green:0.369 blue:0.651 alpha:1]];
    
    
    self.mSlideUpMenu.buttonTitles = titles;
    [self.mSlideUpMenu addButtons];
}

//delegate methods slide up menu
-(void)slideUpMenuButtonPressed:(UIButton*)button
{
    SBApiAccess *api = [[SBApiAccess alloc] init];
    
    NSInteger tag = button.tag;
    //from here you can do some actions based on the button pressed
    switch (tag) {
        case 1:
            break;
        case 2:
            [MenuBox goToSelectedOption:@"myContestsController" calledByVC:self];
            break;
        case 3:
            [MenuBox goToSelectedOption:@"NewGame" calledByVC:self];
            break;
        case 4:
            [MenuBox goToSelectedOption:@"Profile" calledByVC:self];
            break;
        case 5:
            [MenuBox goToSelectedOption:@"HowToPlay" calledByVC:self];
            break;
        case 6:
            NSLog(@"Share");
            break;
        case 7:
            //log out SB API
            [api logout];
            
            //dismiss view controllers and go to sign in page
            [self dismissModalStack];
            
            break;
            
        default:
            break;
    }
}

-(void)dismissModalStack {
    UIViewController *vc = self.presentingViewController;
    while (vc.presentingViewController) {
        vc = vc.presentingViewController;
    }
    [vc dismissViewControllerAnimated:YES completion:NULL];
}


//cancel was pressed
-(void)cancelButtonWasPressed
{
    
}
//will close
-(void)drawerWillClose
{
    self->cover.hidden = YES;
}
//did close
-(void)drawerDidClose
{
    self->cover.hidden = YES;
}
//will open
-(void)drawerWillOpen
{
    self->cover.hidden = NO;
}
//opened
-(void)drawerDidOpen
{
    self->cover.hidden = NO;
}
//drawer dragging add the barrier view
-(void)DrawerWillDrag
{
    if (self->cover.hidden == NO) {
        self->cover.hidden = YES;
    }
    if (self->cover.hidden == YES) {
        self->cover.hidden = NO;
    }
    
}

@end
