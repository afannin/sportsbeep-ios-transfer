//
//  QuizViewController.h
//  SportsBeep
//
//  Created by iOSDev on 5/25/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuizViewController.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "individualGame.h"
@interface QuizAlertViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@property (weak, nonatomic) IBOutlet UILabel *colorLabel;
- (void) initializeAlertView;

@property(nonatomic,strong)individualGame *indieGame;

@property(nonatomic,strong)NSDictionary *homeTeamObject;
@property(nonatomic,strong)NSDictionary *awayTeamObject;

@property(nonatomic,strong)NSString *ticket_id;

-(void)getQuizQuestions:(NSString *)ticket_id;
@end
