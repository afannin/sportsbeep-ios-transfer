//
//  bottomTriangle.m
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/1/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import "bottomTriangle.h"

@implementation bottomTriangle
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self clip];
        
    }
    return self;
}


-(void)addLabels:(NSString *)state:(NSString *)team
{
    self.subLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,self.frame.size.height-32, self.frame.size.width-10, 12)];
    [self.subLabel setTextColor:self.textColor];
    [self.subLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:14.0]];
    [self.subLabel setText:state];
    [self.subLabel setTextAlignment:NSTextAlignmentRight];
    [self addSubview:self.subLabel];
    
    self.mainLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,self.frame.size.height-20, self.frame.size.width-10, 18)];
    [self.mainLabel setTextColor:self.textColor];
    [self.mainLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:20.0]];
    [self.mainLabel setTextAlignment:NSTextAlignmentRight];
    [self.mainLabel setText:team];
    [self addSubview:self.mainLabel];
    
}

-(void)changeLabelText:(NSString *)state:(NSString *)team
{
    [self.subLabel setText:state];
    [self.mainLabel setText:team];
    [self.subLabel setTextColor:self.textColor];
    [self.mainLabel setTextColor:self.textColor];
}


-(void)clip
{
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    // Build a triangular path
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){width, 0}];
    [path addLineToPoint:(CGPoint){width,height}];
    [path addLineToPoint:(CGPoint){0,height}];
    
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask = [CAShapeLayer new];
    mask.frame = self.bounds;
    mask.path = path.CGPath;
    
    // Mask the imageView's layer with this shape
    self.layer.mask = mask;
}
@end
