//
//  playerBustView.h
//  SportsBeep
//
//  Created by Rafi Chehirian on 6/25/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface playerBustView : UIView
@property(nonatomic,strong)UILabel *position;
@property(nonatomic,strong)UILabel *questionType;
@property(nonatomic,strong)UIView *pointsHolder;
@property(nonatomic,strong)UILabel *pointsAmount;
@property(nonatomic,strong)UITextView *question;
@property(nonatomic,strong)UILabel *answer;
@property(nonatomic,strong)UILabel *isCorrect;
@property(nonatomic,strong)UITextView *otherInfo;
@property(nonatomic,strong)UIImageView *rightWrong;
@property(nonatomic,strong)UILabel *cilabel;
-(void)addOtherStuff:(NSString *)teamName:(NSString *)points;
@end
