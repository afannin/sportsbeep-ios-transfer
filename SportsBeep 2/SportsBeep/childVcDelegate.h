//
//  childVcDelegate.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 4/17/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol childVcDelegate <NSObject>

-(void)sampleMethod;

-(void)positionButtonWasPressed:(int)position;
-(void)submitTeamWasPressed;
@end