//
//  thirdVC.m
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 4/17/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import "thirdVC.h"

#import "PickGameViewController.h"

#import "bigTriangleHolder.h"
#import "playerPickerView.h"
@interface thirdVC(){
    
    NSMutableArray *linearLayoutItemArray;
    
}

@end

@implementation thirdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.buttonShouldBeEnabled = NO;
    
  
    UILabel *l = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 50)];
    [l setBackgroundColor:DARK_BLUE];
    [l setTextColor:[UIColor whiteColor]];
    [l  setFont: [UIFont fontWithName:@"BebasNeueBold" size:26.0]];
    [l  setTextAlignment:NSTextAlignmentCenter];
    [l setText:@"BUILD A TEAM"];
    [self.view addSubview:l];
    

    
    //  ViewController *c = (ViewController *)self.parentViewController;
    
    
    individualGame *g = self.iGame;
    
    
    [self.view setFrame:[[UIScreen mainScreen] bounds]];
    
    
    
    [self.thirdML setFont:[UIFont fontWithName:@"BebasNeueBold" size:32.0]];
    
    
    //holds all of the linear layout items
    linearLayoutItemArray = [[NSMutableArray alloc]init];
    
    self.linearLayout = [[CSLinearLayoutView alloc]initWithFrame:CGRectMake(0, 210, self.view.frame.size.width, self.view.frame.size.height-260)];
    
    
    [self.view addSubview:self.linearLayout];
    [self addInitialLinearItems];
    
    
    
    UIView *gameInfoBox = [[UIView alloc]initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, 110)];
    [gameInfoBox setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:gameInfoBox];
    
    
    bigTriangleHolder *holder = [[bigTriangleHolder alloc]initWithFrame:gameInfoBox.bounds];
    [gameInfoBox addSubview:holder];
    [holder addGameTriangles:g.awayTeamObject :g.homeTeamObject :self.view.frame.size.width :g.gameDay :g.gameTime];
    
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    
}


-(void)addInitialLinearItems
{
    
    NSArray *titles = [[NSArray alloc]init];
    titles = @[@"PITCHER",@"CATCHER",@"CORNER INFIELDER",@"MIDDLE INFIELDER",@"OUTFIELDER",@"FLEX PLAYER"];
    int i = 0;
    for(NSString *title in titles)
    {
        UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
        [b setFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        [b.titleLabel setTextColor:[UIColor whiteColor]];
        [b setTitle:title forState:UIControlStateNormal];
        [b.titleLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:26.0]];
        [b setBackgroundImage:[self imageWithColor:  [UIColor colorWithRed:0.314 green:0.349 blue:0.478 alpha:1]] forState:UIControlStateNormal];
        [b setBackgroundImage:[self imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateHighlighted];
        [b addTarget:self
              action:@selector(buttonWasTapped:)
    forControlEvents:UIControlEventTouchUpInside];
        [b setTag:i];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, b.frame.size.width, 1)];
        lineView.backgroundColor = [UIColor colorWithRed:0.224 green:0.259 blue:0.388 alpha:1];
        [b addSubview:lineView];
        CSLinearLayoutItem *item = [CSLinearLayoutItem layoutItemForView:b];
        [linearLayoutItemArray addObject:item];
        [self.linearLayout addItem:item];
        
        
        i++;
    }
    
    [self addOtherLinears];
    
    
}



//when the button was clicked
-(void)buttonWasTapped:(id)sender
{
    UIButton *b = sender;
    
    int tag = (int)b.tag;
    
    [self.delegate positionButtonWasPressed: tag];
}




-(UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}



-(void)replaceLinear :(playerPickerView *)v :(int)pos
{
    
    NSLog(@"the v %@",v);
    
    UIView *fu = [[UIView alloc ]initWithFrame:CGRectMake(0,0, 50,30)];
    [fu setBackgroundColor:[UIColor greenColor]];
    
    playerPickerView *f = (playerPickerView *)[v snapshotViewAfterScreenUpdates:YES];
    
    
    UIGraphicsBeginImageContext(v.bounds.size);
    
    [v.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    
    
    UIImageView *iv = [[UIImageView alloc]initWithFrame:v.bounds];
    [iv setImage:viewImage];
    UIGraphicsEndImageContext();
    
    //   CSLinearLayoutItem *i = [linearLayoutItemArray objectAtIndex:pos];
    [self.linearLayout removeAllItems];
    int vi = 0;
    for(CSLinearLayoutItem *li in [linearLayoutItemArray mutableCopy])
    {
        if(pos != vi)
        {
            [self.linearLayout addItem:li];
        }
        else
        {
            f.tag = vi;
            UITapGestureRecognizer *singleFingerTap =
            [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(posTapped:)];
            [f addGestureRecognizer:singleFingerTap];
            CSLinearLayoutItem *item2 = [CSLinearLayoutItem layoutItemForView:f];
            [self.linearLayout addItem:item2];
            
            [linearLayoutItemArray replaceObjectAtIndex:pos withObject:item2];
            
        }
        
        
        vi++;
    }
    
    
    [self addOtherLinears];
}



-(void)posTapped:(UITapGestureRecognizer *)r
{
    
    int tag = (int)r.view.tag;
    
    [self.delegate positionButtonWasPressed:tag];
}


-(void)addOtherLinears
{
    
    CGRect main = [[UIScreen mainScreen] bounds];
    
    UIView *change = [[UIView alloc]initWithFrame:CGRectMake(0, 0, main.size.width, 50)];
    [change setBackgroundColor:  [UIColor colorWithRed:0.314 green:0.349 blue:0.478 alpha:1]];
    UILabel *l = [[UILabel alloc]initWithFrame:change.bounds];
    [l setNumberOfLines:0];
    [l setTextAlignment:NSTextAlignmentCenter];
    [l setTextColor:[UIColor whiteColor]];
    [l setText:@"YOU CAN CHANGE ANY OF YOUR CHOICES BY TAPPING A POSITION ABOVE"];
    [change addSubview:l];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, change.frame.size.width, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:0.224 green:0.259 blue:0.388 alpha:1];
    [change addSubview:lineView];
    [l setFont:[UIFont systemFontOfSize:12]];
    CSLinearLayoutItem *item = [CSLinearLayoutItem layoutItemForView:change];
    [self.linearLayout addItem:item];
    
    
    
    
    
    
    self.submitTeam = nil;
    self.submitTeam = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.submitTeam setFrame:CGRectMake(0, 0, main.size.width, 50)];
    [self.submitTeam.titleLabel setTextColor:[UIColor whiteColor]];
    [self.submitTeam setTitle:@"SET YOUR TEAM" forState:UIControlStateNormal];
    [self.submitTeam setBackgroundImage:[self imageWithColor:[UIColor redColor]] forState:UIControlStateNormal];
    [self.submitTeam setBackgroundImage:[self imageWithColor:[UIColor darkGrayColor]] forState:UIControlStateHighlighted];
    [self.submitTeam setBackgroundImage:[self imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateDisabled];
       [self.submitTeam.titleLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:26.0]];
    if(!self.buttonShouldBeEnabled)
    {
        [self.submitTeam setEnabled:NO];
    }
    
    [self.submitTeam addTarget:self
                 action:@selector(submitPressd)
       forControlEvents:UIControlEventTouchUpInside];
    
    
    CSLinearLayoutItem *item2 = [CSLinearLayoutItem layoutItemForView:self.submitTeam];
    [self.linearLayout addItem:item2];
    
}



-(void)submitPressd
{
    
    [self.delegate submitTeamWasPressed];
}

@end
