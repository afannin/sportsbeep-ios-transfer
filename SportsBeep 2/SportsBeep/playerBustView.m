//
//  playerBustView.m
//  SportsBeep
//
//  Created by Rafi Chehirian on 6/25/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "playerBustView.h"
#import "Colors.h"
@implementation playerBustView



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self createLayout];
    }
    return self;
}




-(void)createLayout
{
    
    self.position = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, 20, 20)];
    [self.position.layer setCornerRadius:10.0f];
    [self.position.layer setBorderColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] /*#ffffff*/.CGColor];
    [self.position.layer setBorderWidth:1.0f];
    [self.position setTextColor:[UIColor whiteColor]];
    [self.position setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:self.position];
    
   self.questionType = [[UILabel alloc]initWithFrame:CGRectMake(25, 7, 150, 20)];
    [self.questionType setTextColor:SKY_BLUE];
     [self.questionType setFont: [UIFont boldSystemFontOfSize:14]];
    [self addSubview:self.questionType];
    
    
    UIView *pHolder = [[UIView alloc]initWithFrame:CGRectMake(self.frame.size.width-60, 5, 50, 55)];
    [pHolder setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.6]];
    [self addSubview:pHolder];
    
    
    UILabel *pointsL = [[UILabel alloc]initWithFrame:CGRectMake(0, pHolder.frame.size.height-14, pHolder.frame.size.width, 14)];
    [pointsL setTextAlignment:NSTextAlignmentCenter];
    pointsL.font = [ UIFont fontWithName:@"BebasNeueBold" size:14.0];
    [pointsL setText:@"POINTS"];
    [pHolder addSubview:pointsL];
    
    
    
    self.pointsAmount = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, pHolder.frame.size.width, pHolder.frame.size.height-5)];
    [self.pointsAmount setTextColor:[UIColor whiteColor]];
    [self.pointsAmount setTextAlignment:NSTextAlignmentCenter];
     self.pointsAmount.font = [ UIFont fontWithName:@"BebasNeueBold" size:30.0];
    [pHolder addSubview:self.pointsAmount];
    
    
    
    self.question = [[UITextView alloc]initWithFrame:CGRectMake(22, 20, 215, 35)];
    [self.question setBackgroundColor:[UIColor clearColor]];
    [self.question setScrollEnabled:NO];
    [self.question setTextColor:[UIColor whiteColor]];
    [self addSubview:self.question];
    
    
    UILabel *yAnswer = [[UILabel alloc]initWithFrame:CGRectMake(25, 57, 200, 15)];
    [yAnswer setTextColor:SKY_BLUE];
    [yAnswer setText:@"YOUR ANSWER"];
    [yAnswer setFont: [UIFont boldSystemFontOfSize:14]];
    [self addSubview:yAnswer];
    
    UIView *borderView = [[UIView alloc]initWithFrame:CGRectMake(0,self.frame.size.height-1, self.frame.size.width,1)];
    [borderView setBackgroundColor:[UIColor blackColor]];
    
    [self addSubview:borderView];
    
    
    self.rightWrong = [[UIImageView alloc]initWithFrame:CGRectMake(135, 52, 25, 25)];
    [self addSubview:self.rightWrong];
    [self.rightWrong setContentMode:UIViewContentModeScaleAspectFit];
    
    
    self.cilabel = [[UILabel alloc]initWithFrame:CGRectMake(165, 57, 110, 20)];
    self.cilabel.font = [ UIFont fontWithName:@"BebasNeueBold" size:14.0];
    [self addSubview:self.cilabel];
    
    
    
    self.otherInfo = [[UITextView alloc]initWithFrame:CGRectMake(22, 77, 295, 30)];
    [self.otherInfo setScrollEnabled:NO];
    [self addSubview:self.otherInfo];
    [self.otherInfo setBackgroundColor:[UIColor clearColor]];
}




-(NSAttributedString *)pointsString:(NSString *)points
{
    
    
    
    
    NSString *fullString = [NSString stringWithFormat:@" %@ POINTS",points];
    
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:fullString];
    [hogan addAttribute:NSFontAttributeName
                  value: [UIFont fontWithName:@"BebasNeueBold" size:20.0]
                  range:NSMakeRange(0,fullString.length)];
    
    [hogan addAttribute:NSForegroundColorAttributeName value:GREEN range:NSMakeRange(0, fullString.length)];
    
    
    
    return hogan;
    
    
}


-(void)addOtherStuff:(NSString *)teamName:(NSString *)points
{
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:@""];
    
    [string appendAttributedString:[self teamName:teamName]];
    
    [string appendAttributedString:[self tp:points]];
    
    [self.otherInfo setAttributedText:string];
    
}


-(NSAttributedString *)teamName:(NSString *)tn
{
    
    
    
    
    NSString *fullString = [NSString stringWithFormat:@"%@",tn];
    
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:fullString];
    
    [hogan addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, fullString.length)];
    
    
    
    return hogan;
    
    
}


-(NSAttributedString *)tp:(NSString *)tp
{
    
    
    
    
    NSString *fullString = [NSString stringWithFormat:@" %@",tp];
    
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:fullString];
    
    [hogan addAttribute:NSFontAttributeName
                  value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, fullString.length)];
    
;
    [hogan addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(0, fullString.length)];
    
    
    
    return hogan;
    
    
}


@end
