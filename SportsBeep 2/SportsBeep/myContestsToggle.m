//
//  myContestsToggle.m
//  SportsBeep
//
//  Created by Rafi Chehirian on 6/23/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "myContestsToggle.h"
#import "Colors.h"
@implementation myContestsToggle


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self createToggle];
    }
    return self;
}



-(void)createToggle
{
    self.buttons = [[NSMutableArray alloc]init];
    self.selectedIndex = 100;
    self.selectedButton = nil;
    
    NSArray *strings = [[NSArray alloc]init];
    strings = @[@"LIVE\nCONTESTS",@"UPCOMING\nCONTESTS",@"CONTESTS\nHISTORY",@"INCOMPLETE\nCONTESTS"];
    int i = 0;
    CGFloat width = (self.frame.size.width/4)-4;
    for(NSString *string in strings)
    {
        
        UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake((i* width)+(3*(i+1)), 0, width, self.frame.size.height)];
        button.layer.cornerRadius = 5.0f;
        button.titleLabel.font = [UIFont fontWithName:@"BebasNeueBold" size:14.0];
        [button setBackgroundImage:[self imageWithColor:BLUE_GRAY] forState:UIControlStateNormal];
        [button setBackgroundImage:[self imageWithColor:RED] forState:UIControlStateSelected];
        [button setTitle:string forState:UIControlStateNormal];
        [button setTag:i];
        button.titleLabel.numberOfLines = 0;
        [button.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [button setClipsToBounds:YES];
        [self addSubview:button];
        [self.buttons addObject:button];
        [button  addTarget:self action:@selector(toggleButtons:) forControlEvents:UIControlEventTouchUpInside];
        i++;
    }
    
    
 
    
}


-(void)manuallyToggleButtons:(NSInteger)index
{
    
    if(self.selectedButton)
    {
        NSInteger currentIndex = self.selectedButton.tag;
        
        if(index != currentIndex)
        {
            
            [self.selectedButton setSelected:NO];
        }
    
    }
    
    
    UIButton *button = [self.buttons objectAtIndex:index];
    [button setSelected:YES];
    self.selectedButton  = button;
    self.selectedIndex = button.tag;
}

-(void)toggleButtons:(id)sender
{
    
    UIButton *button = sender;
    if(self.selectedButton)
    {
        if(self.selectedButton != button)
        {
            
            [self.selectedButton setSelected:NO];
        }
        
    }
    
    
    [button setSelected:YES];
    self.selectedButton  = button;
    self.selectedIndex = button.tag;
    [self.delegate buttonWasSelected:button.tag];
     
     }



-(UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
@end
