//
//  MyContestsViewController.m
//  SportsBeep
//
//  Created by iOSDev on 3/30/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "MyContestsViewController.h"
#import "liveContestsViewController.h"
#import "specificContestViewController.h"
#import "DarkCover.h"


@interface MyContestsViewController (){
    NSMutableArray *vcHolder;
    DarkCover *cover;
    BOOL firstTime;
    NSInteger myHeight;
    myContestsToggle *mToggle;
}

@end

@implementation MyContestsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [bgImageView setImage:[UIImage imageNamed:@"baseball"]];
    bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    bgImageView.layer.zPosition = -1;
    [self.view addSubview:bgImageView];
    
     mToggle = [[myContestsToggle alloc]initWithFrame:CGRectMake(1, 68, self.view.frame.size.width-1, 40)];
    [mToggle setDelegate:self];
    [mToggle manuallyToggleButtons:0];
    [self.view addSubview:mToggle];
    
    
    UILabel *headLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 50)];
    [headLabel setBackgroundColor:DARK_BLUE];
    [headLabel setTextColor:[UIColor whiteColor]];
    [headLabel  setFont: [UIFont fontWithName:@"BebasNeueBold" size:30.0]];
    [headLabel  setTextAlignment:NSTextAlignmentCenter];
    [headLabel setText:@"MY CONTESTS"];
    [self.view addSubview:headLabel];
    
    
    
    [self addCoverAndMenuBox];
    
    myHeight = 0;
    firstTime = YES;
    
    //holds reference to all the vcs so they do not get put in garbage
    vcHolder = [[NSMutableArray alloc]init];
       CGRect main =    [[UIScreen mainScreen] bounds];

    [self.scrollView setContentSize:CGSizeMake(main.size.width *4,self.scrollView.frame.size.height)];
    [self.scrollView  setPagingEnabled:YES];
    [self.scrollView setDelegate:self];
    self.holderViewArray = [[NSMutableArray alloc]init];
    
    NSArray *randColors = [[NSArray alloc]init];
    randColors = @[
                   [UIColor greenColor],
                   [UIColor redColor],
                   [UIColor blueColor],
                   [UIColor orangeColor],];
    
       [self.mLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:32.0]];
    

     [self makeSubs];
    
    
    // black container box
    UIView *containerBox = self.blackContainerBox;
    containerBox.backgroundColor = [UIColor blackColor];
    
    // button
    UIButton *menuBox = self.menuBox;
    menuBox.backgroundColor = GREEN;
    
    CGFloat totalBlankSpace = 100.f;
    CGFloat buttonsSpace = self.view.frame.size.width - totalBlankSpace;
    
    CGFloat buttonWidth = buttonsSpace/4;
    
    CGRect buttonFrame = self.liveContestsButton.frame;
    
    buttonFrame.size = CGSizeMake(buttonWidth, 40);
    
    NSLog(@"button width: %f",buttonWidth);

    
    

}

-(void)viewWillAppear:(BOOL)animated
{

}

- (IBAction)liveContests:(id)sender {
    int configuration = 0;
    [self updateButtonColors:configuration];
    self.liveContestsButton.backgroundColor = RED;
    [self setScrollOffset:configuration];
   // [self displayContests:@"live"];
    
}
- (IBAction)upcomingContests:(id)sender {
    int configuration = 1;
    [self updateButtonColors:configuration];
    [self setScrollOffset:configuration];
    //[self displayContests:@"upcoming"];

}
- (IBAction)contestsHistory:(id)sender {
    int configuration = 2;
    [self updateButtonColors:configuration];
    [self setScrollOffset:configuration];
  //  [self displayContests:@"past"];

}
- (IBAction)incompleteContests:(id)sender {
    int configuration = 3;
    [self updateButtonColors:configuration];
    [self setScrollOffset:configuration];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateButtonColors: (int) configuration {
    NSArray *buttonArray = [NSArray arrayWithObjects:self.liveContestsButton, self.upcomingContestsButton,self.contestsHistoryButton,self.incompleteContestsButton, nil];
    NSInteger buttonCount = buttonArray.count;
    for (int i = 0; i<buttonCount; i++) {
        UIButton *currentButton = buttonArray[i];
        if (i == configuration){
            currentButton.backgroundColor = RED;
        } else {
            currentButton.backgroundColor = BLUE_GRAY;
        }
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    
    NSLog(@"appearing");
    if(firstTime)
    {
        firstTime = NO;
    
    
        
    }
    if(myHeight == 0)
    {
        myHeight = self.scrollView.frame.size.height+18;
    }
    
   [self.scrollView setFrame:CGRectMake(0, self.scrollView.frame.origin.y, self.view.frame.size.width, myHeight)];
}

-(void)makeSubs
{
    
     CGRect main =    [[UIScreen mainScreen] bounds];
    for(int i = 0;i<4;i++)
    {
        UIView *holderView = [[UIView alloc]initWithFrame:CGRectMake(main.size.width * i, 0, main.size.width, main.size.height-175)];
        [holderView setBackgroundColor:[UIColor clearColor]];
        [self.scrollView addSubview:holderView];
        [self.holderViewArray addObject:holderView];
    }
    
    //[self.scrollView setFrame:CGRectMake(0, self.scrollView.frame.origin.y, self.view.frame.size.width, self.scrollView.frame.size.height+10)];
    
    NSArray *status= [[NSArray alloc]init];
    status = @[@"live",@"upcoming",@"past",@"abcd"];
    int i = 0;
    for(UIView *v in self.holderViewArray)
    {
        liveContestsViewController *lc = [self.storyboard instantiateViewControllerWithIdentifier:@"LiveContests"];
        lc.gameStatus = [status objectAtIndex:i];
        [lc.view setFrame:v.bounds];
        lc.position = i;
        [v addSubview:lc.view];
        [lc setDelegate:self];
        [vcHolder addObject:lc];
        [lc.view setBackgroundColor:[UIColor clearColor]];
        [lc.tableView setBackgroundColor:[UIColor clearColor]];
        [lc didMoveToParentViewController:self];
     //   [lc.tableView setFrame:CGRectMake(lc.tableView.frame.origin.x, lc.tableView.frame.origin.y, main.size.width, 22)];
        i++;
    }
    
    
}

-(void)setScrollOffset:(int)pos
{
    self.currentPage = pos;
    [self.scrollView setContentOffset:CGPointMake(self.scrollView.frame.size.width * pos, 0) animated:YES];
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    self.currentPage = page;
 
    
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [mToggle manuallyToggleButtons:self.currentPage];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
     [mToggle manuallyToggleButtons:self.currentPage];
}


-(void) displayContests : aStatus {
    NSString *url = @"http://sportsbeep.com//api/mlb/contests";
    
    NSDictionary *params = @{
                             @"status":aStatus
                             };
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults stringForKey:@"authentication_token"];
    NSString *tokenString = [NSString stringWithFormat:@"Token token=%@", token];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:tokenString forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer = requestSerializer;
    
    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response Code %ld",(long)[operation.response statusCode]);
        NSLog(@"JSON: %@", responseObject);
        
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //NSString *errorMessage = operation.responseObject[@"error"];
        NSLog(@"upcoming contests error");
    }];
    
}


//live contests delegate
-(void)contestWasSelected:(individualGame *)g
{
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    specificContestViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"specificContest"];
    [self presentViewController:vc animated:YES completion:nil];
    [vc addGame:g];
}

//end delegate

-(void) addCoverAndMenuBox {
    
    //cover
    CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    self->cover = [[DarkCover alloc] initWithFrame:frame];
    self->cover.hidden = YES;
    [self.view addSubview:self->cover];
    
    NSArray *titles = [[NSArray alloc]init];
    titles = @[@"PICK A GAME",@"MY CONTESTS",@"CHANGE SPORT",@"PROFILE",@"HOW TO PLAY",@"SHARE",@"LOGOUT"];
    
    
    CGFloat btnHeight = 60;
    self.mSlideUpMenu = [[MenuBox alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - btnHeight, self.view.frame.size.width,btnHeight * ([titles count] + 1))];
    
    [self.mSlideUpMenu setDelegate:self];
    [self.view setClipsToBounds:YES];
    
    [self.view addSubview:self.mSlideUpMenu];
    
    [self.mSlideUpMenu setBackgroundColor: [UIColor colorWithRed:0.18 green:0.369 blue:0.651 alpha:1]];
    
    
    self.mSlideUpMenu.buttonTitles = titles;
    [self.mSlideUpMenu addButtons];
}

//delegate methods slide up menu
-(void)slideUpMenuButtonPressed:(UIButton*)button
{
    NSInteger tag = button.tag;
    //from here you can do some actions based on the button pressed
    switch (tag) {
        case 1:
            [MenuBox goToSelectedOption:@"SecondVC" calledByVC:self];
            break;
        case 2:
            break;
        case 3:
            [MenuBox goToSelectedOption:@"NewGame" calledByVC:self];
            break;
        case 4:
            [MenuBox goToSelectedOption:@"Profile" calledByVC:self];
            break;
        case 5:
            [MenuBox goToSelectedOption:@"HowToPlay" calledByVC:self];
            break;
        case 6:
            NSLog(@"Share");
            break;
        case 7:
            NSLog(@"LOGOUT");
            break;
            
        default:
            break;
    }
}


//cancel was pressed
-(void)cancelButtonWasPressed
{
    
}
//will close
-(void)drawerWillClose
{
    self->cover.hidden = YES;
}
//did close
-(void)drawerDidClose
{
    self->cover.hidden = YES;
}
//will open
-(void)drawerWillOpen
{
    self->cover.hidden = NO;
}
//opened
-(void)drawerDidOpen
{
    self->cover.hidden = NO;
}
//drawer dragging add the barrier view
-(void)DrawerWillDrag
{
    if (self->cover.hidden == NO) {
        self->cover.hidden = YES;
    }
    if (self->cover.hidden == YES) {
        self->cover.hidden = NO;
    }
    
}



-(void)buttonWasSelected:(NSInteger)index
{
    
    [self setScrollOffset:index];
    
}


@end
