//
//  teamRectangleView.h
//  SportsBeep
//
//  Created by Daniel Nasello on 6/1/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface teamRectangleView : UIView



@property(nonatomic,retain)NSString *homeTeamId;
@property(nonatomic,retain)NSString *awayTeamId;

@property(nonatomic,strong)UIView *homeTeamView;
@property(nonatomic,strong)UILabel *homeTeamLabel;
@property(nonatomic,strong)UILabel *homeCityLabel;

@property(nonatomic,strong)UIView *awayTeamView;
@property(nonatomic,strong)UILabel *awayTeamLabel;
@property(nonatomic,strong)UILabel *awayCityLabel;

-(void)addGameRectanges :(NSDictionary*)homeTeam :(NSDictionary *)awayTeam :(CGFloat)cellWidth :(NSString *)date :(NSString *)time;
@end
