//
//  ViewController.m
//  SportsBeep
//
//  Created by iOSDev on 3/24/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "SignInViewController.h"
#import "Colors.h"
#import "LoginManager.h"
#import "CreateAccountViewController.h"
#import "HighlightableTextField.h"

@interface SignInViewController () {
    LoginManager *lim;
}

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    [self initiateObjectSettings];
    
    // check defaults for username and password
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *email = [defaults objectForKey:@"email"];
    NSString *password = [defaults objectForKey:@"password"];
    
    NSLog(@"%@, %@",email, password);
    
    // auto login
    //if (![email isEqual:@""] && ![password isEqual:@""]) {
    if (email && password) {
        NSLog(@"in auto login");
        
        if(!lim)
        {
            lim = [[LoginManager alloc]init];
            lim.delegate = self;
        }
        
        [lim login:email withPassword:password];
        
    } else {
        NSLog(@"no defaults");
    }
    
    
    
}

// Set inital settings for view controller
-(void) initiateObjectSettings {
    
    // add main background image to the image view
    UIImage *bgImage = [UIImage imageNamed:@"baseball.png"];
    [self.mainBG setImage: bgImage];
    self.mainBG.contentMode = UIViewContentModeScaleAspectFill;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"baseball.png"]];
    
    // blue container box
    self.blueBgBox.backgroundColor = PANEL_BG_BLUE;
    
    
    // email field add left image and set field properties
    UIImageView *envelope = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"email_icon"]];
    [envelope setFrame:CGRectMake(0, 0, 55, (self.emailField.frame.size.height)*0.7)];
    envelope.layer.masksToBounds = YES;
    [envelope setContentMode: UIViewContentModeScaleAspectFit];
    [self.emailField setLeftViewMode:UITextFieldViewModeAlways];
    [self.emailField setLeftView: envelope];
    self.emailField.rightViewMode = UITextFieldViewModeAlways;
    self.emailField.layer.borderWidth = 1.0f;
    self.emailField.layer.borderColor = [[UIColor blackColor] CGColor];
    self.emailField.backgroundColor = BLUE_GRAY;
    self.emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    
    
    // password field add left image and set field properties
    UIImageView *lock = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password_icon.png"]];
    [lock setFrame:CGRectMake(0, 0, 55, (self.emailField.frame.size.height)*0.7)];
    lock.layer.masksToBounds = YES;
    [lock setContentMode: UIViewContentModeScaleAspectFit];
    self.passwordField.layer.borderWidth = 1.0f;
    [self.passwordField setLeftViewMode:UITextFieldViewModeAlways];
    [self.passwordField setLeftView: lock];
    self.passwordField.backgroundColor = BLUE_GRAY;
    self.passwordField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    
    
    // sign in button
    self.signInButton.backgroundColor = RED;
    
    // create account button
    self.createAccountButton.backgroundColor = BLUE;
    
    lim = nil;
    
}

// Validate Form
-(BOOL) formIsValid {
    
    NSString *email = self.emailField.text;
    NSString *password = self.passwordField.text;
    
    if ([email  isEqual: @""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"EMAIL" message:@"Email field is required." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    if ([password  isEqual: @""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"PASSWORD" message:@"Password field is required." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    return YES;
}

// Logging In
-(void)loginManagerLogin
{
    if(!lim)
    {
        lim = [[LoginManager alloc]init];
        lim.delegate = self;
    }
    
    NSString *email = self.emailField.text;
    NSString *password = self.passwordField.text;
    
    [lim login:email withPassword:password];
    
}

// Forgot Password
-(void)loginManagerForgotPassword
{
    if(!lim)
    {
        lim = [[LoginManager alloc] init];
        lim.delegate = self;
    }
    [lim forgotPassword:self.emailField.text];
}

// Incorrect login
-(void)PushLoginError: (NSString*) errorMessage {
    
    lim = nil;
    
    
    // create and display toast error message
    ToastMessage *locToastMessage = [[ToastMessage alloc] initWithMessage:errorMessage andWithRequestingView: self.view];
    [locToastMessage displayToastMessage];
    
}

// Correct Login
-(void)PushUpdatedLogin
{
    // Log in Manager Variable
    lim = nil;
    
    // present New Game View Controller
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"NewGame"];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self.emailField setText:@""];
    [self.passwordField setText:@""];
    [self dismissViewControllerAnimated:NO completion:nil];
    [self presentViewController:vc animated:YES completion:NULL];
    
}



// Forgot password errror occured
-(void)forgotPasswordError: (NSString*) errorMessage {
    
    // Login Manager variable
    lim = nil;
    
    // login error alert
    ToastMessage *locToastMessage = [[ToastMessage alloc] initWithMessage:errorMessage andWithRequestingView: self.view];
    [locToastMessage displayToastMessage];
    
}

// Correct Password reset
-(void)forgotPasswordSuccess
{
    // Log in Manager Variable
    lim = nil;
    NSString *resetMessage = @"Your password has been reset. Please check your email for further instructions";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"RESET SUCCESS" message:resetMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    // present New Game View Controller
    
}

//Pressing the Forgot Password Button
- (IBAction)forgotPassword:(id)sender {
    
    [self loginManagerForgotPassword];
}

// Pressing the Sign In Button
-(IBAction)signInButton:(id)sender {
    
    [self.view endEditing:YES];
    
    [self loginManagerLogin];
    
    
}

// Touching the screen
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // dismiss the keyboard when user touches anywhere on the screen
    [self.view endEditing:YES];
}

// Pressing the Return Button on the keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    // pressing the return key
    [self.view endEditing:YES];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)toastMessage:(NSString *)aErrorMessage {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color = DARK_BLUE;
    
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.labelText = aErrorMessage;
    hud.xOffset = 0;
    hud.yOffset = 0;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:3];
}

@end
























