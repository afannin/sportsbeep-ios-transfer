//
//  PersonalFavoriteViewController.h
//  Pods
//
//  Created by iOSDev on 3/26/15.
//
//

#import <UIKit/UIKit.h>

@interface PersonalFavoriteViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIImageView *mainBG;
@property (weak, nonatomic) IBOutlet UITextField *personalFavoriteField;
@property (weak, nonatomic) IBOutlet UIView *blueBgBox;
@property (weak, nonatomic) IBOutlet UIButton *setPersonalFavoritesButton;
- (IBAction)setPersonalFavoritesButton:(id)sender;


@end
