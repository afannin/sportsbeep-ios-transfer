//
//  PlayerBustsController.h
//  SportsBeep
//
//  Created by Daniel Nasello on 6/1/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//
#import "AFNetworking.h"
#import <UIKit/UIKit.h>
#import "teamRectangleView.h"
#import "CSLinearLayoutView.h"
#import "playerBustView.h"
#import "Colors.h"


@interface PlayerBustsController : UIViewController
-(void)addTopView:(teamRectangleView *)tr;
@property (weak, nonatomic) IBOutlet UILabel *mHeadLabel;
- (IBAction)dismissB:(id)sender;
-(void)getBestBusts:(NSString *)tid;

@property(nonatomic,strong)NSDictionary *homeDict;
@property(nonatomic,strong)NSDictionary *awayDict;

@property(nonatomic,retain)teamRectangleView *rView;
@end
