//
//  Timer.m
//
//  Created by Daniel Nasello on 9/11/14.
//  Copyright (c) 2014 Combustion Innovation Group. All rights reserved.
//

#import "Timer.h"

@implementation Timer




- (void) startTimer
{
    //self.countdownAmount = 45;
    self.timer =   [NSTimer scheduledTimerWithTimeInterval:1
                                                    target:self
                                                  selector:@selector(tick:)
                                                  userInfo:nil
                                                   repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

- (void) tick:(NSTimer *) timer {
    //do something here..
    
    self.countdownAmount--;
    [self.delegate ptimerHasTicked:[self formatZeros:self.countdownAmount]];
    if(self.countdownAmount <1)
    {
        [self timerHasFailed];
    }
    
}
-(NSString *)formatZeros:(NSInteger)number
{
    NSString *val = @"0";
    if(number < 10)
    {
        val  = [NSString stringWithFormat:@"%02ld", (long)number];
    }
    else
    {
        val =[NSString stringWithFormat:@"%ld", (long)number];
    }
    
    return val;
}

-(void)endTimer
{
    [self.timer invalidate];
    self.countdownAmount = 45;
}

-(void)timerHasFailed
{
    [self endTimer];
    [self.delegate prettyTimerHasEnded];
    self.countdownAmount = 45;
}


-(void)pauseTimer
{
    
}
@end