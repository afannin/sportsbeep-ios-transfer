//
//  DragButton.m
//  SportsBeep
//
//  Created by iOSDev on 5/29/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "DragButton.h"

@implementation DragButton



-(void)addRecognizer
{
    //set a fake last location. fixes a weird drag bug
    self.lastLocation = CGPointMake(-1,-1);
    //add a pan recognizer to the mix
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(detectPan:)];
    [panRecognizer setDelegate:self];
    [self addGestureRecognizer:panRecognizer];
}


- (void) detectPan:(UIPanGestureRecognizer *) uiPanGestureRecognizer
{
    
    
    //if the state has began tell the drawer we will move
    if(uiPanGestureRecognizer.state == UIGestureRecognizerStateBegan)
    {
        //tell the delegate
        [self.delegate gesturHasStarted:self.lastLocation];
        
        
    }
    else if(uiPanGestureRecognizer.state == UIGestureRecognizerStateCancelled || uiPanGestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        CGPoint translation = [uiPanGestureRecognizer translationInView:self.superview.superview];
        CGPoint p= CGPointMake(self.lastLocation.x + translation.x,
                               self.lastLocation.y + translation.y);
        //if we have ended
        //tell the delegate so it can close or open the drawer
        [self.delegate gestureHasEnded:p];
        
    }
    
    //fixes point of location
    if(self.lastLocation.x == -1)
    {
        CGPoint location = [uiPanGestureRecognizer locationInView:[uiPanGestureRecognizer.view superview]];
        self.lastLocation = location;
        
    }
    //if we arent in cancel mode and we are moving...
    else if(uiPanGestureRecognizer.state != UIGestureRecognizerStateCancelled && uiPanGestureRecognizer.state != UIGestureRecognizerStateEnded)
    {
        
        //translation is the translation in view COMPARED TO THE DRAWERS SUPERVIEW  which is the view controller.
        CGPoint translation = [uiPanGestureRecognizer translationInView:self.superview.superview];
        //make a point based on this.
        CGPoint p= CGPointMake(self.lastLocation.x + translation.x,
                               self.lastLocation.y + translation.y);
        //tell the delegate so we can move the drawer.
        [self.delegate gestureHasMoved:p];
        
    }
}


@end
