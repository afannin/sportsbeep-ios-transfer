//
//  sports.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/1/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface sports : NSObject

//will return the sports level pending on the index
-(NSString *)getSportsType:(NSInteger)type;

//will give the sport pending on the index
-(NSString *)getSport:(NSInteger)sport;


//upcoming or all events for a  sport
-(NSString *)getIsUpcomingGames:(NSInteger)isFuture;


@end
