//
//  ToastMessage.h
//  SportsBeep
//
//  Created by iOSDev on 4/10/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MBProgressHud.h"
#import "Colors.h"

@interface ToastMessage : NSObject

@property NSString *mErrorMessage;
@property UIView *mRequestingView;
@property MBProgressHUD *hud;

- (id)initWithMessage: (NSString*)aErrorMessage andWithRequestingView: (UIView*)aRequestingView;
-(void)displayToastMessage;

@end
