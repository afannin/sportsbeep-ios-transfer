//
//  CreateAccountViewController.h
//  SportsBeep
//
//  Created by iOSDev on 3/26/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ToastMessage.h"

@interface CreateAccountViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *mainBG;
@property (weak, nonatomic) IBOutlet UITextField *createUserNameField;
@property (weak, nonatomic) IBOutlet UITextField *createEmailField;
@property (weak, nonatomic) IBOutlet UITextField *createPasswordField;
@property (weak, nonatomic) IBOutlet UITextField *enterPromoCodeField;
@property (weak, nonatomic) IBOutlet UIView *blueBgBox;
@property (weak, nonatomic) IBOutlet UIButton *createAccountButton;
- (IBAction)createAccountButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
- (void) loginManagerCreateAccount: (NSString*) username withEmail: (NSString*) email withPassword: (NSString*) password;
-(void)PushLoginError: (NSString*) errorMessage;

@end
