//
//  QuizViewController.m
//  SportsBeep
//
//  Created by iOSDev on 5/25/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "QuizAlertViewController.h"
#import "Colors.h"
#import "Timer.h"

@interface QuizAlertViewController (){
    Timer *timer;
    NSArray *quizQuestions;
}

@end

@implementation QuizAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    quizQuestions = [[NSArray alloc]init];
    
    //set background image
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"baseball.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:image]];
    
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initializeAlertView {
    

    
    //initialize alert text label
    self.textLabel.numberOfLines = 0;
    self.textLabel.text = @"ANSWER QUESTIONS WITHIN 14 SECONDS. THE FASTER YOU ANSWER THE MORE POINTS YOU SCORE!";
    [self.textLabel setTextAlignment: NSTextAlignmentCenter];
    self.textLabel.layer.shadowRadius = 3.0;
    self.textLabel.layer.shadowOpacity = 0.5;
    self.textLabel.layer.masksToBounds = NO;
    
    //initialize countdown label
    self.colorLabel.clipsToBounds = YES;
    self.colorLabel.layer.cornerRadius = 5;
    [self.colorLabel setTextAlignment: NSTextAlignmentCenter];
    self.colorLabel.textColor = [UIColor whiteColor];
    [[self colorLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:26]];
    
    //initialize and start timer
    timer = [[Timer alloc] init];
    timer.countdownAmount = 4;
    timer.delegate = self;
    [timer startTimer];
}

- (void) initializeQuiztView {

}


-(void)prettyTimerHasEnded
{
    //countdown has ended present quiz
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    QuizViewController *vc = [sb instantiateViewControllerWithIdentifier:@"Quiz"];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.homeTeamObject = self.homeTeamObject;
    vc.awayTeamObject = self.awayTeamObject;
    vc.ticket_id = self.ticket_id;
    //[self.modalViewController dismissViewControllerAnimated:NO completion:nil];
    vc.quizQuestions = quizQuestions;
    vc.indieGame = self.indieGame;
    [self presentViewController:vc animated:YES completion:nil];
    //[self presentViewController:vc animated:YES completion:NULL];
    [vc addGameTriangle:self.indieGame];
    [vc startQuiz];
}



-(void)getQuizQuestions:(NSString *)ticket_id
{
    NSString *url = [NSString stringWithFormat: @"http://sportsbeep.com/api/mlb/tickets/%@/questions",ticket_id];
    
    
    NSLog(@"the url is %@",url);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *token = [defaults stringForKey:@"authentication_token"];
    
    NSString *tokenString = [NSString stringWithFormat:@"Token token=%@", token];
    NSDictionary *dict =@{
                          @"ticket_id":[NSString stringWithFormat:@"%@",ticket_id],
                          };
    
    
    NSLog(@"the full dict to send is %@",dict);
    
    NSDictionary *params = @{
                             @"":dict,
                             };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:tokenString forHTTPHeaderField:@"Authorization"];
     manager.requestSerializer = requestSerializer;
    
    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"question response: %@", responseObject);
        
        quizQuestions = [responseObject objectForKey:@"questions"];
        
        
          [self initializeAlertView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorMessage = operation.responseObject[@"error"];
        NSLog(@"error adding pool %@",errorMessage);
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    
    
}




-(void)ptimerHasTicked:(NSString*)timeLeft;
{
    // timer has ticked change label color and text
    self.colorLabel.text = timeLeft;
    
    NSArray *locMessageArray = @[@"DONE",@"GO!",@"SET",@"READY"];
    
    NSInteger locTime = [timeLeft integerValue];
    
    switch (locTime) {
        case 3:
            self.colorLabel.backgroundColor = RED;
            self.colorLabel.text = locMessageArray[locTime];
            break;
            
        case 2:
            self.colorLabel.backgroundColor = ORANGE;
            self.colorLabel.text = locMessageArray[locTime];
            break;
            
        case 1:
            self.colorLabel.backgroundColor = RED;
            self.colorLabel.text = locMessageArray[locTime];
            break;
            
        case 0:
            break;
            
        default:
            break;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
