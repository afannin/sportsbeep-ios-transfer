//
//  sportTab.h
//  SportsBeep
//
//  Created by iOSDev on 5/21/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface sportTab : UIView


@property(nonatomic,strong)UIView *topSpacerView;

@property(nonatomic,strong)UIImageView *iconView;


@property(nonatomic,strong)UILabel *sportName;

@property(nonatomic,assign)BOOL canClick;
@property(nonatomic,assign)BOOL isSelected;
-(void)setFakeDisabled;
-(void)setFakeEnabled;
-(void)setFakeSelected;
-(void)setFakeUnselected;
@end
