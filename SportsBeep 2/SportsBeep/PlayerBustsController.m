//
//  PlayerBustsController.m
//  SportsBeep
//
//  Created by Daniel Nasello on 6/1/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "PlayerBustsController.h"
#import "Colors.h"

@interface PlayerBustsController (){
        CSLinearLayoutView *linearLayout;
    }

@end

@implementation PlayerBustsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mHeadLabel.backgroundColor = DARK_BLUE;
    [self.mHeadLabel  setFont: [UIFont fontWithName:@"BebasNeueBold" size:30.0]];
    
    
    self.view.backgroundColor = PANEL_BG_BLUE;
    
    
    linearLayout = [[CSLinearLayoutView alloc]initWithFrame:CGRectMake(0, 125, self.view.frame.size.width, self.view.frame.size.height-125)];

    [self.view addSubview:linearLayout];
    
    
    UILabel *scoringBreakdown =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-20, 50)];
    [scoringBreakdown setBackgroundColor:BLUE];
    [scoringBreakdown setText:@"SCORING BREAKDOWN"];
    [scoringBreakdown setTextColor:[UIColor whiteColor]];
    [scoringBreakdown setFont:[UIFont fontWithName:@"BebasNeueBold" size:30.0]];
    [scoringBreakdown setTextAlignment:NSTextAlignmentCenter];
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:scoringBreakdown.bounds
                                     byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                           cornerRadii:CGSizeMake(3.0, 3.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = scoringBreakdown.bounds;
    maskLayer.path = maskPath.CGPath;
    scoringBreakdown.layer.mask = maskLayer;
    
    CSLinearLayoutItem *i3 = [[CSLinearLayoutItem alloc]initWithView:scoringBreakdown];
    i3.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
    
    [linearLayout addItem:i3];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addTopView:(teamRectangleView*)tr
{
    [tr setFrame:CGRectMake(tr.frame.origin.x, 60, tr.frame.size.width, tr.frame.size.height)];
    [self.view addSubview:tr];
    
    self.rView = (teamRectangleView  *)tr;

}

- (IBAction)dismissB:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)getBestBusts:(NSString *)tid
{
    
    CGFloat statWid = self.view.frame.size.width*.6;
    CGFloat otherWid = self.view.frame.size.width*.2;
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults stringForKey:@"authentication_token"];
    NSString *tokenString = [NSString stringWithFormat:@"Token token=%@", token];
    NSString *url =[NSString stringWithFormat:@"http://sportsbeep.com/api/mlb/tickets/%@/answers",tid];
    
    NSDictionary *dict =@{
                 
                          @"ticket_id":tid,
                          };
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:tokenString forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer = requestSerializer;
    
    [manager GET:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        int i = 0;
        NSArray *answers = [responseObject objectForKey:@"answers"];
        
        for(NSDictionary *dict in answers)
        {
            
            NSString *correct = [NSString stringWithFormat:@"%@",[dict objectForKey:@"correct"]];
            NSDictionary *question = [dict objectForKey:@"question"];
            NSString *text = [question objectForKey:@"text"];
            NSString *type = [question objectForKey:@"type"];
            
            
            NSString *score =[NSString stringWithFormat:@"%@",[dict objectForKey:@"score"]];
            NSString *time =[NSString stringWithFormat:@"%@",[dict objectForKey:@"time"]];
            
     
            
              NSDictionary *team =[dict objectForKey:@"team"];
             NSString *teamId =[NSString stringWithFormat:@"%@",[team objectForKey:@"id"]];
            
            NSString *myTeam = @"t";
            
            NSLog(@"the team dict = %@",teamId);
            
           if([teamId isEqualToString:[NSString stringWithFormat:@"%@",[self.homeDict objectForKey:@"id"]]])
           {
                myTeam = [self.homeDict objectForKey:@"name"];
            }
            else
            {
                myTeam = [self.awayDict objectForKey:@"name"];
           }
            
            playerBustView *v = [[playerBustView alloc]initWithFrame:CGRectMake(5, 0, self.view.frame.size.width-5, 150)];
            
            i++;
            
            if([correct isEqualToString:@"0"])
            {
                [v.rightWrong setImage:[UIImage imageNamed:@"red_x"]];
                [v.cilabel setText:@"INCORRECT"];
                [v.cilabel setTextColor:RED];
            }
            else
            {
                [v.rightWrong setImage:[UIImage imageNamed:@"green_check"]];
                [v.cilabel setText:@"CORRECT"];
                [v.cilabel setTextColor:GREEN];
            }
            
            
            [v addOtherStuff:myTeam :[NSString stringWithFormat:@"in %@ seconds",time]];
            
            [v.question setText:text];
            
            
            NSString *pos = [NSString stringWithFormat:@"%d",i];

            [v.position setText:pos];
            
            [v.questionType setText:@"BEST QUESTION"];
            if([type isEqualToString:@"bust"])
            {
                [v.questionType setText:@"BUST QUESTION"];
            }
            
            
            CSLinearLayoutItem *i3 = [[CSLinearLayoutItem alloc]initWithView:v];
            i3.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
            [v setBackgroundColor:PANEL_BG_BLUE];
            
            
            [v.pointsAmount setText:score];
            
            [linearLayout addItem:i3];
            
       
        }
        
        
        
        //CSLinearLayoutItem * myi = [self addTotalScore];
       // [linearLayout addItem:myi];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorMessage = operation.responseObject[@"error"];
        
        NSLog(@"err: %@", errorMessage);
        
    }];
}

@end
