//
//  contestPlayerView.h
//  SportsBeep
//
//  Created by Daniel Nasello on 5/30/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Colors.h"
@interface contestPlayerView : UIView


@property(nonatomic,strong)UIView *topView;
@property(nonatomic,strong)UILabel *placeLabel;
@property(nonatomic,strong)UILabel *userLabel;
@property(nonatomic,strong)UILabel *totalLabel;
@property(nonatomic,strong)UIImageView *icon;
@property(nonatomic,strong)NSDictionary *playerDict;
-(void)addUandP:(NSString *)username:(NSString *)place;
-(void)addPoints:(NSString *)points;
@end
