//
//  getGamesProtocol.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/1/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol getGamesProtocol <NSObject>



//the games has started to fetch the web data
-(void)getGamesStartedToFetchGames;

//get games has failed
-(void)getGamesFailedWithError:(NSString *)error;

//brings back the type of game, the level of game, and the object that has games in it.
-(void)getGamesHasFetchedGames :(NSInteger)gameType :(NSInteger)gameLevel :(NSDictionary*)gameObject;



@end
