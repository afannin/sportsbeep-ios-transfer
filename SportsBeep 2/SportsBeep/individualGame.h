//
//  individualGame.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/1/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TimeConversion.h"
@interface individualGame : NSObject


//object that holds all of the home team information
@property(nonatomic,strong)NSDictionary *homeTeamObject;
//object that holds all of the away team information
@property(nonatomic,strong)NSDictionary *awayTeamObject;

//the pool fee id and prize
@property(nonatomic,strong)NSDictionary *pool;

//the game id
@property(nonatomic,strong)NSString *gameId;

//user object of the person who created this.
@property(nonatomic,strong)NSDictionary *creatorUser;

//some ticket object
@property(nonatomic,strong)NSDictionary *ticket;

//date of the game
@property(nonatomic,strong)NSString *date;


-(void)changeDateToRightDate:(NSDate *)date;


@property(nonatomic,strong)NSString *gameDay;
@property(nonatomic,strong)NSString *gameTime;


@property(nonatomic,strong) NSString *homeTeamName;
@property(nonatomic,strong) NSString *homeTeamColor;

@property(nonatomic,strong)NSString *awayTeamName;
@property(nonatomic,strong)NSString *awayTeamColor;


@property(nonatomic,strong)NSString *contestID;


@property(nonatomic,strong) NSAttributedString *placingString;

@end

