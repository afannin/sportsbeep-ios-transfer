//
//  specificContestViewController.m
//  SportsBeep
//
//  Created by Daniel Nasello on 5/30/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "specificContestViewController.h"
#import "CSLinearLayoutView.h"

@interface specificContestViewController (){
    CSLinearLayoutView *linearLayout;
    NSMutableArray *players;
    UIView *statsHolder;
}

@end

@implementation specificContestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [bgImageView setImage:[UIImage imageNamed:@"baseball"]];
    bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    bgImageView.layer.zPosition = -1;
    [self.view addSubview:bgImageView];
    
    players = [[NSMutableArray alloc]init];
    
    
    statsHolder = [[UIView alloc]initWithFrame:CGRectMake(0, 220, self.view.frame.size.width,20)];
    [statsHolder setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:statsHolder];
    
    
    linearLayout = [[CSLinearLayoutView alloc]initWithFrame:CGRectMake(0, 250, self.view.frame.size.width, self.view.frame.size.height-250)];
    [self.view addSubview:linearLayout];
    [linearLayout setBackgroundColor:[UIColor clearColor]];
}


-(void)addGame:(individualGame *)game
{
    
    self.myGame = game;
    
    bigTriangleHolder *th =[[bigTriangleHolder alloc]initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, 140)];
    
    [th addGameTriangles:game.awayTeamObject :game.homeTeamObject :self.view.frame.size.width :game.gameTime :game.gameDay];
    
    [self.view addSubview:th];
    
    [self getGamePlayers:game.contestID];
    
    [self.nsLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:30.0f]];
    self.nsLabel.backgroundColor = DARK_BLUE;
}

- (IBAction)doneButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)getGamePlayers:(NSString *)gameID
{
    NSString *url =[NSString stringWithFormat:@"http://sportsbeep.com/api/mlb/contests/%@/tickets",gameID];
    
    NSLog(@"game id %@",gameID);
    NSLog(@"%@",url);
    
    NSDictionary *dict =@{
                          //  @"status":[NSString stringWithFormat:self.gameTimeType],
                          @"status":@"upcoming",
                          };
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults stringForKey:@"authentication_token"];
    NSString *tokenString = [NSString stringWithFormat:@"Token token=%@", token];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:tokenString forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer = requestSerializer;
    
    [manager GET:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        
        
        NSArray *array = [responseObject objectForKey:@"tickets"];
        
          NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *u_name =[defaults objectForKey:@"username"];
        
        int i = 0;
        
        for(NSDictionary *dict in array)
        {
            
            NSString *place = [dict objectForKey:@"placing"];
             NSString *prize_pool = [dict objectForKey:@"prize_pool"];
            NSString *score = [dict objectForKey:@"score"];
            NSDictionary *user = [dict objectForKey:@"user"];
            
            NSString *userName = [user objectForKey:@"username"];
            NSString *avatar = [user objectForKey:@"avatar_url"];
            NSString *gid = [dict objectForKey:@"id"];
            
            
          //  -(void)addUandP:(NSString *)username:(NSString *)place
            //-(void)addPoints:(NSString *)points;
            if([u_name isEqualToString:userName])
            {
                [self addTopString:place:prize_pool:score];
            }
            
            contestPlayerView *pv = [[contestPlayerView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
            pv.playerDict = dict;
            
            [pv addUandP:userName :[NSString stringWithFormat:@"%@",place]];
            [pv addPoints:[NSString stringWithFormat:@"%@",score]];
            UITapGestureRecognizer *playerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playerTapped:)];
            [pv addGestureRecognizer:playerTap];
            
            [pv.icon sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",avatar]]
                       placeholderImage:[UIImage imageNamed:@"player_silhouette"]];
            
            
            CSLinearLayoutItem *item = [CSLinearLayoutItem layoutItemForView:pv];
            [linearLayout addItem:item];
            
            [players addObject:pv];
            
            pv.restorationIdentifier = gid;
            
            
            i++;
            
        }
        
        
        
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorMessage = operation.responseObject[@"error"];
        
        NSLog(@"err: %@", errorMessage);
        
    }];
    
}

-(void)addTopString:(NSString *)place:(NSString *)pool:(NSString *)score
{
    CGFloat locScore = [score floatValue];
    UILabel *l = [[UILabel alloc]initWithFrame:statsHolder.bounds];
  //  [l setText:[NSString stringWithFormat:@"%@ place in a %@ pool with  %.02f POINTS",place,pool,locScore]];
  //  [l setTextColor:[UIColor whiteColor]];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]init];
    [string appendAttributedString:[self placeString:[NSString stringWithFormat:@"%@",place]:[NSString stringWithFormat:@"%@",pool] ]];
    [string appendAttributedString:[self pointsString:[NSString stringWithFormat:@"%@",score]]];
  
    
    
    [l setAttributedText:string];
    [statsHolder addSubview:l];
}
//when a player is tapped
-(void)playerTapped:(UITapGestureRecognizer *)r
{
    
    NSInteger tag = r.view.tag;
    
    contestPlayerView  *pv = [players objectAtIndex:tag];
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    ScoringBreakdownController * vc = [storyboard instantiateViewControllerWithIdentifier:@"scoringBreakdown"];
    vc.game = self.myGame;
    NSLog(@"%@ my fucking game",self.myGame);
    vc.pv = pv;
    [vc addGameTeam];
    [vc getContestStats:pv:self.myGame:r.view.restorationIdentifier];
    [self presentViewController:vc animated:YES completion:nil];
    
    
}


-(NSAttributedString *)pointsString:(NSString *)points
{
   
    
    
    
    NSString *fullString = [NSString stringWithFormat:@" %@ POINTS",points];
    
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:fullString];
    [hogan addAttribute:NSFontAttributeName
                  value: [UIFont fontWithName:@"BebasNeueBold" size:20.0]
                  range:NSMakeRange(0,fullString.length)];

    [hogan addAttribute:NSForegroundColorAttributeName value:GREEN range:NSMakeRange(0, fullString.length)];
    
    
    
    return hogan;
    
    
}


-(NSAttributedString *)placeString:(NSString*)place:(NSString *)pool
{
    
    
    NSString *suffix = @"";
    
    if([place isEqualToString:@"1"])
    {
        suffix = @"st";
    }
    else if([place isEqualToString:@"2"])
    {
        suffix = @"nd";
    }
    else if([place isEqualToString:@"3"])
    {
        suffix = @"rd";
    }
    else
    {
        suffix = @"th";
    }
    
    
    NSString *fullString = [NSString stringWithFormat:@"%@%@ place in a %@ pool with",place,suffix,[self getPool:pool]];
    
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:fullString];
    [hogan addAttribute:NSFontAttributeName
                  value:[UIFont systemFontOfSize:14]
                  range:NSMakeRange(0, fullString.length)];
    [hogan addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, fullString.length)];
    
    
    return hogan;
}



-(NSString *)getPool:(NSString *)pool
{
    
    NSArray *pools = [[NSArray alloc]init];
    
    pools = @[@"Free Pool",@"10 Dollar Pool",@"20 Dollar Pool",@"50 Dollar Pool",@"100 Dollar Pool",@"250 Dollar Pool",@"500 Dollar Pool"];
    
    
    return [pools objectAtIndex:[pool intValue]];
    
    
}
 

@end
