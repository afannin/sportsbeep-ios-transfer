//
//  TimeConversion.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/14/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeConversion : NSObject

@property(nonatomic,retain)NSDate *date;

-(NSString *)getWeekDayAbbr;
-(NSString *)getMonthThreeDigit:(NSDate *)date;
-(NSString *)getDayOfMonth;
-(NSString *)getYear;
@end
