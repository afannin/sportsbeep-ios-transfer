//
//  contestPlayerView.m
//  SportsBeep
//
//  Created by Daniel Nasello on 5/30/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "contestPlayerView.h"
#import "Colors.h"
@implementation contestPlayerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    
        [self createSubViews];
        
        
    }
    return self;
}




-(void)createSubViews
{
    /*
    @property(nonatomic,strong)UIView *topView;
    @property(nonatomic,strong)UILabel *placeLabel;
    @property(nonatomic,strong)UILabel *userLabel;
    @property(nonatomic,strong)UILabel *totalLabel;
    @property(nonatomic,strong)UIImageView *icon;
    */
    
    
    [self setBackgroundColor:[UIColor blackColor]];
    
    self.topView = [[UIView alloc]initWithFrame:CGRectMake(60,0,self.frame.size.width-60,30)];
    [self.topView setBackgroundColor:PANEL_BG_BLUE];
    
    [self addSubview:self.topView];
    
    
    
    
    
    self.placeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.topView.frame.size.width, self.topView.frame.size.height+5)];
    [self.topView addSubview:self.placeLabel];
    
    
    
    
    
    
    self.totalLabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 35, self.topView.frame.size.width, self.topView.frame.size.height)];
    [self.totalLabel setTextAlignment:NSTextAlignmentRight];
    
    [self addSubview:self.totalLabel];
    
    
    
    
    self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 60, 60)];
    [self.icon setBackgroundColor:[UIColor orangeColor]];
    [self addSubview:self.icon];

}


-(void)addUandP:(NSString *)username:(NSString *)place
{
    
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]init];
    
    [string appendAttributedString:[self placeString:place]];
    [string appendAttributedString:[self userNameString:username]];
    
    
    [self.placeLabel setAttributedText:string];
}





-(void)addPoints:(NSString *)points
{
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]init];
    
    [string appendAttributedString:[self total]];
    [string appendAttributedString:[self points:points]];
    
    
    [self.totalLabel setAttributedText:string];
    
}






-(NSAttributedString *)placeString:(NSString *)place
{
        NSString *suffix = @"";

       if([place isEqualToString:@"1"])
        {
            suffix = @"st";
        }
        else if([place isEqualToString:@"2"])
        {
            suffix = @"nd";
        }
         else if([place isEqualToString:@"3"])
         {
             suffix = @"rd";
         }
        else
        {
            suffix = @"th";
        }
    
  

    
    NSString *fullString = [NSString stringWithFormat:@"%@%@",place,suffix];
    
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:fullString];
    [hogan addAttribute:NSFontAttributeName
                  value: [UIFont fontWithName:@"BebasNeueBold" size:25.0]
                  range:NSMakeRange(0,place.length)];
    [hogan addAttribute:NSFontAttributeName
                  value: [UIFont fontWithName:@"BebasNeueBold" size:15.0]
                  range:NSMakeRange(place.length,suffix.length)];
    [hogan addAttribute:NSForegroundColorAttributeName value:SKY_BLUE range:NSMakeRange(0, fullString.length)];
    
    
    
    return hogan;
    
    
}


-(NSAttributedString *)userNameString:(NSString *)uname
{
    NSString *fullString = uname;
    
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:fullString];
    [hogan addAttribute:NSFontAttributeName
                  value: [UIFont fontWithName:@"BebasNeueBold" size:25.0]
                  range:NSMakeRange(0, fullString.length)];
    [hogan addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, fullString.length)];
    
    
    return hogan;
}







-(NSAttributedString*)total{
    
    
    NSString *fullString = @"TOTAL:";
    
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:fullString];
    [hogan addAttribute:NSFontAttributeName
                  value:[UIFont systemFontOfSize:12]
                  range:NSMakeRange(0, fullString.length)];
    [hogan addAttribute:NSForegroundColorAttributeName value:SKY_BLUE range:NSMakeRange(0, fullString.length)];
 
    
    return hogan;
}



-(NSAttributedString *)points:(NSString *)points
{
    NSString *fullString = points;
    
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:fullString];
    [hogan addAttribute:NSFontAttributeName
                  value: [UIFont fontWithName:@"BebasNeueBold" size:25.0]
                  range:NSMakeRange(0, fullString.length)];
    [hogan addAttribute:NSForegroundColorAttributeName value:LIGHT_RED range:NSMakeRange(0, fullString.length)];
    
    
    return hogan;
}




@end
