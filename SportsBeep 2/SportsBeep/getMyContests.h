//
//  getMyContests.h
//  SportsBeep
//
//  Created by iOSDev on 5/22/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "getMyContestsProtocol.h"
@interface getMyContests : NSObject{
    id<getMyContestsProtocol>delegate;
}


@property(nonatomic,weak)id delegate;

-(void)getContests:(NSString *)aStatus;
@end
