//
//  PlayerScoringController.h
//  SportsBeep
//
//  Created by Daniel Nasello on 6/1/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "teamRectangleView.h"
@interface PlayerScoringController : UIViewController
- (IBAction)dismissit:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *pScoreLabel;
-(void)addTopView:(teamRectangleView*)tr;
-(void)addPlayerGameStats:(NSDictionary *)dict;
-(void)getPStats:(NSString *)playerId:(NSString *)user_id:(NSString *)player_head:(NSString *)player_name;
@end
