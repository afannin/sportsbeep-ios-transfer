//
//  getGames.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/1/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "sports.h"
#import "getGamesProtocol.h"

@interface getGames : NSObject{
    id<getGamesProtocol>delegate;
}

//ref to the delegate.
@property(nonatomic,weak)id delegate;

//tells me if the sport is college or pro
@property(nonatomic,assign)NSInteger sportLevel;

//tells me what sport we are playing
@property(nonatomic,assign)NSInteger sport;

///string name of the sport
@property(nonatomic,strong)NSString *sportString;

//string name of the sport level
@property(nonatomic,strong)NSString *levelString;


//whether we want all the games or just the upcoming events;
@property(nonatomic,strong)NSString *gameTimeType;

//sets the game variables, the level of sport, and if we want all or just future games
-(void)setSPortAndType:(NSInteger)sport :(NSInteger)level :(NSInteger)futureGames;
//after we set the variables we get the games
-(void)getGames;


@end
