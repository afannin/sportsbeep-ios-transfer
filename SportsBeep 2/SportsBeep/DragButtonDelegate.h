//
//  DragButtonDelegate.h
//  SportsBeep
//
//  Created by iOSDev on 5/29/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DragButtonDelegate <NSObject>

-(void)gestureHasMoved:(CGPoint)point;
-(void)gestureHasEnded:(CGPoint)point;
-(void)gesturHasStarted:(CGPoint)point;

@end
