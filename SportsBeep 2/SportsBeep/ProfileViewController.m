//
//  ProfileViewController.m
//  SportsBeep
//
//  Created by iOSDev on 3/31/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"

#import "ProfileViewController.h"
#import "Colors.h"
#import "LoginManager.h"


@interface ProfileViewController ()
@property(nonatomic,strong) UIImage  *avatar;
@end

@implementation ProfileViewController



- (void)viewDidLoad {
    [super viewDidLoad];
 
    // player profile image button
    
    [self.saveProfileButton setEnabled:NO];
    
    NSString *avatarURL = [[NSUserDefaults standardUserDefaults] objectForKey:@"avatar_url"];
    
    self.blueBox.backgroundColor = PANEL_BG_BLUE;
    self.blueBox.layer.cornerRadius = 5.0f;
    self.cancelProfileButton.titleLabel.font = [UIFont fontWithName:@"BebasNeueBold" size:25.0];
    self.saveProfileButton.titleLabel.font = [UIFont fontWithName:@"BebasNeueBold" size:25.0];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userName = [defaults stringForKey:@"username"];
    
    self.userNameLabel.text = userName;
    self.userNameLabel.font = [UIFont fontWithName:@"BebasNeueBold" size:25.0];

    

    //   NSURL *imageURL = [NSURL URLWithString:avatarURL];
    //   NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    //   self.avatar = [UIImage imageWithData:imageData];

       // UIImage *avatar = [UIImage imageNamed:@"player_silhouette"];
    
 //   [self drawProfileImage];
    
    UIView *bview = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    [bview setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:bview];
    [bview setAlpha:.8];
    [self.view sendSubviewToBack:bview];
    self.proImage = [[UIImageView alloc]initWithFrame:self.playerProfileButton.bounds];
    [self.playerProfileButton addSubview:self.proImage];
    [self.playerProfileButton setClipsToBounds:YES];
    [self.proImage setContentMode:UIViewContentModeScaleAspectFill];
    
    [self.proImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",avatarURL]]
                                          placeholderImage:[UIImage imageNamed:@"player_silhouette"]];
    
   
    
    // save porfile button
    self.saveProfileButton.backgroundColor = RED;
    
    // cansel button
    self.cancelProfileButton.backgroundColor = RED;
    
    [self.playerProfileButton setBackgroundColor:[UIColor colorWithPatternImage:self.avatar]];
    [[self.playerProfileButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    self.playerProfileButton.layer.cornerRadius = 110.0f;
    self.playerProfileButton.layer.borderWidth = 5.0f;
    self.playerProfileButton.layer.borderColor = [BLUE_GRAY CGColor];
}

-(void)drawProfileImage {
    
   // CGFloat buttonHeight = self.playerProfileButton.frame.size.height;
    //CGFloat buttonWidth = self.playerProfileButton.frame.size.width;
    
    // redraw the image to fit |yourView|'s size
 //  UIGraphicsBeginImageContextWithOptions(self.playerProfileButton.frame.size, NO, 0.f);
 //  [self.avatar drawInRect:CGRectMake(0.f, 0.f, buttonWidth, buttonHeight)];
 //   UIImage * newImg = UIGraphicsGetImageFromCurrentImageContext();
  // UIGraphicsEndImageContext();
    
    [self.playerProfileButton setBackgroundColor:[UIColor colorWithPatternImage:self.avatar]];
    [[self.playerProfileButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    self.playerProfileButton.layer.cornerRadius = 110.0f;
    self.playerProfileButton.layer.borderWidth = 5.0f;
    self.playerProfileButton.layer.borderColor = [BLUE_GRAY CGColor];
    
    [self.playerProfileButton setContentMode:UIViewContentModeScaleAspectFit];
    
}

- (IBAction)pushPlayerProfile:(id)sender {
    
    YCameraViewController *camController = [[YCameraViewController alloc] initWithNibName:@"YCameraViewController" bundle:nil];
    camController.delegate=self;
    [self presentViewController:camController animated:YES completion:^{

    }];
}

- (IBAction)cancelProfile:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)saveProfile:(id)sender {
    
    
    [self uploadPhoto:self.avatar];
    
    /*
    // upload avatar
    NSString *url = @"http://sportsbeep.com/api/users/login";
    
    NSDictionary *dict =@{
                          @"email": self.avatar
                          };
    
    NSDictionary *params = @{
                             @"api_user":dict,
                             };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults stringForKey:@"authentication_token"];
    NSString *tokenString = [NSString stringWithFormat:@"Token token=%@", token];
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:tokenString forHTTPHeaderField:@"Authorization"];
    
    manager.requestSerializer = requestSerializer;
    
    [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        

        //[defaults setObject:user.avatarUrl forKey:@"avatar_url"];

        
        [defaults synchronize];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorMessage = operation.responseObject[@"error"];

    }];
    
    */
}
    


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)didFinishPickingImage:(UIImage *)image{
    
    self.avatar = image;
    [self.proImage setImage:image];
 
        [self.saveProfileButton setEnabled:YES];
    
}
-(void)yCameraControllerdidSkipped{
    // Called when user clicks on Skip button on YCameraViewController view
    
}
-(void)yCameraControllerDidCancel{
    // Called when user clicks on "X" button to close YCameraViewController
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



-(void)uploadPhoto:(UIImage *)image
{
   //
   // [self.delegate willBeginToUploadImage];
    
    
    //NSString *paramNameForImage = @"avatar";
    
   /* NSString *dateString = [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                          dateStyle:NSDateFormatterShortStyle
                                                          timeStyle:NSDateFormatterFullStyle];*/
    
    //CGSize size = CGSizeMake(image.size.width * .5,image.size.height * .5);
    //UIImage *resizedimage = [self resizeImage:image:size];
    UIImage *rotatedimage = [self scaleAndRotateImage:image];
    //NSString *imagename = [NSString stringWithFormat:@"%f.jpg",[[NSDate date] timeIntervalSince1970]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    
    
  //   AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    NSData *imageData = UIImageJPEGRepresentation(rotatedimage, 0.5);
    NSString *token = [defaults stringForKey:@"authentication_token"];
    NSString *tokenString = [NSString stringWithFormat:@"Token token=%@", token];
    
    /*
    

    
  //  NSData *imageData = UIImagePNGRepresentation(image);

    AFHTTPRequestOperationManager *man = [[AFHTTPRequestOperationManager alloc]init];
  NSDictionary *parameters = @{@"api_user":@{}};
    AFHTTPRequestOperationManager *client = [[AFHTTPRequestOperationManager alloc]init];
    
    [manager]
    NSMutableURLRequest *request =
    [client multipartFormRequestWithMethod:@"PUT"
                                      path:@"http://sportsbeep.com/api/users"
                                parameters:parameters
                 constructingBodyWithBlock: ^(id<AFMultipartFormData> formData) {
                     
                     [formData appendPartWithFileData:imageData
                                                 name:@"api_user[avatar]"
                                             fileName:@"your_filename.jpeg"
                                             mimeType:@"image/jpeg"];
                 }];
    
    
  
    
    
    AFHTTPRequestOperation *requestOperation = [client HTTPRequestOperationWithRequest:request
                                                                               success:success
                                                                               failure:failure];
    
    [requestOperation start];
    
    
 
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:tokenString forHTTPHeaderField:@"Authorization"];

    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField: @"Content-Type"];
 
     manager.requestSerializer = requestSerializer;
    id block = ^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData
                                    name:@"image"
                                fileName:@"image"
                                mimeType:@"image/jpeg"];
        
    
        
    };
    NSDictionary *parameters = [[NSDictionary alloc]init];
    
    NSString *mimeType = @"image/jpeg"; // ideally this would be dynamically set. Not defaulted to a jpeg!
    
    // you could add to the metadata dictionary (after converting it to a mutable copy) if you needed to.
    
    // convert to NSData


    
    
      NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:imageData];
   //ß [postbody appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
     parameters =@{@"avatar": postbody};
    
    NSDictionary *dict =@{
                          @"api_user":parameters,
                          
                          };
    
    
    
      NSMutableURLRequest *request = [manager.requestSerializer
                                    multipartFormRequestWithMethod:@"PUT"
                                    URLString:@"/api/users"
                                    parameters:dict
                                    constructingBodyWithBlock:block
                                    error:nil];
   
    
    
    [manager PUT:@"http://sportsbeep.com/api/users" parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"success");
        NSLog(@"%@",responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"failure");
    }];
    
  */
    NSDictionary *parameters = @{@"api_user":@{
                                         @"avatar":imageData
                                         }};
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [manager.requestSerializer setValue:tokenString forHTTPHeaderField:@"Authorization"];
  //  [manager.requestSerializer setValue:@"image/jpeg" forHTTPHeaderField:@"Content-Type"];

    NSError *error;
    NSURLRequest *request = [manager.requestSerializer multipartFormRequestWithMethod:@"PUT" URLString:@"http://sportsbeep.com/api/users" parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
         //      [formData appendPartWithFormData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding] name:@"Content-type"];
        [formData appendPartWithFileData:imageData
                                    name:@"api_user[avatar]"
                                fileName:@"your_filename.jpeg"
                                mimeType:@"image/jpeg"];


    } error:&error];

    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self dismissViewControllerAnimated:YES completion:nil];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
      
  
        [defaults setObject:[responseObject objectForKey:@"avatar_url" ] forKey:@"avatar_url"];
        NSLog(@"%@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
    }];
    
    [operation start];
}

-(void)successBlock
{
    
}


- (UIImage *)scaleAndRotateImage:(UIImage *)image {
    
    int kMaxResolution = 400; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}


@end
