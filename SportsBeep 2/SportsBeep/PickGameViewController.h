////
////  PickGameViewController.h
////  SportsBeep
////
////  Created by iOSDev on 3/27/15.
////  Copyright (c) 2015 iOSDev. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//#import "PSCollectionView.h"
//#import "MenuBox.h"
//@interface PickGameViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
//@property (weak, nonatomic) IBOutlet UICollectionView *gamesCollectionVeiw;
//@property (weak, nonatomic) IBOutlet UILabel *pickGameLabel;
//@property(nonatomic,strong)MenuBox *mSlideUpMenu;
//@end


//
//  ViewController.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 4/17/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "secondVC.h"
#import "thirdVC.h"
#import "individualGame.h"
#import "getPlayersForGame.h"
@interface PickGameViewController : UIViewController<UIScrollViewDelegate,childVcDelegate,getPlayersDelegate>


@property(nonatomic,retain)individualGame *indieGame;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

//tells us the current page we are on
@property(nonatomic,assign)NSInteger currentPage;

@property(nonatomic,strong)UIButton *backButton;

@end