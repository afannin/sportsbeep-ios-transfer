//
//  playerPositionViewDelegate.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/27/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <Foundation/Foundation.h>
@class  playerPlayerView;
@class  playerPickerView;
@protocol playerPositionViewDelegate <NSObject>

-(void)playerPositionWasChosen:(playerPickerView*)p :(playerPlayerView *)v :(int)position :(NSString *)po;


@end
