//
//  contestsCell.h
//  SportsBeep
//
//  Created by iOSDev on 5/30/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "bigTriangleHolder.h"
@interface contestsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (strong, nonatomic) IBOutlet UIView *dateHolder;
@property(nonatomic,strong)bigTriangleHolder *triangleHolder;
-(void)addGameTriangles :(NSDictionary*)homeTeam :(NSDictionary *)awayTeam:(CGFloat)cellWidth:(NSString *)date:(NSString*)time;
-(void)changeTriangleColors:(NSDictionary*)homeTeam :(NSDictionary *)awayTeam:(NSString *)date:(NSString*)time;
@property (strong, nonatomic) IBOutlet UILabel *bottomView;

@end
