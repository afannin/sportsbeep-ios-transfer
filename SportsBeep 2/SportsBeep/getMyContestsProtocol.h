//
//  getMyContestsProtocol.h
//  SportsBeep
//
//  Created by iOSDev on 5/22/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol getMyContestsProtocol <NSObject>
-(void)getContestsHasStarted;
-(void)getContestsDidSucceed:(NSDictionary*)result;
-(void)getContestsDidFail:(NSString *)error;


@end
