

#import "PickGameViewController.h"
#import "getGames.h"
#import "gamePlayer.h"
#import "playerPickerView.h"
#import "liveContestsViewController.h"
#import "PickPoolViewController.h"
@interface PickGameViewController (){
    secondVC *secondV;
    thirdVC *thirdV;
    
    getPlayersForGame *getPlayers;
    NSDictionary *teamDict;
    NSDictionary    *playerHolders;
    
    BOOL somethingPicked;
    
    BOOL willGoBack;
    
    NSMutableArray *playerListHolders;
    
    //holds all of the views that will hold the player lists
    NSMutableArray *placeholderViews;
    
    NSArray *holderToAdd;
    
    NSMutableArray *listSomething;
    
    //tells me that all positions have been filled;
    NSMutableArray *positionHolder;
    
    NSMutableDictionary  *holdPlayersToSend;
    NSMutableArray *holdPlayersArray;
}

@end

@implementation PickGameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self showHud];
 
    CGRect main = [UIScreen mainScreen].bounds;
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:main];
    [bgView setImage:[UIImage imageNamed:@"baseball"]];
    bgView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:bgView];
    
    [self.view sendSubviewToBack:bgView];
    
    willGoBack = NO;
    
    listSomething = [[NSMutableArray alloc]init];
    
    self.currentPage = 0;
    
    [self setupScrollView];
    
    [self addViewControllers];
    
    [self makeButton];
    
    
    
    holdPlayersToSend = [[NSMutableDictionary alloc]init];
    
    holdPlayersArray = [[NSMutableArray alloc]init];
    
    positionHolder = [[NSMutableArray alloc]init];
    
    somethingPicked = NO;
    
    
    //titles for the positions... need to be changed for football
    holderToAdd = [[NSArray alloc]init];
    holderToAdd = @[@"pitchers",@"catchers",@"cinfielders",@"minfielders",@"outfielders",@"flex"];
    
    
    NSDictionary *hometeam = self.indieGame.homeTeamObject;
    NSDictionary *awayteam = self.indieGame.awayTeamObject;
    
    
    NSString *home_team_id = [NSString stringWithFormat:@"%@", [hometeam objectForKey:@"id"]];
    NSString *away_team_id =[NSString stringWithFormat:@"%@", [awayteam objectForKey:@"id"]];
    
    
    
    
    //the views that hold all of the lists of players
    placeholderViews = [[NSMutableArray alloc]init];
    [self addPlaceholders];
    
    
    //dicts that hold the teams informatiopn
    teamDict = [NSDictionary dictionaryWithObjectsAndKeys:
                hometeam, home_team_id,
                awayteam,away_team_id,
                nil];
    
    
    
    
    //game id
    NSString *game_id = [NSString stringWithFormat:@"%@",self.indieGame.gameId];
    
    
    
    //gets players for the game, needs to change for football
    getPlayers = [[getPlayersForGame alloc]init];
    [getPlayers setDelegate:self];
    [getPlayers getPlayers:game_id:@"baseball"];
    
    
    playerListHolders = [[NSMutableArray alloc]init];
    
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    [self.scrollView setBackgroundColor:[UIColor clearColor]];
    
}

-(void)addPlaceholders
{
    for(int i=0;i<[holderToAdd count];i++)
    {
        UIView *v = [[UIView alloc]initWithFrame:CGRectMake((i+1)*self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [v setBackgroundColor:[UIColor clearColor]];
        [self.scrollView addSubview:v];
        [placeholderViews addObject:v];
        
    }
    
}


-(void)getCurrentPoolsForGame
{
    
    
    
    
    
    
}




-(void)setupScrollView
{
    //number of pages depend on the number of positions
    NSInteger lNumberOfPages = holderToAdd.count;
    
    CGFloat lWidth = self.view.frame.size.width;
    CGFloat lHeight = self.view.frame.size.height;
    
    //sets the content size so it is 4* the size of the body.
    [self.scrollView setContentSize:CGSizeMake(lWidth * lNumberOfPages, lHeight)];
    [self.scrollView setPagingEnabled:YES];
    [self.scrollView setDelegate:self];
    [self.scrollView setScrollEnabled:NO];
    
}


//add view controllers to the view
-(void)addViewControllers
{
    
    CGFloat lWidth = self.view.frame.size.width;
    CGFloat lHeight = self.view.frame.size.height;
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    thirdV = [storyboard instantiateViewControllerWithIdentifier:@"thvc"];
    thirdV.iGame = self.indieGame;
    [thirdV.view setFrame:[[UIScreen mainScreen] bounds]];
    [self.scrollView setBackgroundColor:  [UIColor colorWithRed:0.314 green:0.349 blue:0.478 alpha:1]];
    [thirdV setDelegate:self];
    [self.scrollView addSubview:thirdV.view];
    [thirdV didMoveToParentViewController:self];

    
}


//makes the backbutton
-(void)makeButton
{
    
    self.backButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backButton setFrame:CGRectMake(5, 9, 50, 50)];
    [self.backButton setTitle:@"back" forState:UIControlStateNormal];
    [self.view addSubview:self.backButton];
    [self.backButton  addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
}



-(void)backButtonPressed:(id)sender
{
    
    if(self.currentPage < 1)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        if(somethingPicked)
        {
            willGoBack = YES;
        }
        
        [self scrollSCrollViewBack];
    }
    
}


//scroll the scrollview back
-(void)scrollSCrollViewBack
{
    NSInteger newOFfset = self.currentPage - 1;
    //make sure we never go behind 0
    if(newOFfset < 0)
    {
        newOFfset = 0;
    }
   
    //CGFloat lWidth = self.view.frame.size.width;
  
    
    ///scrolls to the new position
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
    
    
    if(somethingPicked)
    {
        willGoBack = YES;
    }
    
    
    
    
}


//scroll the scrollview back
-(void)scrollSCrollViewForward
{
    NSInteger newOFfset = self.currentPage + 1;
    //make sure we never go behind 0
    if(newOFfset >= 6)
    {
        newOFfset = 6;
    }
    CGFloat lWidth = self.view.frame.size.width;
    
    ///scrolls to the new position
    [self.scrollView setContentOffset:CGPointMake(lWidth * newOFfset, 0) animated:YES];
    
    
}


//when the scroll view scrolls we can tell what page its on
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.currentPage = page;
    
    [self toggleBackButton:page];
}



-(void)toggleBackButton:(NSInteger)page
{
    if(page < 1)
    {
        //    [self.backButton setEnabled:NO];
    }
    else
    {
        [self.backButton setEnabled:YES];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}





//position view controller delegatge
-(void)positionButtonWasPressed:(int)position
{
    [self scrollSCrollViewForward];
    [self resortSports:position];
    
    
    
}

//re arranges how the sports appear;
-(void)resortSports:(int)position
{
    NSMutableArray *listCopy = [playerListHolders mutableCopy];
    
    [listSomething removeAllObjects];
    listSomething  = nil;
    
    if(position == 0)
    {
        [self addSCreens:listCopy];
    }
    else
    {
        int pos = position;
        if(pos > 0)
        {
            pos = position -1;
        }
        
        for(int i = pos;i>=0;i--)
        {
            playerPickerView * v= [listCopy objectAtIndex:i];
            [listCopy removeObjectAtIndex:i];
            [listCopy addObject:v];
        }
        
        [self addSCreens:listCopy];
    }
    
    
    listSomething = listCopy;
}

//adds all the player picker views
-(void)addSCreens:(NSMutableArray *)ar
{
    int i=0;
    for(playerPickerView *v in ar)
    {
        [v setFrame:self.view.bounds];
        
        UIView *view  = [placeholderViews objectAtIndex:i];
        
        NSLog(@"view added");
        [view addSubview:v];
        
        i++;
    }
}

-(void)getPlayersHasStarted
{
   
    
}

-(void)getPlayersSuccess:(NSDictionary *)response
{

    
    
    
    
    NSMutableArray *pitchers = [[NSMutableArray alloc]init];
    NSMutableArray *catchers = [[NSMutableArray alloc]init];
    NSMutableArray *cinfield = [[NSMutableArray alloc]init];
    NSMutableArray *minfield = [[NSMutableArray alloc]init];
    NSMutableArray *outfielder = [[NSMutableArray alloc]init];
    NSMutableArray *flex = [[NSMutableArray alloc]init];
    
    
   NSArray *players = [response objectForKey:@"players"];
    
    for(NSDictionary *dict in players)
    {
        
        NSString *headshot = [dict objectForKey:@"headshot_url"];
        NSString *pid = [dict objectForKey:@"id"];
        NSString *pname = [dict objectForKey:@"name"];
        NSString *number = [dict objectForKey:@"number"];
        NSString *position = [NSString stringWithFormat:@"%@",[dict objectForKey:@"position"]];
        NSArray *stats = [dict objectForKey:@"stats"];
        NSDictionary *team = [dict objectForKey:@"team"];
        
        NSString *tid = [NSString stringWithFormat:@"%@", [team objectForKey:@"id"]];
        NSDictionary *team_info = [teamDict objectForKey:tid];
        
        
        
        NSArray *pos = [[NSArray alloc]init];
        pos = @[@"catcher",@"pitcher",@"corner_infielder",@"middle_infielder",@"outfielder"];
        
        
        
        gamePlayer *p = [[gamePlayer alloc]init];
        
        p.player_headshot = headshot;
        p.player_id = pid;
        p.player_name = pname;
        p.player_number = number;
        p.player_position = position;
        p.stats = stats;
        p.team_id = tid;
        p.team_object = team_info;
        p.team_abbr = [team_info objectForKey:@"abbr"];

        
        NSLog(@"the position %@",position);
        
        if([position isEqualToString:@"2B"] ||[position isEqualToString:@"SS"] )
            
        {
            p.fakePos = @"middle_infielder";
            [minfield addObject:p];
            
        }
        else if([position isEqualToString:@"C"] )
        {
            p.fakePos = @"catcher";
            [catchers addObject:p];
        }
        else if([position isEqualToString:@"RP"]||[position isEqualToString:@"SP"])
        {
            p.fakePos = @"pitcher";
            [pitchers addObject:p];
            NSLog(@"pitcher added");
        }
        else if([position isEqualToString:@"1B"] || [position isEqualToString:@"3B"] )
        {
            p.fakePos = @"corner_infielder";
            [cinfield addObject:p];
        }
        else if([position isEqualToString:@"LF"] || [position isEqualToString:@"CF"] || [position isEqualToString:@"RF"])
        {
            p.fakePos = @"outfielder";
            [outfielder addObject:p];
        }
        
        [flex addObject:p];
        
        
    }
    
    playerHolders = [NSDictionary dictionaryWithObjectsAndKeys:
                     [self shuffleArray:pitchers], @"pitchers",
                      [self shuffleArray:catchers],@"catchers",
                      [self shuffleArray:cinfield],@"cinfielders",
                      [self shuffleArray:minfield],@"minfielders",
                      [self shuffleArray:outfielder],@"outfielders",
                      [self shuffleArray:flex],@"flex",
                     nil];
    
    
    NSArray *titles =[[NSArray alloc]init];
    
    titles = @[
               @"PICK YOUR PITCHER",
               @"PICK YOUR CATCHER",
               @"PICK YOUR CORNER INFIELDER",
               @"PICK YOUR MIDDLE INFIELDER",
               @"PICK YOUR OUTFIELDER",
               @"PICK YOUR FLEX PLAYER",
               ];
    
    
    NSArray *properTitles = [[NSMutableArray alloc]init];
    properTitles = @[@"pitchers",@"catchers",@"cinfielders",@"minfielders",@"outfielders",@
                     "flex"];
    
    
    

    int i = 0;
    for(NSString *s in titles)
    {
        playerPickerView *v = [[playerPickerView alloc]initWithFrame:self.view.bounds];
        [v createHeader:s];
        
        NSString * p = [properTitles objectAtIndex:i];
        v.positionView =p;
        //so we always have access to the views
        [playerListHolders addObject:v];
        
        NSMutableArray *array = [playerHolders objectForKey:p];
        
        v.delegate = self;
        v.tag = i;
        [v addPlayers:array];


        i++;
        
    }
    
    
    [self hideHud];
}

-(void)getPLayersHasFailed:(NSString*)err
{
    [self hideHud];
}




//position view delegate

-(void)playerPositionWasChosen:(playerPickerView*)p :(playerPlayerView *)v :(int)position :(NSString *)po
{
    
    //gets the position key from the game Obj and sets it
    
    
    //get the id from the player so we can hide all the same players so we dont confuse flex with a normal player
    NSString *playersId= v.playerId;
    
 
    
    [holdPlayersToSend setObject:v forKey:p.positionView];
    
    
    
       [self hideDuplicatePlayers:playersId:v];
    
    
   
    
    
    playerPickerView *vi = [listSomething lastObject];
    
    if(vi.tag == position  || willGoBack)
    {
        [self scrollSCrollViewBack];
    }
    
    else
    {
        [self scrollSCrollViewForward];
    }
    
    
    somethingPicked = YES;
    
    if([self areAllPositionsThere:po])
    {
        thirdV.buttonShouldBeEnabled = YES;
    }
    
    [thirdV replaceLinear:v :position];
    
}

//hides duplicate player ids and shows other ones that might have been previously hidden
-(void)hideDuplicatePlayers:(NSString *)playerid:(playerPlayerView *)player
{
    //get all the player picker views in the player list holders to reference all the player player views
   for(playerPickerView *p  in playerListHolders)
   {
       
       for(playerPlayerView *ppv in  p.playerViews)
       {
           if([ppv.playerId isEqualToString:playerid] || [self evalCurrentPicked:ppv.playerId])
           {
               //if its not the exact view it tapped
               if(player != ppv )
               {
                   [ppv setFrame:CGRectZero];
                   NSLog(@"should hide a player");
               }
              
           }
           else
           {
               [ppv setFrame:CGRectMake(ppv.frame.origin.x, ppv.frame.origin.y, self.view.frame.size.width, 50)];
               [ppv setHidden:NO];
           }
       }
       
       
   }
    
    
    
}


-(BOOL)evalCurrentPicked:(NSString *)playerId
{
    NSString *key;
    for(key in holdPlayersToSend)
    {
        NSLog(@"Key: %@, Value %@", key, [holdPlayersToSend objectForKey: key]);
        
        playerPlayerView *pv = [holdPlayersToSend objectForKey: key];

        if([playerId isEqualToString:pv.playerId])
        {
            return YES;
        }
    }
    
    
    return NO;
}



-(BOOL)areAllPositionsThere:(NSString *)addpos
{
    
    if([self shouldAddString:addpos])
    {
        [positionHolder addObject:addpos];
    }
    
    if([positionHolder count] == [holderToAdd count])
    {
        return YES;
    }
    
    return NO;
}


-(BOOL)shouldAddString:(NSString *)string
{
    
    
    for(NSString *s in positionHolder)
    {
        if([s isEqualToString:string])
        {
            
            return NO;
        }
    }
    
    return  YES;
}



-(void)showHud
{
    KVNProgressConfiguration *configuration = [[KVNProgressConfiguration alloc] init];
    
    
    //  configuration.statusColor = [UIColor whiteColor];
    configuration.statusFont = [UIFont fontWithName:@"HelveticaNeue-Thin" size:15.0f];
    configuration.circleStrokeForegroundColor = [UIColor darkGrayColor];
    configuration.circleStrokeBackgroundColor = [UIColor clearColor];
    configuration.circleFillBackgroundColor = [UIColor clearColor];
    configuration.backgroundFillColor = [UIColor clearColor];
    configuration.backgroundTintColor = [UIColor clearColor];
    configuration.successColor = [UIColor whiteColor];
    configuration.errorColor = [UIColor whiteColor];
    configuration.circleSize = 50.0f;
    configuration.lineWidth = 2.0f;
    configuration.fullScreen = NO;
    
    
    configuration.tapBlock = ^(KVNProgress *progressView) {
        // Do something you want to do when the user tap on the HUD
        // Does nothing by default
    };
    
    // You can allow user interaction for behind views but you will losse the tapBlock functionnality just above
    // Does not work with fullscreen mode
    // Default is NO
    configuration.allowUserInteraction = NO;
    
    [KVNProgress setConfiguration:configuration];
    [KVNProgress showWithStatus:@""
                         onView:self.view];
    
}

-(void)hideHud
{
    [KVNProgress dismiss];
    
}


-(NSMutableArray*)shuffleArray:(NSMutableArray *)ar
{
    NSUInteger count = [ar count];
    for (NSUInteger i = 0; i < count; ++i) {
        // Select a random element between i and end of array to swap with.
        NSUInteger nElements = count - i;
        NSUInteger n = (random() % nElements) + i;
        [ar exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    
    return ar;
}




//third vc delegate to submit a team

-(void)submitTeamWasPressed
{
    [self submitTeam];
}
//end


-(void)submitTeam
{

    
    
   
    
    
    [holdPlayersArray removeAllObjects];
    NSString *key;
    for(key in holdPlayersToSend){
        NSLog(@"Key: %@, Value %@", key, [holdPlayersToSend objectForKey: key]);
        
       playerPlayerView *pv = [holdPlayersToSend objectForKey: key];
        
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSString stringWithFormat:@"%@",pv.playerId],@"id",
                              pv.gPlayer.fakePos,@"position"
                              , nil];
        
        [holdPlayersArray addObject: dict];
    }
    
    
    
    NSString *game_id = self.indieGame.gameId;
    
  //takkes th4e game id and the players to send and sends it to the ppol screenc
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PickPoolViewController *pp = [storyboard instantiateViewControllerWithIdentifier:@"PickPool"];
    pp.game_id = game_id;
    pp.homeTeamObject = self.indieGame.homeTeamObject;
    pp.indieGame = self.indieGame;
    pp.awayTeamObject = self.indieGame.awayTeamObject;
    pp.playersPickedForPool = holdPlayersArray;
    [self presentViewController:pp animated:YES completion:nil];
    [pp getCurrentPoolsForGame:game_id];
    //add the pool screen here
    
    
    
}
@end

