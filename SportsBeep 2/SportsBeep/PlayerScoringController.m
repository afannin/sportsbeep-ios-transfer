//
//  PlayerScoringController.m
//  SportsBeep
//
//  Created by Daniel Nasello on 6/1/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "PlayerScoringController.h"
#import "CSLinearLayoutView.h"
#import "Colors.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface PlayerScoringController (){
    CSLinearLayoutView *linearLayout;
    CGFloat totalScoreForPlayer;
}

@end

@implementation PlayerScoringController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.pScoreLabel.backgroundColor = DARK_BLUE;
    
    linearLayout = [[CSLinearLayoutView alloc]initWithFrame:CGRectMake(0, 120, self.view.frame.size.width, self.view.frame.size.height-120)];
    [self.view addSubview:linearLayout];
  
   [self.pScoreLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:30.0]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addTopView:(teamRectangleView*)tr
{
    [tr setFrame:CGRectMake(tr.frame.origin.x, 60, tr.frame.size.width, tr.frame.size.height)];
    [self.view addSubview:tr];
}


-(void)addPlayerGameStats:(NSDictionary *)dict
{
    
    NSLog(@"%@",dict);
    
}


-(void)getPStats:(NSString *)playerId:(NSString *)user_id:(NSString *)player_head:(NSString *)player_name
{
    CGFloat statWid = self.view.frame.size.width*.4;
    CGFloat otherWid = self.view.frame.size.width*.2;
    
    
   
    UIImageView *v = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 150, 150)];
    CGPoint p = CGPointMake(self.view.center.x, 50);
    [v sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",player_head]]
                     placeholderImage:[UIImage imageNamed:@"player_silhouette"]];
    v.layer.cornerRadius = v.frame.size.width/2;
    [v setClipsToBounds:YES];
    [v setContentMode:UIViewContentModeScaleAspectFit];
    CSLinearLayoutItem *i = [[CSLinearLayoutItem alloc]initWithView:v];
    i.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
    [v setCenter:p];
    [linearLayout addItem:i];
     i.padding = CSLinearLayoutMakePadding(10.0, 10.0, 10.0, 10.0);
    [self getPlayerGameStats:playerId:user_id];
    
    UILabel *pn = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    [pn setTextColor:[UIColor whiteColor]];
    [pn setTextAlignment:NSTextAlignmentCenter];
    [pn setText:player_name];
    CSLinearLayoutItem *i2 = [[CSLinearLayoutItem alloc]initWithView:pn];
    i2.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
    [pn setFont: [UIFont fontWithName:@"BebasNeueBold" size:28.0]];
     i2.padding = CSLinearLayoutMakePadding(5.0, 0, 0, 0);
    [linearLayout addItem:i2];

    UIView *statHolder = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    [statHolder setBackgroundColor:BLUE];
    UILabel *sHolders = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, statWid, 20)];
    [sHolders setTextColor:[UIColor whiteColor]];
    [sHolders setFont: [UIFont fontWithName:@"BebasNeueBold" size:16.0f]];
    [sHolders setText:@"STATS"];
    [sHolders setTextAlignment:NSTextAlignmentCenter];
    [statHolder addSubview:sHolders];
    
    UILabel *nHolders = [[UILabel alloc]initWithFrame:CGRectMake(statWid, 0, otherWid, 20)];
    [nHolders setTextColor:[UIColor whiteColor]];
    [nHolders setFont: [UIFont fontWithName:@"BebasNeueBold" size:16.0f]];
    [nHolders setText:@"#"];
    [nHolders setTextAlignment:NSTextAlignmentCenter];
    [statHolder addSubview:nHolders];
    
    UILabel *pHolders = [[UILabel alloc]initWithFrame:CGRectMake(statWid + otherWid, 0, otherWid, 20)];
    [pHolders setTextColor:[UIColor whiteColor]];
    [pHolders setFont: [UIFont fontWithName:@"BebasNeueBold" size:16.0f]];
    [pHolders setText:@"PTS"];
    [pHolders setTextAlignment:NSTextAlignmentCenter];
    [statHolder addSubview:pHolders];
    
    UILabel *tHolders = [[UILabel alloc]initWithFrame:CGRectMake(statWid + (otherWid*2), 0, otherWid, 20)];
    [tHolders setTextColor:[UIColor whiteColor]];
    [tHolders setFont: [UIFont fontWithName:@"BebasNeueBold" size:16.0f]];
    [tHolders setText:@"TOTAL"];
    [tHolders setTextAlignment:NSTextAlignmentCenter];
    [statHolder addSubview:tHolders];
    
    UIView *bottomBorder = [[UIView alloc]initWithFrame:CGRectMake(0, 19, self.view.frame.size.width, 1)];
    [bottomBorder setBackgroundColor:PANEL_BG_BLUE];
    [statHolder addSubview:bottomBorder];
    CSLinearLayoutItem *i3 = [[CSLinearLayoutItem alloc]initWithView:statHolder];
    i3.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
    
    CGFloat h = statHolder.frame.size.height;
    UIView *rightOne = [[UIView alloc]initWithFrame:CGRectMake(statWid -1, 0, 1,h )];
    [rightOne setBackgroundColor:PANEL_BG_BLUE];
    [sHolders addSubview:rightOne];
    
    UIView *rightTwo = [[UIView alloc]initWithFrame:CGRectMake(otherWid -1, 0, 1,h )];
    [rightTwo setBackgroundColor:PANEL_BG_BLUE];
    [nHolders addSubview:rightTwo];
    
    UIView *rightThree = [[UIView alloc]initWithFrame:CGRectMake(otherWid -1, 0, 1,h )];
    [rightThree setBackgroundColor:PANEL_BG_BLUE];
    [pHolders addSubview:rightThree];

    
    [linearLayout addItem:i3];

    
    
    
}

-(void)getPlayerGameStats:(NSString *)pid:(NSString *)uid
{
    
    CGFloat statWid = self.view.frame.size.width*.6;
    CGFloat otherWid = self.view.frame.size.width*.2;
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults stringForKey:@"authentication_token"];
    NSString *tokenString = [NSString stringWithFormat:@"Token token=%@", token];
    NSString *url =[NSString stringWithFormat:@"http://sportsbeep.com/api/mlb/tickets/%@/players/%@/scoresheet",uid,pid];
    
    NSDictionary *dict =@{
                          //  @"status":[NSString stringWithFormat:self.gameTimeType],
                          @"status":@"upcoming",
                          };
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:tokenString forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer = requestSerializer;
    
    [manager GET:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSArray *arr = [responseObject objectForKey:@"stats"];
        
        for(NSDictionary *dict in arr)
        {
            
            
            CSLinearLayoutItem *i = [self addItemWithStats:[dict objectForKey:@"name"] :[dict objectForKey:@"amount"] :[dict objectForKey:@"points"]:[dict objectForKey:@"points_per"]];
            
            
            CGFloat pp = [[dict objectForKey:@"points_per"] floatValue];
            totalScoreForPlayer += pp;
            
            [linearLayout addItem:i];
        }
        
       
        NSLog(@"@ the total points are %f",totalScoreForPlayer);
        
        CSLinearLayoutItem * myi = [self addTotalScore];
        [linearLayout addItem:myi];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorMessage = operation.responseObject[@"error"];
        
        NSLog(@"err: %@", errorMessage);
        
    }];
}



-(CSLinearLayoutItem *)addItemWithStats:(NSString *)title:(NSString *)num:(NSString *)pts:(NSString *)total
{
    
    
    CGFloat statWid = self.view.frame.size.width*.4;
    CGFloat otherWid = self.view.frame.size.width*.2;
    
    UIView *statHolder = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    
    UILabel *sHolders = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, statWid, 20)];
    [sHolders setTextColor:[UIColor whiteColor]];
    [sHolders setFont: [UIFont systemFontOfSize:14]];
    [sHolders setText:title];
    [sHolders setTextAlignment:NSTextAlignmentCenter];
    [statHolder addSubview:sHolders];
    
    UILabel *nHolders = [[UILabel alloc]initWithFrame:CGRectMake(statWid, 0, otherWid, 20)];
    [nHolders setTextColor:[UIColor whiteColor]];
    [nHolders setFont:[UIFont systemFontOfSize:12]];
    [nHolders setText:num];
    [nHolders setTextAlignment:NSTextAlignmentCenter];
    [statHolder addSubview:nHolders];
    
    UILabel *pHolders = [[UILabel alloc]initWithFrame:CGRectMake(statWid + otherWid, 0, otherWid, 20)];
    [pHolders setTextColor:[UIColor whiteColor]];
    [pHolders setFont:[UIFont systemFontOfSize:12]];
    [pHolders setText:pts];
    [pHolders setTextAlignment:NSTextAlignmentCenter];
    [statHolder addSubview:pHolders];
    
    UILabel *tHolders = [[UILabel alloc]initWithFrame:CGRectMake(statWid + (otherWid*2), 0, otherWid, 20)];
    [tHolders setTextColor:[UIColor whiteColor]];
    [tHolders setFont: [UIFont systemFontOfSize:12]];
    [tHolders setText:total];
    [tHolders setTextAlignment:NSTextAlignmentCenter];
    [statHolder addSubview:tHolders];
    
    
    UIView *bottomBorder = [[UIView alloc]initWithFrame:CGRectMake(0, 19, self.view.frame.size.width, 1)];
    [bottomBorder setBackgroundColor:PANEL_BG_BLUE];
    [statHolder addSubview:bottomBorder];

    CGFloat h = statHolder.frame.size.height;
    UIView *rightOne = [[UIView alloc]initWithFrame:CGRectMake(statWid -1, 0, 1,h )];
    [rightOne setBackgroundColor:PANEL_BG_BLUE];
    [sHolders addSubview:rightOne];
    
    UIView *rightTwo = [[UIView alloc]initWithFrame:CGRectMake(otherWid -1, 0, 1,h )];
    [rightTwo setBackgroundColor:PANEL_BG_BLUE];
    [nHolders addSubview:rightTwo];
    
    UIView *rightThree = [[UIView alloc]initWithFrame:CGRectMake(otherWid -1, 0, 1,h )];
    [rightThree setBackgroundColor:PANEL_BG_BLUE];
    [pHolders addSubview:rightThree];
    
    CSLinearLayoutItem *i3 = [[CSLinearLayoutItem alloc]initWithView:statHolder];
    i3.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
    
    
    
    return i3;
    
    
}





-(CSLinearLayoutItem *)addTotalScore
{
    
    
    CGFloat statWid = self.view.frame.size.width*.4;
    CGFloat otherWid = self.view.frame.size.width*.2;
    
    UIView *statHolder = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    
    UILabel *sHolders = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, statWid, 20)];
    [sHolders setTextColor:GREEN];
    [sHolders setFont: [UIFont fontWithName:@"BebasNeueBold" size:18.0f]];
    [sHolders setText:@"PLAYER TOTAL"];
    [sHolders setTextAlignment:NSTextAlignmentCenter];
    [statHolder addSubview:sHolders];
    
    UILabel *nHolders = [[UILabel alloc]initWithFrame:CGRectMake(statWid, 0, otherWid, 20)];
    [nHolders setTextColor:[UIColor whiteColor]];
    //  [nHolders setFont: [UIFont fontWithName:@"BebasNeueBold" size:18.0f]];

    [nHolders setTextAlignment:NSTextAlignmentCenter];
    [statHolder addSubview:nHolders];
    
    UILabel *pHolders = [[UILabel alloc]initWithFrame:CGRectMake(statWid + otherWid, 0, otherWid, 20)];
    [pHolders setTextColor:[UIColor whiteColor]];
    //  [pHolders setFont: [UIFont fontWithName:@"BebasNeueBold" size:18.0f]];

    [pHolders setTextAlignment:NSTextAlignmentCenter];
    [statHolder addSubview:pHolders];
    
    UILabel *tHolders = [[UILabel alloc]initWithFrame:CGRectMake(statWid + (otherWid*2), 0, otherWid, 20)];
    [tHolders setTextColor:GREEN];
    [tHolders setFont: [UIFont fontWithName:@"BebasNeueBold" size:18.0f]];
    CGFloat rounded_up = round(totalScoreForPlayer * 100) / 100;
    [tHolders setText:[NSString stringWithFormat:@"%.01f",rounded_up]];
    [tHolders setTextAlignment:NSTextAlignmentCenter];
    [statHolder addSubview:tHolders];
    
    
    UIView *bottomBorder = [[UIView alloc]initWithFrame:CGRectMake(0, 19, self.view.frame.size.width, 1)];
    [bottomBorder setBackgroundColor:PANEL_BG_BLUE];
    [statHolder addSubview:bottomBorder];
    
    CGFloat h = statHolder.frame.size.height;
    UIView *rightOne = [[UIView alloc]initWithFrame:CGRectMake(statWid -1, 0, 1,h )];
    [rightOne setBackgroundColor:PANEL_BG_BLUE];
    [sHolders addSubview:rightOne];
    
    UIView *rightTwo = [[UIView alloc]initWithFrame:CGRectMake(otherWid -1, 0, 1,h )];
    [rightTwo setBackgroundColor:PANEL_BG_BLUE];
    [nHolders addSubview:rightTwo];
    
    UIView *rightThree = [[UIView alloc]initWithFrame:CGRectMake(otherWid -1, 0, 1,h )];
    [rightThree setBackgroundColor:PANEL_BG_BLUE];
    [pHolders addSubview:rightThree];
    
    CSLinearLayoutItem *i3 = [[CSLinearLayoutItem alloc]initWithView:statHolder];
    i3.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentCenter;
    
    
    
    return i3;
    
    
}


- (IBAction)dismissit:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
