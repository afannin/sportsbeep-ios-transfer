//
//  BuildTeamViewController.m
//  SportsBeep
//
//  Created by iOSDev on 3/30/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "BuildTeamViewController.h"
#import "Colors.h"

@interface BuildTeamViewController ()

@end

@implementation BuildTeamViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGFloat deviceWidth = self.view.frame.size.width;
    //CGFloat deviceHeight = self.view.frame.size.height;
    
    self.menuBox.backgroundColor = GREEN;
    
    // TODO
    
    // create and display matchup image
    
    // resize scrollview
    self.buildTeamScrollView.backgroundColor = [UIColor redColor];
    
    NSLog(@"Width before: %f",self.buildTeamScrollView.frame.size.width);
    
    [self.buildTeamScrollView setContentSize:CGSizeMake(deviceWidth*4, self.buildTeamScrollView.frame.size.height)];
    
    NSLog(@"Width after: %f",self.buildTeamScrollView.frame.size.width);
    
    // populate scrollview
    UILabel *firstLabel;
    firstLabel.center = CGPointMake(0, 300);
    firstLabel.text = @"First";
    firstLabel.textColor = [UIColor blackColor];
    
    UILabel *secondLabel;
    secondLabel.center = CGPointMake(deviceWidth, 0);
    secondLabel.text = @"Second";
    
    UILabel *thirdLabel;
    thirdLabel.center = CGPointMake(2*deviceWidth, 0);
    thirdLabel.text = @"Third";
    
    UILabel *fourthLabel;
    fourthLabel.center = CGPointMake(3*deviceWidth, 0);
    fourthLabel.text = @"Fourth";
    
    [self.buildTeamScrollView addSubview:firstLabel];
    [self.buildTeamScrollView addSubview:secondLabel];
    [self.buildTeamScrollView addSubview:thirdLabel];
    [self.buildTeamScrollView addSubview:fourthLabel];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
