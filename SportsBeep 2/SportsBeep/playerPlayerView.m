//
//  playerPlayerView.m
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/27/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import "playerPlayerView.h"

@implementation playerPlayerView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self createSubviews];
    }
    return self;
}




-(void)createSubviews
{

    
    self.playerNumber = [[UILabel alloc]initWithFrame:CGRectMake(0, 4, 50, 30)];
    [self.playerNumber setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:self.playerNumber];
    [self.playerNumber setFont: [UIFont fontWithName:@"BebasNeueBold" size:34.0]];
    
    self.playerTeamAbbr = [[UILabel alloc]initWithFrame:CGRectMake(0, 30, 50, 20)];
    [self.playerTeamAbbr setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:self.playerTeamAbbr];
    [self.playerTeamAbbr setFont: [UIFont fontWithName:@"BebasNeueBold" size:14.0]];
    
    
    
    
    UIView *leftBarrier = [[UIView alloc]initWithFrame:CGRectMake(49, 0, 1, 50)];
    [leftBarrier setBackgroundColor:[UIColor blackColor]];
    [self addSubview:leftBarrier];
    
    UIView *topBarrier = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 1)];
    [topBarrier setBackgroundColor:[UIColor blackColor]];
    [self addSubview:topBarrier];
    
    self.playerName = [[UILabel alloc]initWithFrame:CGRectMake(60, 3, self.frame.size.width -60, 30)];
    [self addSubview:self.playerName];
     [self.playerName setFont: [UIFont fontWithName:@"BebasNeueBold" size:28.0]];
    
    self.playerPosition = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width-60, 0, 60, 30)];
    [self.playerPosition setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:self.playerPosition];
     [self.playerPosition setFont: [UIFont fontWithName:@"BebasNeueBold" size:19.0]];
    
    
    
    self.playerStats = [[UILabel alloc]initWithFrame:CGRectMake(59, 30, self.frame.size.width-60, 20)];
    [self addSubview:self.playerStats];
   [self.playerStats setFont: [UIFont fontWithName:@"BebasNeueBold" size:16.0]];
    
}


-(void)setLabelColors:(NSString *)color
{
    
    [self.playerNumber setTextColor:[UIColor colorWithHexString:color]];
    [self.playerTeamAbbr setTextColor:[UIColor colorWithHexString:color]];
    [self.playerName setTextColor:[UIColor colorWithHexString:color]];
    [self.playerStats setTextColor:[UIColor colorWithHexString:color]];
    [self.playerPosition setTextColor:[UIColor colorWithHexString:color]];
}


-(void)setBG:(NSString *)color
{
    [self setBackgroundColor:[UIColor colorWithHexString:color]];
}


-(void)setStats:(NSArray *)stats
{
    
    NSMutableString *string = [[NSMutableString alloc]init];
    
    for(NSDictionary *dict in stats)
    {
        
        NSString *type = [dict objectForKey:@"abbr"];
        NSString *amount = [dict objectForKey:@"amount"];
        
        NSString *formattedString = [NSString stringWithFormat:@" %@ %@ ",type,amount];
        
        [string appendString:formattedString];
        
        
    }
    
    [self.playerStats setText:string];
    
    
    
}

@end
