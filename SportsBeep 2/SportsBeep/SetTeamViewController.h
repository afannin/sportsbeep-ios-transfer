//
//  SetTeamViewController.h
//  SportsBeep
//
//  Created by iOSDev on 4/3/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetTeamViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *playerScrollView;
@property (weak, nonatomic) IBOutlet UIButton *menuBox;

@end
