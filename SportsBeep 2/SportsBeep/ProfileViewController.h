//
//  ProfileViewController.h
//  SportsBeep
//
//  Created by iOSDev on 3/31/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCameraViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface ProfileViewController : UIViewController <YCameraViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *playerProfileButton;
@property (weak, nonatomic) IBOutlet UIButton *saveProfileButton;
@property (weak, nonatomic) IBOutlet UIView *blueBox;
@property (weak, nonatomic) IBOutlet UIButton *cancelProfileButton;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property(nonatomic,strong)UIImageView *proImage;
@end
