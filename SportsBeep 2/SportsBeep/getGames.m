//
//  getGames.m
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/1/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import "getGames.h"

@implementation getGames






//sets the class variables to sport and sport level
-(void)setSPortAndType:(NSInteger)sport :(NSInteger)level :(NSInteger)futureGames
{
    sports *sp = [[sports alloc]init];
    
    self.sportLevel = level;
    
    self.sport = sport;
    
    
    self.gameTimeType = [sp getIsUpcomingGames:futureGames];
    
    self.sportString  = [sp getSport:sport];
    
    self.levelString = [sp getSportsType:level];
    
}


-(void)getGames{
    
    [self.delegate getGamesStartedToFetchGames];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *token = [defaults objectForKey:@"authentication_token"];
    NSString *authorizationString = [NSString stringWithFormat:@"Token token=%@",token];
    NSString *url = @"http://sportsbeep.com/api/mlb/games";
    
    NSDictionary *dict =@{
                          //  @"status":[NSString stringWithFormat:self.gameTimeType],
                          @"status":@"upcoming",
                          };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:authorizationString forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer = requestSerializer;
    
    [manager GET:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        
        [self.delegate getGamesHasFetchedGames:self.sport :self.sportLevel :responseObject];
        
        
        
        
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorMessage = operation.responseObject[@"error"];
        
        NSLog(@"GetGamesError: %@", errorMessage);
        
        [self.delegate getGamesFailedWithError:errorMessage];
    }];
    
    
}



@end
