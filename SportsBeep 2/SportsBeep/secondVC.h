//
//  secondVC.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 4/17/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "childVcDelegate.h"
#import "getGames.h"
#import <KVNProgress/KVNProgress.h>
#import "individualGame.h"
#import "gameInfoHolder.h"
#import "PickGameViewController.h"
#import "MenuBox.h"
#import "PickGameViewController.h"

@interface secondVC : UIViewController<getGamesProtocol,UICollectionViewDataSource,UICollectionViewDelegate>
{
    id<childVcDelegate>delegate;
}
@property (weak, nonatomic) IBOutlet UILabel *mLabel;
@property (weak, nonatomic) IBOutlet UILabel *mCtpLabel;

@property(nonatomic,weak)id delegate;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property(nonatomic,strong)MenuBox *mSlideUpMenu;
@end
