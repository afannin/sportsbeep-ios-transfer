//
//  TimeConversion.m
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/14/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import "TimeConversion.h"

@implementation TimeConversion
-(NSString *)getWeekDayAbbr
{
    NSArray *weekdayarray = [[NSArray alloc]init];
    weekdayarray = @[@"Sun",@"Mon",@"Tue",@"Wed",@"Thu",@"Fri",@"Sat"];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSISO8601Calendar];
    NSDateComponents* components = [calendar components:NSWeekdayCalendarUnit fromDate:self.date]; //
    return  [weekdayarray objectAtIndex:[components weekday] - 1];
}

-(NSString *)getWeekDayFull
{
    NSArray *weekdayarray = [[NSArray alloc]init];
    weekdayarray = @[@"Sunday",@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday"];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSISO8601Calendar];
    NSDateComponents* components = [calendar components:NSWeekdayCalendarUnit fromDate:self.date]; //
    return  [weekdayarray objectAtIndex:[components weekday] - 1];
}


-(NSString *)getYear
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSISO8601Calendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit fromDate:self.date];
    NSString *year = [[NSString alloc] initWithFormat: @"%ld",(long)[components year]];
    return year;
}


-(NSString *)getMonthTwoDigit
{
    NSArray *monthAbbr = [[NSArray alloc]init];
    monthAbbr    = @[@"JA",@"FE",@"MA",@"AP",@"MA",@"JU",@"JY",@"AU",@"SE",@"OC",@"NV",@"DC"];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSISO8601Calendar];
    NSDateComponents* components = [calendar components:NSMonthCalendarUnit fromDate:self.date];
    return [monthAbbr objectAtIndex:[components month] - 1];
}


-(NSString *)getMonthThreeDigit:(NSDate *)date
{
    NSArray *monthAbbr = [[NSArray alloc]init];
    monthAbbr    = @[@"Jan",@"Feb",@"Mar",@"Apr",@"May",@"Jun",@"Jul",@"Aug",@"Sep",@"Oct",@"Nov",@"Dec"];

    NSString *stringFromDate = [NSString stringWithFormat:@"%@",date];
    
    NSString *value =  [[NSString stringWithFormat:@"%@",stringFromDate] substringWithRange:NSMakeRange(5, 7)];
    
    NSLog(@"altered date %@",stringFromDate);
    
    // NSString *dateString = [NSDateFormatter localizedStringFromDate:self.date
    //                                                        dateStyle:NSDateFormatterShortStyle
    //            timeStyle:NSDateFormatterFullStyle];
    //   NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    //  [dateFormat setDateFormat:@"yyyy-M-DD HH:mm:SSSS"];
    //  NSDate *date = [dateFormat dateFromString:dateString];
    
    NSCalendar *calender = [NSCalendar currentCalendar];
    
    unsigned units = (NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|
                      NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit);
    NSDateComponents *components = [calender components:units
                                               fromDate:self.date];
    
    
    NSLog(@"the hell %ld",[stringFromDate integerValue]);
    
    return [monthAbbr objectAtIndex:[components month] - 1];
    
    
    
}


-(NSString *)getDayOfMonth
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSISO8601Calendar];
    NSDateComponents* components = [calendar components:NSDayCalendarUnit fromDate:self.date];
    NSString *day =  [[NSString alloc] initWithFormat: @"%d",[components day]];
    return day;
}



-(Boolean)isPastDate
{
    
    NSDate * today = [NSDate date];
    NSComparisonResult result = [today compare:self.date];
    
    Boolean code;
    
    switch (result)
    {
        case NSOrderedAscending:
            code = NO;
            break;
        case NSOrderedDescending:
            code = YES;
            break;
        case NSOrderedSame:
            code = YES;
            break;
        default:
            code = YES;
            break;
    }
    
    return code;
}


@end
