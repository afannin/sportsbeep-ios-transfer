//
//  bigTriangleHolder.m
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/29/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import "bigTriangleHolder.h"

@implementation bigTriangleHolder

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //     NSLog(@"%f",self.frame.size.width);
        
    }
    return self;
}




-(void)addGameTriangles :(NSDictionary*)homeTeam :(NSDictionary *)awayTeam :(CGFloat)cellWidth :(NSString *)time :(NSString *)date{
    
    
    
    
    
    
    if(!self.bottomT)
    {
        CGFloat triangleHeight =  self.frame.size.height;
        CGFloat triangleWidth  = cellWidth;
        
        self.bottomT = nil;
        self.topT = nil;
        
        
        UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, triangleWidth, 20)];
        [v setBackgroundColor:[UIColor blackColor]];
        [self addSubview:v];
        
        
        self.dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, (triangleWidth/2) + 10, 20)];
        [self.dateLabel setTextColor:[UIColor whiteColor]];
          [self.dateLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:14.0]];
        [self.dateLabel setText:date];
   
        
        
        self.timeLabel = [[UILabel alloc]initWithFrame:CGRectMake((triangleWidth-23)/2, 0, triangleWidth/2, 20)];
        [self.timeLabel setTextColor:[UIColor whiteColor]];
        [self.timeLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:14.0]];
        
        [self.timeLabel setText:time];
     
        [self.timeLabel setTextAlignment:NSTextAlignmentRight];
        [v addSubview:self.timeLabel];
        
        [v addSubview:self.dateLabel];
        
        
        
        UIColor *pHomeColor = [UIColor colorWithHexString:[homeTeam objectForKey:@"primary_color"]];
        UIColor *aHomeColor = [UIColor colorWithHexString:[homeTeam objectForKey:@"secondary_color"]];
        
        UIColor *aAwayColor = [UIColor colorWithHexString:[awayTeam objectForKey:@"secondary_color"]];
        UIColor *pAwayColor = [UIColor colorWithHexString:[awayTeam objectForKey:@"primary_color"]];
        
        
        
        
        
        
        self.bottomT = [[bottomTriangle alloc]initWithFrame:CGRectMake(0, 20,triangleWidth,triangleHeight)];
        [self.bottomT setBackgroundColor:pAwayColor];
        self.bottomT.textColor = aAwayColor;
        [self.bottomT addLabels:[awayTeam objectForKey:@"market"] :[awayTeam objectForKey:@"name"]];
        
        
        
        self.topT = [[topTriangle alloc]initWithFrame:CGRectMake(0,20,triangleWidth,triangleHeight)];
        [self.topT setBackgroundColor:pHomeColor];
        
        self.topT.textColor = aHomeColor;
        [self.topT addLabels:[homeTeam objectForKey:@"market"] :[homeTeam objectForKey:@"name"]];
        
        
        [self.topT.mainLabel setFrame:CGRectMake(self.topT.mainLabel.frame.origin.x, self.topT.mainLabel.frame.origin.y+5, self.topT.mainLabel.frame.size.width,  32)];
        [self.topT.subLabel setFrame:CGRectMake(self.topT.subLabel.frame.origin.x, self.topT.subLabel.frame.origin.y, self.topT.subLabel.frame.size.width,  25)];
        [self.topT.mainLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:31.0]];
        [self.topT.subLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:24.0]];
        [self addSubview:self.bottomT];
        [self addSubview:self.topT];
        [self bringSubviewToFront:v];
        
        
        self.im = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
        
        [self.im    setCenter:CGPointMake(triangleWidth/2,(triangleHeight/2)+20)];
        [self.im   setImage:[UIImage imageNamed:@"at"]];
        [self addSubview:self.im];
        [self.bottomT.mainLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:31.0]];
        [self.bottomT.mainLabel setFrame:CGRectMake(self.bottomT.mainLabel.frame.origin.x+5, self.bottomT.mainLabel.frame.origin.y-10, self.bottomT.mainLabel.frame.size.width,  32)];
        [self.bottomT.subLabel setFrame:CGRectMake(self.bottomT.subLabel.frame.origin.x+5, self.bottomT.subLabel.frame.origin.y-15, self.bottomT.subLabel.frame.size.width,  25)];
         [self.bottomT.subLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:24.0]];
     
    }
    else
    {
        [self changeTriangleColors:homeTeam :awayTeam:date:time];
        
    }
}



-(void)changeTriangleColors :(NSDictionary*)homeTeam :(NSDictionary *)awayTeam :(NSString *)date :(NSString *)time
{
    
    
    UIColor *pHomeColor = [UIColor colorWithHexString:[homeTeam objectForKey:@"primary_color"]];
    UIColor *aHomeColor = [UIColor colorWithHexString:[homeTeam objectForKey:@"secondary_color"]];
    
    UIColor *aAwayColor = [UIColor colorWithHexString:[awayTeam objectForKey:@"secondary_color"]];
    UIColor *pAwayColor = [UIColor colorWithHexString:[awayTeam objectForKey:@"primary_color"]];
    
    if(!aAwayColor)
    {
        NSLog(@"couldnt get color");
    }
    
    
    [self.dateLabel setText:date];
    [self.timeLabel setText:time];
    
    self.bottomT.textColor = aAwayColor;
    [self.bottomT changeLabelText:[awayTeam objectForKey:@"market"] :[awayTeam objectForKey:@"name"]];
    
    [self.bottomT setBackgroundColor: pAwayColor];
    
    
    
    
    [self.topT setBackgroundColor:pHomeColor];
    self.topT.textColor = aHomeColor;
    [self.topT changeLabelText:[homeTeam objectForKey:@"market"] :[homeTeam objectForKey:@"name"]];
    
    
}
@end