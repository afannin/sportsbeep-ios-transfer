//
//  MyContestsViewController.h
//  SportsBeep
//
//  Created by iOSDev on 3/30/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Colors.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "liveContestsViewController.h"
#import "MenuBox.h"
#import "myContestsToggle.h"
@interface MyContestsViewController : UIViewController<UIScrollViewDelegate,liveContestsProtocol,MyContestsToggleProtocol>
@property (weak, nonatomic) IBOutlet UILabel *myContestsHeadLabel;
@property (weak, nonatomic) IBOutlet UIView *blackContainerBox;
@property (weak, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (weak, nonatomic) IBOutlet UIButton *menuBox;
@property (weak, nonatomic) IBOutlet UIButton *liveContestsButton;
@property (weak, nonatomic) IBOutlet UIButton *upcomingContestsButton;
@property (weak, nonatomic) IBOutlet UIButton *contestsHistoryButton;
@property (weak, nonatomic) IBOutlet UIButton *incompleteContestsButton;
@property(nonatomic,strong)NSString *sportToPicks;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property(nonatomic,strong)NSMutableArray *holderViewArray;
@property(nonatomic,assign)int currentPage;
@property(nonatomic,strong)MenuBox *mSlideUpMenu;

@property (weak, nonatomic) IBOutlet UILabel *mLabel;

-(void) displayContests:(NSString *)aStatus;
-(void)setScrollOffset:(int)pos;
-(void)scrollViewDidScroll:(UIScrollView *)scrollView;

@end
