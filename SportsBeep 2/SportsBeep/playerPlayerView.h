//
//  playerPlayerView.h
//  swipeScreenVCs
//
//  Created by Daniel Nasello on 5/27/15.
//  Copyright (c) 2015 Combustion Innovation Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+Expanded.h"
#import "gamePlayer.h"
@interface playerPlayerView : UIView
@property(nonatomic,strong)gamePlayer *gPlayer;
@property(nonatomic,strong)NSString *playerId;
@property(nonatomic,strong)UILabel *playerNumber;
@property(nonatomic,strong)UILabel *playerTeamAbbr;
@property(nonatomic,strong)UILabel *playerName;
@property(nonatomic,strong)UILabel *playerStats;
@property(nonatomic,strong)UILabel *playerPosition;
-(void)setLabelColors:(NSString *)color;
-(void)setBG:(NSString *)color;
-(void)setStats:(NSArray *)stats;
@end
