//
//  SBApiAccess.m
//  SportsBeep
//
//  Created by Rafi Chehirian on 6/17/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "SBApiAccess.h"


@implementation SBApiAccess

-(void)logout {
    
    
    
    NSString *url = @"http://sportsbeep.com/api/users/logout";
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults valueForKey:@"authentication_token"];
    NSString *authString = [NSString stringWithFormat:@"Token token=%@",token];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:authString forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer = requestSerializer;
    
    [manager DELETE:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        
        [defaults setObject:NULL forKey:@"authentication_token"];
        [defaults setObject:NULL forKey:@"usernam"];
        [defaults setObject:NULL forKey:@"password"];
        NSString *token = [defaults valueForKey:@"authentication_token"];
        NSLog(@"%@",token);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}


@end
