//
//  User.h
//  SportsBeep
//
//  Created by iOSDev on 4/6/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property (nonatomic, retain) NSString *authenticationToken;
@property (nonatomic, retain) NSString *avatarUrl;
@property (nonatomic, retain) NSString *createdAt;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *firstName;
@property (nonatomic, retain) NSString *userId;
@property (nonatomic, retain) NSString *lastName;
@property (nonatomic, retain) NSString *updatedAt;
@property (nonatomic, retain) NSString *userName;

@end
