//
//  sportTab.m
//  SportsBeep
//
//  Created by iOSDev on 5/21/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "Colors.h"
#import "sportTab.h"

@implementation sportTab

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setClipsToBounds:YES];
        self.canClick = YES;
        self.isSelected = NO;
        [self createTabs];
    }
    return self;
}



-(void)createTabs
{
    
      self.layer.borderWidth = 1.0f;
      self.layer.borderColor = PANEL_BG_BLUE.CGColor;
  
    
    
    self.topSpacerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 4)];
    [self addSubview:self.topSpacerView];
    
    self.iconView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, self.frame.size.width, self.frame.size.height/2)];
    [self.iconView setContentMode:UIViewContentModeScaleAspectFit];
    [self addSubview:self.iconView];
    
    
    self.sportName = [[UILabel alloc]initWithFrame:CGRectMake(0, self.frame.size.height-22, self.frame.size.width,20)];
    [self.sportName setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:self.sportName];
    [self.sportName setTextColor:[UIColor whiteColor]];
    [self.sportName setFont: [UIFont fontWithName:@"BebasNeueBold" size:16.0]];
    

    
}



-(void)setFakeDisabled
{
    if(self.canClick)
    {
        self.canClick = NO;
        
        [self setBackgroundColor:HINT_COLOR];
           self.layer.borderWidth = 1.0f;
        self.layer.borderColor = PANEL_BG_BLUE.CGColor;

    }

}



-(void)setFakeEnabled
{
    if(!self.canClick)
    {
        self.canClick = YES;
        
    }
}


-(void)setFakeSelected
{
    
    if(!self.isSelected)
    {
        
        self.isSelected = YES;
          self.layer.borderWidth = 4.0f;
        self.layer.borderColor = RED.CGColor;
    }
    
}

-(void)setFakeUnselected
{
    if(self.isSelected)
    {
        self.isSelected = NO;
        
        self.layer.borderWidth = 1.0f;
        self.layer.borderColor = PANEL_BG_BLUE.CGColor;
        
    }
    
}

@end


