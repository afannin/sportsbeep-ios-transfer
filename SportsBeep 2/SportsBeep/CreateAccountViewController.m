//
//  CreateAccountViewController.m
//  SportsBeep
//
//  Created by iOSDev on 3/26/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "CreateAccountViewController.h"
#import "Colors.h"
#import "LoginManager.h"

@interface CreateAccountViewController () {
    LoginManager *lim;
}

@end

@implementation CreateAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initiateObjectSettings];  
}

- (void) initiateObjectSettings {
    //  background
    UIImage *bgImage = [UIImage imageNamed:@"baseball.png"];
    [self.mainBG setImage: bgImage];
    self.mainBG.contentMode = UIViewContentModeScaleAspectFill;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"baseball.png"]];
    
    
    [self.navigationController.navigationBar setOpaque:YES];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    
    
    // blue container box
    self.blueBgBox.backgroundColor = PANEL_BG_BLUE;
    
    // username field
    UIImageView *person = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"profile_icon"]];
    [person setFrame:CGRectMake(0, 0, 55, (self.createUserNameField.frame.size.height)*0.7)];
    person.layer.masksToBounds = YES;
    [person setContentMode: UIViewContentModeScaleAspectFit];
    [self.createUserNameField setLeftViewMode:UITextFieldViewModeAlways];
    [self.createUserNameField setLeftView: person];
    self.createUserNameField.backgroundColor = BLUE_GRAY;
    self.createUserNameField.rightViewMode = UITextFieldViewModeAlways;
    self.createUserNameField.layer.borderWidth = 1.0f;
    self.createUserNameField.layer.borderColor = [[UIColor blackColor] CGColor];
    self.createUserNameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"User Name" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    
    
    // email field
    UIImageView *envelope = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"email_icon"]];
    [envelope setFrame:CGRectMake(0, 0, 55, (self.createEmailField.frame.size.height)*0.7)];
    envelope.layer.masksToBounds = YES;
    [envelope setContentMode: UIViewContentModeScaleAspectFit];
    [self.createEmailField setLeftViewMode:UITextFieldViewModeAlways];
    [self.createEmailField setLeftView: envelope];
    self.createEmailField.backgroundColor = BLUE_GRAY;
    self.createEmailField.rightViewMode = UITextFieldViewModeAlways;
    self.createEmailField.layer.borderWidth = 1.0f;
    self.createEmailField.layer.borderColor = [[UIColor blackColor] CGColor];
    self.createEmailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    
    
    
    // password field
    UIImageView *lock = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password_icon"]];
    [lock setFrame:CGRectMake(0, 0, 55, (self.createPasswordField.frame.size.height)*0.7)];
    lock.layer.masksToBounds = YES;
    [lock setContentMode: UIViewContentModeScaleAspectFit];
    [self.createPasswordField setLeftViewMode:UITextFieldViewModeAlways];
    [self.createPasswordField setLeftView: lock];
    self.createPasswordField.backgroundColor = BLUE_GRAY;
    self.createPasswordField.rightViewMode = UITextFieldViewModeAlways;
    self.createPasswordField.layer.borderWidth = 1.0f;
    self.createPasswordField.layer.borderColor = [[UIColor blackColor] CGColor];
    self.createPasswordField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    
    
    // promo code field
    UIImageView *star = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"promo_code_icon"]];
    [star setFrame:CGRectMake(0, 0, 55, (self.enterPromoCodeField.frame.size.height)*0.7)];
    star.layer.masksToBounds = YES;
    [star setContentMode: UIViewContentModeScaleAspectFit];
    [self.enterPromoCodeField setLeftViewMode:UITextFieldViewModeAlways];
    [self.enterPromoCodeField setLeftView: star];
    self.enterPromoCodeField.backgroundColor = BLUE_GRAY;
    self.enterPromoCodeField.rightViewMode = UITextFieldViewModeAlways;
    self.enterPromoCodeField.layer.borderWidth = 1.0f;
    self.enterPromoCodeField.layer.borderColor = [[UIColor blackColor] CGColor];
    self.enterPromoCodeField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Promo Code (Optional)" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    
    // create account button
    self.createAccountButton.backgroundColor = BLUE;
    
}

// Incorrect login
-(void)PushLoginError: (NSString*) errorMessage {
    
    // Login Manager variable
    lim = nil;
    
    // login error alert
    // create and display toast error message
    ToastMessage *locToastMessage = [[ToastMessage alloc] initWithMessage:errorMessage andWithRequestingView: self.view];
    [locToastMessage displayToastMessage];
    
    // clear text fields
}

-(void)PushCreatedAccount
{
    // Log in Manager Variable
    lim = nil;
    
    // present New Game View Controller
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"FavoriteBaseballTeam"];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:NULL];
}

// Create account
-(void)loginManagerCreateAccount: (NSString*) username withEmail: (NSString*) email withPassword: (NSString*) password
{
    // initialize log in manager
    if(!lim)
    {
        lim = [[LoginManager alloc]init];
        lim.delegate = self;
    }
    
    // create account
    
    [lim createAccount:username withEmail:email withPassword:password];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)dismissCreateAccount:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.layer.borderColor=[[UIColor cyanColor] CGColor];
     [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height * 1.3)];
     [UIView animateWithDuration:0.4f animations:^{
        self.scrollView.contentOffset = CGPointMake(0, 100);
    }];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    textField.layer.borderColor=[[UIColor blackColor] CGColor];
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [UIView animateWithDuration:0.4f animations:^{
        self.scrollView.contentOffset = CGPointMake(0, 0);}];
    [self.view endEditing:YES];
    [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height )];
    return YES;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)createAccountButton:(id)sender {
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self loginManagerCreateAccount: self.createUserNameField.text withEmail:self.createEmailField.text withPassword:self.createPasswordField.text];
}


- (BOOL) formIsValid {
    return YES;
}
@end
