//
//  ScoringBreakdownController.m
//  SportsBeep
//
//  Created by Daniel Nasello on 6/1/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "ScoringBreakdownController.h"
#import "PlayerScoringController.h"
#import "Colors.h"
@interface ScoringBreakdownController (){
    NSDictionary *teamDict;
    teamRectangleView *tr;
    NSMutableArray *arrayOfPlayers;
    NSString *myid;
    NSString *bestBustScore;
    CGFloat bbScore;
    CGFloat totalScore;
}

@end

@implementation ScoringBreakdownController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.sbLabel.backgroundColor = DARK_BLUE;
    self.view.backgroundColor = PANEL_BG_BLUE;
    
    arrayOfPlayers = [[NSMutableArray alloc]init];
    
    self.linearLayout = [[CSLinearLayoutView alloc]initWithFrame:CGRectMake(0, 120, self.view.frame.size.width, self.view.frame.size.height-120)];
    [self.view addSubview:self.linearLayout];
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)addGameTeam{
    tr= [[teamRectangleView alloc]initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, 60)];
    [tr addGameRectanges:self.game.awayTeamObject :self.game.homeTeamObject :self.view.frame.size.width :self.game.gameDay :self.game.gameTime];
    [self.view addSubview:tr];
   // NSLog(@"the whatever %@",[NSString stringWithFormat: @"%@",[self.game.homeTeamObject objectForKey:@"id"]]);
    
    tr.homeTeamId   =[NSString stringWithFormat: @"%@",[self.game.homeTeamObject objectForKey:@"id"]];
    tr.awayTeamId   =[NSString stringWithFormat: @"%@",[self.game.awayTeamObject objectForKey:@"id"]];
}


- (IBAction)doneButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)getContestStats:(contestPlayerView *)pv:(individualGame *)game:(NSString *)rid
{
    
    self.game = game;
    NSDictionary *hometeam = self.game.homeTeamObject;
    NSDictionary *awayteam = self.game.awayTeamObject;
    NSString *home_team_id = [NSString stringWithFormat:@"%@", [hometeam objectForKey:@"id"]];
    NSString *away_team_id =[NSString stringWithFormat:@"%@", [awayteam objectForKey:@"id"]];
    
    
    
    
    
    myid = rid;
    
       [self.sbLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:32.0]];
    
    
    //dicts that hold the teams informatiopn
    teamDict = [NSDictionary dictionaryWithObjectsAndKeys:
                hometeam, home_team_id,
                awayteam,away_team_id,
                nil];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults stringForKey:@"authentication_token"];
    NSString *tokenString = [NSString stringWithFormat:@"Token token=%@", token];
    self.pv = pv;
    
    NSLog(@"player dict %@",pv.playerDict);
    
    NSString *ticket_id = [pv.playerDict objectForKey:@"id"];
    
    NSDictionary *contest_dict = [pv.playerDict objectForKey:@"contest"];
    NSString *contest_id = [contest_dict objectForKey:@"id"];
    
    NSString *url =[NSString stringWithFormat:@"http://sportsbeep.com/api/mlb/tickets/%@/players",rid];
    
    bestBustScore = [pv.playerDict objectForKey:@"best_or_bust_score"];
    
    bbScore = [bestBustScore floatValue];
    totalScore =[[pv.playerDict objectForKey:@"score"] floatValue];
    [self addScoreUI];
    
    
    
    NSDictionary *dict =@{
                          //  @"status":[NSString stringWithFormat:self.gameTimeType],
                          @"ticket_id":ticket_id,
                          @"player_id": contest_id
                          };
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:tokenString forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer = requestSerializer;
    
    [manager GET:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON the res: %@", responseObject);
        
        
        NSArray *players = [responseObject objectForKey:@"players"];
        
        int ind=0;
        for(NSDictionary *d in players)
        {
            //UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0,299, 50)];
            
            scoreBreakdownPlayerView *v = [[scoreBreakdownPlayerView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
            
            
            NSDictionary *team_dict = [d objectForKey:@"team"];
            NSString *team_id = [team_dict objectForKey:@"id"];
            
            NSDictionary *to  = [teamDict objectForKey:[NSString stringWithFormat:@"%@",team_id]];
        
            
            
            
            NSString *mainColor = [to objectForKey:@"primary_color"];
            NSString *altColor = [to objectForKey:@"secondary_color"];
            
            NSString *headshot = [d objectForKey:@"headshot_url"];
            NSString *p_id = [d objectForKey:@"id"];
            
            v.player_id = p_id;
            v.player_headshot = headshot;
            [v.playerNumber setText:[NSString stringWithFormat:@"%@",[d objectForKey:@"number"]]];
            [v.playerName setText:[d objectForKey:@"name"] ];
            [v.playerPosition setText:[d objectForKey:@"position"]];
            [v.playerTeamAbbr setText:[to objectForKey:@"abbr"]];
            [v.playerPoints setText:[d objectForKey:@"score"]];
            
            [v.playerIngame setText:@"IN GAME SCORING"];
            
            
           // totalScore += [v.playerPoints.text floatValue];
            
            [v setBG:mainColor];
            [v setLabelColors:altColor];
            v.tag = ind;
            
            //  [self.playerViews addObject:v];
            
            UITapGestureRecognizer *singleFingerTap =
            [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(playerWasTapped:)];
            [v addGestureRecognizer:singleFingerTap];
            
            //[self.linearLayout setBackgroundColor:[UIColor redColor]];
            CSLinearLayoutItem *i = [CSLinearLayoutItem layoutItemForView:v];
            [self.linearLayout addItem:i];
            
            
            [arrayOfPlayers addObject:d];
            
            ind++;
        }
        
        
        
        
        [self addBustLayout];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorMessage = operation.responseObject[@"error"];
        
        NSLog(@"err: %@", errorMessage);
        
    }];
    
}

-(void)addBustLayout
{
    
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    [v setBackgroundColor:[UIColor blackColor]];
    
  
    

    
    UILabel  *l = [[UILabel alloc]initWithFrame:CGRectMake(60, 0,200, 50)];
    [l setFont: [UIFont fontWithName:@"BebasNeueBold" size:25.0f]];
 
    [l setTextColor:[UIColor whiteColor]];
    [l setText:@"BEST & BUST"];
    [v addSubview:l];
    
    UILabel *b = [[UILabel alloc]initWithFrame:CGRectMake(v.frame.size.width-60, 0, 40, 50)];
    [b setTextColor:[UIColor whiteColor]];
 
    [b setText:bestBustScore];
    [v addSubview:b];

    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(goToBusts)];
    [v addGestureRecognizer:singleFingerTap];
    
    
     CSLinearLayoutItem *i = [CSLinearLayoutItem layoutItemForView:v];
    [self.linearLayout   addItem:i];
   
}

-(void)addScoreUI
{
    
    UIView *holder = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-80, self.view.frame.size.width, 80)];
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width, 35)];
    [v setBackgroundColor:[UIColor grayColor]];
    [holder addSubview:v];
    

    UILabel *total = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2, 0, self.view.frame.size.width/2, 30)];
    [total setTextColor:[UIColor whiteColor]];
    [total setTextAlignment:NSTextAlignmentCenter];
    [total setText:@"Total"];
    [v addSubview:total];
    
    
    
    UIView *bottom = [[UIView alloc]initWithFrame:CGRectMake(0, 30, self.view.frame.size.width, 50)];
    [bottom setBackgroundColor:PANEL_BG_BLUE];
    [holder addSubview:bottom];
    
    
    
    UILabel *tLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width-150, 0, 150, 50)];
    [tLabel setTextAlignment:NSTextAlignmentRight];
    [tLabel setFont: [UIFont fontWithName:@"BebasNeueBold" size:35.0f]];
    [tLabel setTextColor:GREEN];
    [tLabel setText:[NSString stringWithFormat:@"%.01f POINTS ",totalScore]];
    [bottom addSubview:tLabel];
    
    
    [self.view addSubview:holder];
    
}

-(void)goToBusts
{
    teamRectangleView *f = (teamRectangleView *)[tr snapshotViewAfterScreenUpdates:YES];
    NSString *tid = myid;
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    PlayerBustsController * vc = [storyboard instantiateViewControllerWithIdentifier:@"playerb"];
    vc.awayDict = self.game.awayTeamObject;
    vc.homeDict = self.game.homeTeamObject;
    [vc addTopView:f];
    [vc getBestBusts:myid];

    [self presentViewController:vc animated:YES completion:nil];
}


-(void)playerWasTapped:(UIGestureRecognizer *)r
{
    
    NSInteger tag = r.view.tag;
    
    scoreBreakdownPlayerView * v = (scoreBreakdownPlayerView *)r.view;
    
    NSString *p_id  = v.player_id;
    NSString *p_head = v.player_headshot;
      NSString *pname = v.playerName.text;
    teamRectangleView *f = (teamRectangleView *)[tr snapshotViewAfterScreenUpdates:YES];
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    PlayerScoringController * vc = [storyboard instantiateViewControllerWithIdentifier:@"playersc"];
    [vc addTopView:f];
    [vc getPStats:p_id:myid:p_head:pname];
    [vc addPlayerGameStats:[arrayOfPlayers objectAtIndex:tag]];
    [self presentViewController:vc animated:YES completion:nil];
    
    

}

@end
