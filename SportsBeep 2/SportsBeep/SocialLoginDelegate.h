//
//  SocialLoginDelegate.h
//  h6
//
//  Created by Daniel Nasello on 10/9/14.
//  Copyright (c) 2014 Combustion Innovation Group. All rights reserved.
//


//  SocialLoginDelegate.h
//  Push
//
//  Created by Daniel Nasello on 9/18/14.
//  Copyright (c) 2014 Combustion Innovation Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SocialLoginDelegate <NSObject>

-(void)PushLoginError:(NSString *)message;
-(void)PushUpdatedLogin;
-(void)PushUpdatedLogout;
-(void)PushCreatedAccount;

@end
