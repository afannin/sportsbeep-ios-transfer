//
//  myContestsToggle.h
//  SportsBeep
//
//  Created by Rafi Chehirian on 6/23/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyContestsToggleProtocol.h"
@interface myContestsToggle : UIView{
    id<MyContestsToggleProtocol>delegate;
}
@property(nonatomic,strong)id delegate;
@property(nonatomic,strong)NSMutableArray *buttons;
@property(nonatomic,strong)UIButton *selectedButton;
@property(nonatomic,assign)NSInteger selectedIndex;
-(void)manuallyToggleButtons:(NSInteger)index;

@end
