//
//  SBApiAccess.h
//  SportsBeep
//
//  Created by Rafi Chehirian on 6/17/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface SBApiAccess : NSObject
-(void)logout;
@end
