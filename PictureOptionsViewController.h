//
//  PictureOptionsViewController.h
//  SportsBeep
//
//  Created by iOSDev on 3/31/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PictureOptionsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *blueBoxView;
@property (weak, nonatomic) IBOutlet UIButton *uploadPhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *takePhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@end
