//
//  PictureOptionsViewController.m
//  SportsBeep
//
//  Created by iOSDev on 3/31/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "PictureOptionsViewController.h"
#import "Colors.h"

@interface PictureOptionsViewController ()

@end

@implementation PictureOptionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // blue box
    self.blueBoxView.backgroundColor = PANEL_BG_BLUE;
    self.blueBoxView.layer.cornerRadius = 5.f;
    self.blueBoxView.layer.borderWidth = 1.0f;
    self.blueBoxView.layer.borderColor = [[UIColor blackColor] CGColor];
    
    // button borders
    self.takePhotoButton.layer.borderWidth = 1.0f;
    self.takePhotoButton.layer.borderColor = [[UIColor blackColor] CGColor];
    
    self.removeButton.layer.borderWidth = 1.0f;
    self.removeButton.layer.borderColor = [[UIColor blackColor] CGColor];
    
}
- (IBAction)cancelPictureOptions:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
