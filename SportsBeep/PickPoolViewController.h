//
//  PickPoolViewController.h
//  SportsBeep
//
//  Created by iOSDev on 4/3/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickPoolViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *pickPoolHeadLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickPoolTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyTextLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *pickPoolScrollView;

@property (strong, nonatomic)  UIButton *cancelBtn;
@property (strong, nonatomic)  UIButton *yesBtn;

@property (strong, nonatomic)  UIView *cover;
@property (strong, nonatomic)  UIView *poolConfirmBox;




-(void)initializeView;

@end
