//
//  getMyContests.m
//  SportsBeep
//
//  Created by iOSDev on 5/22/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "getMyContests.h"
#import "AFNetworking.h"

@implementation getMyContests

-(void)getContests:(NSString *)aStatus
{
    
    [self.delegate getContestsHasStarted];
    NSString *url = @"http://sportsbeep.com//api/mlb/contests";
    
    NSDictionary *params = @{
                             @"status":aStatus
                             };
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults stringForKey:@"authentication_token"];
    NSString *tokenString = [NSString stringWithFormat:@"Token token=%@", token];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:tokenString forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer = requestSerializer;
    
    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response Code %ld",(long)[operation.response statusCode]);
        NSLog(@"JSON: %@", responseObject);
        
        [self.delegate getContestsDidSucceed:responseObject];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorMessage = operation.responseObject[@"error"];
      
        [self.delegate getContestsDidFail:errorMessage];
    }];
}



@end
