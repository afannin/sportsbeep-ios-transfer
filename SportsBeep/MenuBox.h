//
//  MenuBox.h
//  SportsBeep
//
//  Created by iOSDev on 5/29/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MenuBoxDelegate.h"
#import "DragButton.h"

@interface MenuBox : UIView<DragButtonDelegate>{
    id<MenuBoxDelegate>delegate;
}

//background color of the buttons
@property(nonatomic,assign)UIColor *bgColor;
//hightlightedcolor
@property(nonatomic,assign)UIColor *highlightedColor;
//selectedColor
@property(nonatomic,assign)UIColor *selectedColor;
//disabledColor
@property(nonatomic,assign)UIColor *disabledColor;


//the arrray that holds references to the buttons in the toggle view
@property(nonatomic,strong)NSMutableArray *buttonArray;

//titles of the button set so you can dynamically add b uttons
@property(nonatomic,strong)NSArray *buttonTitles;


//fontcolor
@property(nonatomic,assign)UIColor *textColor;


//if the drawer is open
@property(nonatomic,assign)BOOL isOpen;

//if we are allowed to click any of our buttons
@property(nonatomic,assign)BOOL canClick;

@property(nonatomic,weak)id delegate;

//add all the buttons to the view
-(void)addButtons;


//the original frame
@property(nonatomic,assign)CGRect originalFrame;


//the drag button taht allows us to drag the drawer
@property(nonatomic,strong)DragButton *mdragButton;

//the current frame..ie if we are down it will be different than the one if its open we need to know this for the drag offset
@property(nonatomic,assign)CGRect currentFrame;

//offset frame is so we know the current frame as we drag.
@property(nonatomic,assign)CGRect offsetFrame;


@property(nonatomic,assign)NSInteger upOffset;

@end