//
//  BuildTeamViewController.h
//  SportsBeep
//
//  Created by iOSDev on 3/30/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuildTeamViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *menuBox;
@property (weak, nonatomic) IBOutlet UIScrollView *buildTeamScrollView;


@end
