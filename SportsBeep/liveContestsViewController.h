//
//  liveContestsViewController.h
//  SportsBeep
//
//  Created by iOSDev on 5/22/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "getMyContests.h"
#import "contestsCell.h"
#import "liveContestsProtocol.h"
@interface liveContestsViewController : UIViewController<getMyContestsProtocol,UITableViewDataSource,UITableViewDelegate>{
    id<liveContestsProtocol>delegate;
}



@property(nonatomic,weak)id delegate;
@property (weak, nonatomic) IBOutlet UITableView *sasaS;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong)NSString *gameStatus;
@end
