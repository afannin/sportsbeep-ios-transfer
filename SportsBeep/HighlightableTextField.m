//
//  HighlightableTextField.m
//  SportsBeep
//
//  Created by iOSDev on 3/31/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "HighlightableTextField.h"
#import "Colors.h"

@implementation HighlightableTextField

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setBackgroundColor:BLUE_GRAY];
        self.layer.borderWidth = 1.0f;
        self.layer.borderColor=[[UIColor blackColor] CGColor];
    }
    return self;
}

- (BOOL)becomeFirstResponder {
    BOOL outcome = [super becomeFirstResponder];
    if (outcome) {
         [self setBackgroundColor: BLUE_GRAY];
          self.layer.borderColor=[[UIColor cyanColor] CGColor];
    }
    return outcome;
}

- (BOOL)resignFirstResponder {
    BOOL outcome = [super resignFirstResponder];
    if (outcome) {
        [self setBackgroundColor: BLUE_GRAY];
        self.layer.borderColor=[[UIColor blackColor] CGColor];
    }
    return outcome;
}


@end
