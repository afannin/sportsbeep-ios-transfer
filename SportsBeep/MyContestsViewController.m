//
//  MyContestsViewController.m
//  SportsBeep
//
//  Created by iOSDev on 3/30/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "MyContestsViewController.h"
#import "liveContestsViewController.h"
#import "specificContestViewController.h"


@interface MyContestsViewController (){
    NSMutableArray *vcHolder;
}

@end

@implementation MyContestsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //holds reference to all the vcs so they do not get put in garbage
    vcHolder = [[NSMutableArray alloc]init];
    
 CGRect main =    [[UIScreen mainScreen] bounds];
    [self.scrollView setContentSize:CGSizeMake(main.size.width *4,self.scrollView.frame.size.height)];
    [self.scrollView  setPagingEnabled:YES];
    [self.scrollView setDelegate:self];
    self.holderViewArray = [[NSMutableArray alloc]init];
    
    NSArray *randColors = [[NSArray alloc]init];
    randColors = @[
                   [UIColor greenColor],
                   [UIColor redColor],
                   [UIColor blueColor],
                   [UIColor orangeColor],];
    
    for(int i = 0;i<4;i++)
    {
        UIView *holderView = [[UIView alloc]initWithFrame:CGRectMake(main.size.width * i, 0, main.size.width, self.scrollView.frame.size.height)];
        [holderView setBackgroundColor:[randColors objectAtIndex:i ]];
        [self.scrollView addSubview:holderView];
        [self.holderViewArray addObject:holderView];
    }
    
    
    NSArray *status= [[NSArray alloc]init];
    status = @[@"live",@"upcoming",@"past",@"abcd"];
    int i = 0;
    for(UIView *v in self.holderViewArray)
    {
        liveContestsViewController *lc = [self.storyboard instantiateViewControllerWithIdentifier:@"LiveContests"];
        lc.gameStatus = [status objectAtIndex:i];
        
        [v addSubview:lc.view];
        [lc setDelegate:self];
        [vcHolder addObject:lc];
        [lc didMoveToParentViewController:self];
        i++;
    }
  
    
    
    
    
    //label
    UILabel *headLabel = self.myContestsHeadLabel;
    headLabel.backgroundColor = DARK_BLUE;
    headLabel.textColor = [UIColor whiteColor];
    
    
    // black container box
    UIView *containerBox = self.blackContainerBox;
    containerBox.backgroundColor = [UIColor blackColor];
    
    // button
    UIButton *menuBox = self.menuBox;
    menuBox.backgroundColor = GREEN;
    
    CGFloat totalBlankSpace = 100.f;
    CGFloat buttonsSpace = self.view.frame.size.width - totalBlankSpace;
    
    CGFloat buttonWidth = buttonsSpace/4;
    
    CGRect buttonFrame = self.liveContestsButton.frame;
    
    buttonFrame.size = CGSizeMake(buttonWidth, 56);
    self.liveContestsButton.frame = buttonFrame;
    self.liveContestsButton.layer.cornerRadius = 5.f;
    [self.liveContestsButton setTitle: @"LIVE CONTESTS" forState: UIControlStateNormal];
    [self.liveContestsButton.titleLabel setTextAlignment: NSTextAlignmentCenter];
    self.liveContestsButton.backgroundColor = RED;
    
    self.upcomingContestsButton.frame = buttonFrame;
    self.upcomingContestsButton.layer.cornerRadius = 5.f;
    [self.upcomingContestsButton setTitle: @"UPCOMING CONTESTS" forState: UIControlStateNormal];
    [self.upcomingContestsButton.titleLabel setTextAlignment: NSTextAlignmentCenter];
    self.upcomingContestsButton.backgroundColor = BLUE_GRAY;
    
    self.contestsHistoryButton.frame = buttonFrame;
    self.contestsHistoryButton.layer.cornerRadius = 5.f;
    [self.contestsHistoryButton setTitle: @"CONTESTS HISTORY" forState: UIControlStateNormal];
    [self.contestsHistoryButton.titleLabel setTextAlignment: NSTextAlignmentCenter];
    self.contestsHistoryButton.backgroundColor = BLUE_GRAY;
    
    self.incompleteContestsButton.frame = buttonFrame;
    self.incompleteContestsButton.layer.cornerRadius = 5.f;
    [self.incompleteContestsButton setTitle: @"INCOMPLETE CONTESTS" forState: UIControlStateNormal];
    [self.incompleteContestsButton.titleLabel setTextAlignment: NSTextAlignmentCenter];
    self.incompleteContestsButton.backgroundColor = BLUE_GRAY;
    
}
- (IBAction)liveContests:(id)sender {
    int configuration = 0;
    [self updateButtonColors:configuration];
    self.liveContestsButton.backgroundColor = RED;
    [self setScrollOffset:configuration];
   // [self displayContests:@"live"];
    
}
- (IBAction)upcomingContests:(id)sender {
    int configuration = 1;
    [self updateButtonColors:configuration];
    [self setScrollOffset:configuration];
    //[self displayContests:@"upcoming"];

}
- (IBAction)contestsHistory:(id)sender {
    int configuration = 2;
    [self updateButtonColors:configuration];
    [self setScrollOffset:configuration];
  //  [self displayContests:@"past"];

}
- (IBAction)incompleteContests:(id)sender {
    int configuration = 3;
    [self updateButtonColors:configuration];
    [self setScrollOffset:configuration];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateButtonColors: (int) configuration {
    NSArray *buttonArray = [NSArray arrayWithObjects:self.liveContestsButton, self.upcomingContestsButton,self.contestsHistoryButton,self.incompleteContestsButton, nil];
    NSInteger buttonCount = buttonArray.count;
    for (int i = 0; i<buttonCount; i++) {
        UIButton *currentButton = buttonArray[i];
        if (i == configuration){
            currentButton.backgroundColor = RED;
        } else {
            currentButton.backgroundColor = BLUE_GRAY;
        }
    }
}


-(void)setScrollOffset:(int)pos
{
    self.currentPage = pos;
    [self.scrollView setContentOffset:CGPointMake(self.scrollView.frame.size.width * pos, 0) animated:YES];
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    self.currentPage = page;
 
    
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self updateButtonColors:self.currentPage];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self updateButtonColors:self.currentPage];
}


-(void) displayContests : aStatus {
    NSString *url = @"http://sportsbeep.com//api/mlb/contests";
    
    NSDictionary *params = @{
                             @"status":aStatus
                             };
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults stringForKey:@"authentication_token"];
    NSString *tokenString = [NSString stringWithFormat:@"Token token=%@", token];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:tokenString forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer = requestSerializer;
    
    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response Code %ld",(long)[operation.response statusCode]);
        NSLog(@"JSON: %@", responseObject);
        
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //NSString *errorMessage = operation.responseObject[@"error"];
        NSLog(@"upcoming contests error");
    }];
    
}


//live contests delegate
-(void)contestWasSelected:(individualGame *)g
{
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    specificContestViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"specificContest"];
    [self presentViewController:vc animated:YES completion:nil];
    [vc addGame:g];
}

//end delegate
@end
