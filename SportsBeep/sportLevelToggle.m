//
//  sportLevelToggle.m
//  SportsBeep
//
//  Created by iOSDev on 5/21/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "Colors.h"
#import "sportLevelToggle.h"

@implementation sportLevelToggle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.selectedIndex = 100;
        [self createTabs];
    }
    return self;
}



-(void)createTabs
{
    
    
    UIImage *pressed = [self imageWithColor:BABY_BLUE];
    UIImage *pressedinter = [self imageWithColor: HINT_COLOR];
    UIImage *notpressed = [self imageWithColor: HINT_COLOR];
    UIImage *disabled = [self imageWithColor:HINT_COLOR];
    NSArray *sportLevels = [[NSArray alloc]init];
    sportLevels = @[@"PROFESSIONAL",@"COLLEGE",@"INTERNATIONAL"];
    
    int i = 0;
    
    
    for(NSString *level in sportLevels)
    {
        NSInteger offset = 0;
        if(i>0){
            offset = 5;
    
        }
        
        NSUInteger back = 0;
        if(i == 1)
        {
            back = 5;
        }
        
        UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
        b.frame = CGRectMake((((self.frame.size.width/sportLevels.count-back)) * i) +(offset), 0, (self.frame.size.width/sportLevels.count-5) + back, 30);
        // [b setTitle:label forState:UIControlStateNormal];
        b.titleLabel.font = [UIFont systemFontOfSize:10];
        b.clipsToBounds = YES;
        b.tag = i;
        b.layer.cornerRadius = 3.0f;
        [b setAttributedTitle:[[NSAttributedString alloc] initWithString:level attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}]  forState:UIControlStateSelected];
        [b setAttributedTitle:[[NSAttributedString alloc] initWithString:level attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}]  forState:UIControlStateHighlighted];
        [b setAttributedTitle:[[NSAttributedString alloc] initWithString:level attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}]  forState:UIControlStateNormal];
        [b addTarget:self action:@selector(toogleTheButton:) forControlEvents:UIControlEventTouchUpInside];
       // [b.titleLabel setFont:[UIFont fontWithName:@"GillSans" size:13.0]];
        [b setBackgroundImage:notpressed forState:UIControlStateNormal];
        [b setBackgroundImage:pressedinter forState:UIControlStateHighlighted];
        [b setBackgroundImage:pressed forState:UIControlStateSelected];
         [b setBackgroundImage:disabled forState:UIControlStateDisabled];
        if(i>0)
        {
            [b setEnabled:NO];
        }
        else
        {
            [b setSelected:YES];
             b.layer.borderWidth = 3;
             b.layer.borderColor = RED.CGColor;
        }
        
        [self addSubview:b];
        [self.toggleButtons addObject:b];
        i++;
    }
        
    UIButton *initial = [self.toggleButtons objectAtIndex:0];
    initial.selected = YES;
    self.selectedButton = initial;
    self.selectedButton.layer.borderWidth = 3;
    self.selectedButton.layer.borderColor = RED.CGColor;
}


-(void)toogleTheButton:(id)sender
{
    UIButton *b = (UIButton*)sender;
    if(b != self.selectedButton)
    {
        //UIButton *goButton = [self.toggleButtons objectAtIndex:self.selectedIndex];
        self.selectedButton.selected = NO;
        self.selectedButton.layer.borderWidth = 0;
        
        b.selected = YES;
        b.layer.borderWidth = 3;
         self.selectedButton.layer.borderColor = RED.CGColor;
        self.selectedButton = b;
        self.selectedIndex = b.tag;
        //do delegation to let the controller know that the index of the tab changed.
        //[self.delegate tabWasChanged:b.tag];
    }
}

-(void)toggleMyButton:(NSInteger)selectedValue
{
    UIButton *b  = [self.toggleButtons objectAtIndex:selectedValue];
    if(selectedValue != self.selectedIndex)
    {
        UIButton *goButton = [self.toggleButtons objectAtIndex:self.selectedIndex];
        goButton.selected = NO;
    
        b.selected = YES;
        self.selectedIndex = b.tag;
        //do delegation to let the controller know that the index of the tab changed.
        //[self.delegate tabWasChanged:b.tag];
    }
    
}

-(UIImage *)imageWithColor:(UIColor *)color {
    //makes an image out of a UI color
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
