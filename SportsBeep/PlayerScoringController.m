//
//  PlayerScoringController.m
//  SportsBeep
//
//  Created by Daniel Nasello on 6/1/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "PlayerScoringController.h"

@interface PlayerScoringController (){
    
}

@end

@implementation PlayerScoringController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self geyPlayerGameStats];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addTopView:(teamRectangleView*)tr
{
    
    [self.view addSubview:tr];
}


-(void)addPlayerGameStats:(NSDictionary *)dict
{
    
    NSLog(@"%@",dict);
    
}


-(void)geyPlayerGameStats
{
    NSString *url =[NSString stringWithFormat:@"http://sportsbeep.com/api/mlb/tickets/%@/players/%@/scoresheet",@"1",@"1"];

    NSDictionary *dict =@{
                      //  @"status":[NSString stringWithFormat:self.gameTimeType],
                      @"status":@"upcoming",
                      };



    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];


    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:@"Token token=kxPFsRnfuCfqWUx3aAXK" forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer = requestSerializer;

    [manager GET:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
    NSLog(@"JSON: %@", responseObject);
        
    
    
    
    
    
    
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    
        NSString *errorMessage = operation.responseObject[@"error"];
    
        NSLog(@"err: %@", errorMessage);
    
    }];
}


@end
