//
//  QuizViewController.h
//  SportsBeep
//
//  Created by iOSDev on 5/26/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KAProgressLabel/KAProgressLabel.h>

@interface QuizViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *headLabel;
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (strong, nonatomic) UILabel *timerLabel;
@property (strong, nonatomic) KAProgressLabel *progressLabel;

@property (strong, nonatomic) NSTimer *timer;


- (void) initializeScene;
@end
