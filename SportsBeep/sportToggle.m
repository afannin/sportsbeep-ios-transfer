//
//  sportToggle.m
//  SportsBeep
//
//  Created by iOSDev on 5/21/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "Colors.h"
#import "sportToggle.h"

@implementation sportToggle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.selectedIndex = 100;
        [self createTabs];
    }
    return self;
}



-(void)createTabs
{
    
    self.sports = [[NSArray alloc]init];
    self.sports = @[@"BASEBALL",@"FOOTBALL",@"BASKETBALL",@"HOCKEY"];
    
    NSArray *imageNames = [[NSArray alloc]init];
    imageNames = @[@"ic_baseball",@"ic_football",@"ic_basketball",@"ic_hockey"];
    
    
    NSArray *colors = [[NSArray alloc]init];
    colors = @[RED,GREEN];
    self.tabArray = [[NSMutableArray alloc]init];
    int i = 0;
    for(NSString *sport in self.sports)
    {
        
        sportTab *tab = [[sportTab alloc]initWithFrame:CGRectMake(i * (self.frame.size.width/self.sports.count), 0,(self.frame.size.width/self.sports.count)-1,self.frame.size.height + 20 )];
        
        [tab setBackgroundColor: BLUE_GRAY];
        tab.tag = i;
        
        [self addSubview:tab];
        
        
        [self.tabArray addObject:tab];
        
        [tab.sportName setText:sport];
        
        
        [tab.iconView setImage:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
        
    
        if(i > 1)
        {
            [tab setFakeDisabled];
            
      
        }
        else
        {
            
            [tab.topSpacerView setBackgroundColor:[colors objectAtIndex:i]];
            UITapGestureRecognizer *singleFingerTap =
            [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(handleSingleTap:)];
            [tab addGestureRecognizer:singleFingerTap];
            
        }
        
        
        i++;
    }
    
}




-(void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    
    sportTab *t = (sportTab *)recognizer.view;
    
    int tag = (int)t.tag;
    
    if(self.selectedIndex != tag)
    {
    
        if(self.selectedIndex != 100)
        {
            sportTab *oldTab = [self.tabArray objectAtIndex:self.selectedIndex];
            [oldTab setFakeUnselected];
        }
        
        self.selectedIndex = tag;
        [t setFakeSelected];
        [self.delegate sportWasSelected];
        
        
        
    }
    
    
    
}


-(NSString *)getSelectedSports
{

    
    return [self.sports objectAtIndex:self.selectedIndex];
}


@end
