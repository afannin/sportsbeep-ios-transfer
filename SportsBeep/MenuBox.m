//
//  MenuBox.m
//  SportsBeep
//
//  Created by iOSDev on 5/29/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "MenuBox.h"
#import "Colors.h"


@implementation MenuBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.buttonArray = [[NSMutableArray alloc]init];
        
        UIColor *co = [[UIColor alloc]initWithCGColor:[UIColor colorWithRed:0.302 green:0.361 blue:0.49 alpha:1].CGColor];
        
        //if the toggle is open
        self.isOpen = NO;
        self.backgroundColor =  [UIColor colorWithRed:0.18 green:0.369 blue:0.651 alpha:1] ;
        self.highlightedColor =co;
        self.disabledColor = [UIColor darkGrayColor];
        self.selectedColor = [UIColor purpleColor];
        self.canClick = YES;
        
        self.originalFrame = frame;
        
    }
    return self;
}


-(void)addButtons
{
    //I know how many buttons there are based on the number of labels.
    //now i can split the size equally in the view
    
    
    //width of the button
    CGFloat lWidthOfButton = self.frame.size.width;
    //height of the button should be size of frame... so if you want button height to change, change frame hieght
    CGFloat lHeightButton = 60.0f;
    //set tags + offset for button position
    int i = 0;
    UIColor *co = [[UIColor alloc]initWithCGColor:[UIColor colorWithRed:0.302 green:0.361 blue:0.49 alpha:1].CGColor];
    //add the back button
    self.mdragButton= [DragButton buttonWithType:UIButtonTypeCustom];
    [self.mdragButton setFrame:CGRectMake(0, 0, lWidthOfButton, lHeightButton)];
    [self.mdragButton setBackgroundImage:[self imageWithColor: [UIColor colorWithRed:0.525 green:0.671 blue:0.4 alpha:1]] forState:UIControlStateNormal];
    [self.mdragButton setBackgroundImage:[self imageWithColor:co] forState:UIControlStateHighlighted];
    [self.mdragButton setBackgroundImage:[self imageWithColor:self.selectedColor] forState:UIControlStateSelected];
    [self.mdragButton setBackgroundImage:[self imageWithColor:self.disabledColor] forState:UIControlStateDisabled];
    [self.mdragButton setTitle:@"Menu" forState:UIControlStateNormal];
    [self.mdragButton setTag:i];
    [self.mdragButton setImage:[UIImage imageNamed:@"pi"] forState:UIControlStateNormal];
    [self.mdragButton addTarget:self action:@selector(cancelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonArray addObject:self.mdragButton];
    [self.mdragButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [self.mdragButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 5.0f, 0.0f, 0.0f)];
    [self.mdragButton setImageEdgeInsets:UIEdgeInsetsMake(0.0f, 20.0f, 0.0f, 0.0f)];
    [self.mdragButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.mdragButton setDelegate:self];
    [self addSubview:self.mdragButton];
    [self.mdragButton addRecognizer];
    
    //add the rest of the buttons
    
    NSArray *icons = [[NSArray alloc]init];
    icons = @[
              @"pickg",
              @"norm",
              @"pc",
              @"pi",
              @"htp",
              @"pi",
              @"setb",
         
              
              ];
    for(NSString *string in self.buttonTitles)
    {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(0, lHeightButton * (i+1), lWidthOfButton, lHeightButton)];
        [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
       
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, -10.0f, 0.0f, 0.0f)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
        [button.imageView setContentMode:UIViewContentModeScaleAspectFit];
        [button.imageView setFrame:CGRectMake(button.imageView.frame.origin.x, button.imageView.frame.origin.y, 20, self.frame.size.height)];
        [button setBackgroundImage:[self imageWithColor:PANEL_BG_BLUE] forState:UIControlStateNormal];
        [button setBackgroundImage:[self imageWithColor:co] forState:UIControlStateHighlighted];
        [button setBackgroundImage:[self imageWithColor:self.selectedColor] forState:UIControlStateSelected];
        [button setBackgroundImage:[self imageWithColor:self.disabledColor] forState:UIControlStateDisabled];
        [button setTitle:string forState:UIControlStateNormal];
        [button setTag:i+1];
        [button setImage:[UIImage imageNamed:[icons objectAtIndex:i]] forState:UIControlStateNormal];
        [button.imageView setBackgroundColor:[UIColor redColor]];
    
        [button addTarget:self action:@selector(actionButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.buttonArray addObject:button];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, button.frame.size.width, 1)];
        lineView.backgroundColor = [UIColor blackColor];
        [button addSubview:lineView];
        [self addSubview:button];
        
        i++;
        
        
    }
    
    
    
    self.upOffset = ([self.buttonArray count]-1) * lHeightButton;
    
    NSLog(@"%ld",(long)self.upOffset);
    
}


//if the drawer is open close it if not open it
-(void)cancelButtonPressed:(id)sender
{
    if(self.isOpen)
    {
        self.isOpen = NO;
        [self closeDrawer];
    }
    else
    {
        self.isOpen = YES;
        [self openDrawer];
    }
    
}


//open drawer
-(void)openDrawer
{
    [self.delegate drawerWillOpen];
    [UIView animateWithDuration:.51 delay:0
     
                        options:0 animations:^{
                            
                            [self setFrame:CGRectMake(0, self.originalFrame.origin.y- self.upOffset, self.originalFrame.size.width, self.originalFrame.size.height)];
                        } completion:^(BOOL finished) {
                            
                            [self.delegate drawerDidOpen];
                            
                        }];
    
}



//close drawer
-(void)closeDrawer
{
    [self.delegate drawerWillClose];
    [UIView animateWithDuration:0.51 delay:0
 
                        options:0 animations:^{
                            
                            [self setFrame:self.originalFrame];
                        } completion:^(BOOL finished) {
                            
                            [self.delegate drawerDidClose];
                            self.canClick = YES;
                            
                        }];
    
}




//no springing. used for when we drag
-(void)openDrawerAlternate:(CGPoint)point
{
    
    //the only thing to add to this is MATH to speed up or slow down the animation depeding on how far you have to go. if you have to go half the drawer size
    //animation should be a bit longer than if it is almost all the way up
    
    NSLog(@"%f",self.originalFrame.origin.y - point.y);
    NSLog(@"%f",point.y - self.originalFrame.origin.y);
    //open drawer manually.
    self.isOpen = YES;
    [self.delegate drawerWillOpen];
    [UIView animateWithDuration:.5 delay:0
                        options:0 animations:^{
                            
                            [self setFrame:CGRectMake(0,self.originalFrame.origin.y- self.upOffset, self.originalFrame.size.width, self.originalFrame.size.height)];
                        } completion:^(BOOL finished) {
                            
                            [self.delegate drawerDidOpen];
                            
                        }];
    
}



//close drawer//no springing. used for when we drag
-(void)closeDrawerAlternate:(CGPoint)point
{
    
    //the only thing to add to this is MATH to speed up or slow down the animation depeding on how far you have to go. if you have to go half the drawer size
    //animation should be a bit longer than if it is almost all the way down
    
    NSLog(@"%f",self.originalFrame.origin.y - point.y);
    
    
    //close drawer manually
    self.isOpen = NO;
    
    [self.delegate drawerWillClose];
    [UIView animateWithDuration:.5 delay:0
                        options:0 animations:^{
                            
                            [self setFrame:self.originalFrame];
                        } completion:^(BOOL finished) {
                            
                            [self.delegate drawerDidClose];
                            self.canClick = YES;
                            
                        }];
    
}


/// when one of the regular buttons are pressed
-(void)actionButtonPressed:(id)sender
{
    UIButton *b = sender;
    
    if(self.canClick)
    {
        self.canClick = NO;
        [self.delegate slideUpMenuButtonPressed:b];
        [self closeDrawer];
        
    }
}



//image from a color
-(UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


//pull button delegate
-(void)gestureHasMoved:(CGPoint)point
{
    //the offset between the current frame and the point of dragging
    CGFloat pointOffset = (self.currentFrame.origin.y-25) + point.y;
    //dont let the frame go below the max or above the min
    if(pointOffset >= self.originalFrame.origin.y)
    {
        pointOffset = self.originalFrame.origin.y;
    }
    
    if(pointOffset<=self.upOffset -75)
    {
        pointOffset = self.upOffset-80;
    }
    
    //change the frame
    self.offsetFrame = CGRectMake(0,pointOffset, self.currentFrame.size.width, self.currentFrame.size.height);
    //set the frame
    [self setFrame:self.offsetFrame];
    
}
-(void)gestureHasEnded:(CGPoint)point
{
    
    //the gesuter has ended (delegate from the drag buggon
    
    
    //originating frame is the frame from where we had set it last from last resting point
    CGFloat originator = self.currentFrame.origin.y;
    //ending is where the drag has taken the frame
    CGFloat ending = self.offsetFrame.origin.y;
    self.canClick = NO;
    
    //if the drawer is open
    if(self.isOpen)
    {
        //if the difference between resting and ending is -50px then close it if not, bring it back up
        CGFloat diff = originator - ending;
        
        if(diff < -51)
        {
            [self closeDrawerAlternate:point];
        }
        else
        {
            [self openDrawerAlternate:point];
        }
        
    }
    else
    {
        //difference between drag and last resting
        CGFloat diff = originator - ending;
        //if the difference between the two is at least sixty open it if not re close
        if(diff > 60)
        {
            [self openDrawerAlternate:point];
        }
        else
        {
            [self closeDrawerAlternate:point];
        }
        
    }
    
    
}
-(void)gesturHasStarted:(CGPoint)point
{
    //the gesture has started so lets get the current resting state and
    //tell the vc that the drawer is about to be dragged
    [self.delegate DrawerWillDrag];
    self.currentFrame = self.frame;
    
    
}



@end
