//
//  ScoringBreakdownController.h
//  SportsBeep
//
//  Created by Daniel Nasello on 6/1/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//
#import "AFNetworking.h"
#import <UIKit/UIKit.h>
#import "contestPlayerView.h"
#import "individualGame.h"
#import "teamRectangleView.h"
#import "CSLinearLayoutView.h"
#import "scoreBreakdownPlayerView.h"
@interface ScoringBreakdownController : UIViewController
@property(nonatomic,strong)contestPlayerView *pv;
-(void)getContestStats:(contestPlayerView *)pv:(individualGame*)g:(NSString *)rid;
@property(nonatomic,strong)individualGame *game;
-(void)addGameTeam;
- (IBAction)doneButtonPressed:(id)sender;
@property(nonatomic,strong)CSLinearLayoutView *linearLayout;
@end
