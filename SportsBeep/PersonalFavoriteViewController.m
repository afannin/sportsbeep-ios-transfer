//
//  PersonalFavoriteViewController.m
//  Pods
//
//  Created by iOSDev on 3/26/15.
//
//

#import "PersonalFavoriteViewController.h"
#import "Colors.h"


@interface PersonalFavoriteViewController (){
   
}

@end

@implementation PersonalFavoriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // background
    //UIImage *bgImage = [UIImage imageNamed:@"baseball.png"];
    //[self.mainBG setImage: bgImage];
   // self.mainBG.contentMode = UIViewContentModeScaleAspectFill;
   // self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"baseball.png"]];
    
    // blue container box
    self.blueBgBox.backgroundColor = PANEL_BG_BLUE;
    
    // favorite baseball team field
    self.personalFavoriteField.backgroundColor = BLUE_GRAY;
    self.personalFavoriteField.rightViewMode = UITextFieldViewModeAlways;
    self.personalFavoriteField.layer.borderWidth = 1.0f;
    self.personalFavoriteField.layer.borderColor = [[UIColor blackColor] CGColor];
    self.personalFavoriteField.textColor = [UIColor whiteColor];
    self.personalFavoriteField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Favorite Baseball Team" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];

    
    // enter button
    self.setPersonalFavoritesButton.backgroundColor = RED;
    
    
  
    
    
    
    //create the toggle to pick the sport
    
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.layer.borderColor=[[UIColor cyanColor] CGColor];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    textField.layer.borderColor=[[UIColor blackColor] CGColor];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)setPersonalFavoritesButton:(id)sender {
    // TODO - set personal favorites
    
    UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@"FAVORITE TEAM"
                                                       message:@"Your favorite team has been set!"
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
    [theAlert show];
    
}
@end
