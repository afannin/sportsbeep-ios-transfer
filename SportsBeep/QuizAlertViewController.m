//
//  QuizViewController.m
//  SportsBeep
//
//  Created by iOSDev on 5/25/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "QuizAlertViewController.h"
#import "Colors.h"
#import "Timer.h"

@interface QuizAlertViewController (){
    Timer *timer;
}

@end

@implementation QuizAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initializeAlertView];
    
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initializeAlertView {
    
    //set background image
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"baseball.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:image]];
    
    //initialize alert text label
    self.textLabel.numberOfLines = 0;
    self.textLabel.text = @"ANSWER QUESTIONS WITHIN 14 SECONDS. THE FASTER YOU ANSWER THE MORE POINTS YOU SCORE!";
    [self.textLabel setTextAlignment: NSTextAlignmentCenter];
    self.textLabel.layer.shadowRadius = 3.0;
    self.textLabel.layer.shadowOpacity = 0.5;
    self.textLabel.layer.masksToBounds = NO;
    
    //initialize countdown label
    self.colorLabel.clipsToBounds = YES;
    self.colorLabel.layer.cornerRadius = 5;
    [self.colorLabel setTextAlignment: NSTextAlignmentCenter];
    self.colorLabel.textColor = [UIColor whiteColor];
    [[self colorLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:26]];
    
    //initialize and start timer
    timer = [[Timer alloc] init];
    timer.countdownAmount = 4;
    timer.delegate = self;
    [timer startTimer];
}

- (void) initializeQuiztView {

}


-(void)prettyTimerHasEnded
{
    //countdown has ended present quiz
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"Quiz"];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    //[self.modalViewController dismissViewControllerAnimated:NO completion:nil];
    
    [self presentViewController:vc animated:YES completion:nil];
    //[self presentViewController:vc animated:YES completion:NULL];
    
    

}

-(void)ptimerHasTicked:(NSString*)timeLeft;
{
    // timer has ticked change label color and text
    self.colorLabel.text = timeLeft;
    
    NSArray *locMessageArray = @[@"DONE",@"GO!",@"SET",@"READY"];
    
    NSInteger locTime = [timeLeft integerValue];
    
    switch (locTime) {
        case 3:
            self.colorLabel.backgroundColor = RED;
            self.colorLabel.text = locMessageArray[locTime];
            break;
            
        case 2:
            self.colorLabel.backgroundColor = ORANGE;
            self.colorLabel.text = locMessageArray[locTime];
            break;
            
        case 1:
            self.colorLabel.backgroundColor = RED;
            self.colorLabel.text = locMessageArray[locTime];
            break;
            
        case 0:
            break;
            
        default:
            break;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
