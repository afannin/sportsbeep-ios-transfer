//
//  BlueDotToggle.m
//  SportsBeep
//
//  Created by iOSDev on 3/26/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "blueDotToggle.h"

@implementation BlueDotToggle


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self createTabs];
    }
    return self;
}


-(void)createTabs
{
    self.toggleButtons = [[NSMutableArray alloc]init];
    self.currentSelected = 1;
    
    UIImage *pressed = [self imageWithColor:[UIColor colorWithRed:0.188 green:0.678 blue:0.388 alpha:1] /*#30ad63*/];
    UIImage *pressedinter = [self imageWithColor:[UIColor colorWithRed:0.188 green:0.678 blue:0.388 alpha:1] /*#30ad63*/];
    UIImage *notpressed = [self imageWithColor:[UIColor colorWithRed:0.224 green:0.792 blue:0.455 alpha:1] /*#39ca74*/];
    
    NSArray *tabWords = [[NSArray alloc]init];
    self.dotArray = [[NSMutableArray alloc]init];
    
  
    
    tabWords = @[@"Off",@"On"];
    int i = 0;
    for(NSString *label in tabWords)
    {
        
        
        UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
        b.frame = CGRectMake((self.frame.size.width/2) * i, 0, self.frame.size.width/2, self.frame.size.height);
        // [b setTitle:label forState:UIControlStateNormal];
        b.titleLabel.font = [UIFont systemFontOfSize:15];
        b.clipsToBounds = YES;
        b.tag = i;
        b.layer.cornerRadius = 0.0f;
        [b setAttributedTitle:[[NSAttributedString alloc] initWithString:[tabWords objectAtIndex:i] attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}]  forState:UIControlStateSelected];
        [b setAttributedTitle:[[NSAttributedString alloc] initWithString:[tabWords objectAtIndex:i] attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}]  forState:UIControlStateHighlighted];
        [b setAttributedTitle:[[NSAttributedString alloc] initWithString:[tabWords objectAtIndex:i] attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}]  forState:UIControlStateNormal];
        [b addTarget:self action:@selector(toogleTheButton:) forControlEvents:UIControlEventTouchUpInside];
        [b.titleLabel setFont:[UIFont fontWithName:@"GillSans" size:24.0]];
        // [b setBackgroundImage:notpressed forState:UIControlStateNormal];
        //[b setBackgroundImage:pressedinter forState:UIControlStateHighlighted];
        //[b setBackgroundImage:pressed forState:UIControlStateSelected];
        
        
        b.layer.cornerRadius = 0.0f;
        
        [self addSubview:b];
        [self.toggleButtons addObject:b];
        
        i++;
    }
    
    
    for(int i= 0;i<tabWords.count;i++)
    {
        
        NSInteger offset = self.frame.size.width/4 + (i+2);
        
        UIView *v = [[UIView alloc]initWithFrame:CGRectMake((self.frame.size.width/2) * i + (self.frame.size.width/4)-38, -17, 76, 76)];
        [v setBackgroundColor:[UIColor colorWithRed:0.227 green:0.6 blue:0.847 alpha:1] /*#3a99d8*/];
        [v.layer setCornerRadius:38];
        [self addSubview:v];
        [self sendSubviewToBack:v];
        [self.dotArray addObject:v];
        v.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
        NSLog(@"DD");
    }
    
    
    UIButton *initial = [self.toggleButtons objectAtIndex:self.currentSelected];
    initial.selected = YES;
    [self toggleBlue:self.currentSelected :self.currentSelected:NO];
    
}

-(UIImage *)imageWithColor:(UIColor *)color {
    //makes an image out of a UI color
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}



-(void)toggleBlue :(NSInteger)value :(NSInteger)old:(BOOL)animated
{
    NSLog(@"fires");
    UIView *b = [self.dotArray objectAtIndex:value];
    UIView *oldb = [self.dotArray objectAtIndex:old];
    
    if(animated)
    {
        [UIView animateWithDuration:0.4 delay:0
             usingSpringWithDamping:243 initialSpringVelocity:40.0f
                            options:0 animations:^{
                                
                                oldb.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
                                b.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
                                
                            } completion:^(BOOL finished) {
                                
                            }];
    }
    else
    {
        
        oldb.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
        b.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    }
    
}


-(void)toogleTheButton:(id)sender
{
    UIButton *b = (UIButton*)sender;
    if(b.tag !=self.currentSelected)
    {
        NSInteger old = self.currentSelected;
        UIButton *goButton = [self.toggleButtons objectAtIndex:self.currentSelected];
        goButton.selected = NO;
        b.selected = YES;
        self.currentSelected = (int)b.tag;
        //do delegation to let the controller know that the index of the tab changed.
        [self toggleBlue:b.tag :goButton.tag:YES];
      //  [self.delegate tabWasChanged:b.tag];
    }
}


-(void)toggleMyButton:(NSInteger)selectedValue
{
    UIButton *b  = [self.toggleButtons objectAtIndex:selectedValue];
    if(selectedValue != self.currentSelected)
    {
        NSInteger old = self.currentSelected;
        UIButton *goButton = [self.toggleButtons objectAtIndex:self.currentSelected];
        goButton.selected = NO;
        b.selected = YES;
        
        [self toggleBlue:selectedValue :goButton.tag:YES];
        self.currentSelected = (int)selectedValue;
    }
    
}





@end


