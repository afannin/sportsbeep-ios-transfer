//
//  specificContestViewController.h
//  SportsBeep
//
//  Created by Daniel Nasello on 5/30/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//
#import <SDWebImage/UIImageView+WebCache.h>
#import <UIKit/UIKit.h>
#import "bigTriangleHolder.h"
#import "AFNetworking.h"
#import "individualGame.h"
#import "contestPlayerView.h"
#import "scoringBreakdownController.h"
@interface specificContestViewController : UIViewController
-(void)addGame:(individualGame *)game;
- (IBAction)doneButtonPressed:(id)sender;
@property(nonatomic,strong)individualGame *myGame;
@end
