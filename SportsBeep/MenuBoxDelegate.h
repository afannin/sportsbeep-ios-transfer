//
//  MenuBoxDelegate.h
//  SportsBeep
//
//  Created by iOSDev on 5/29/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MenuBox;
@protocol MenuBoxDelegate <NSObject>


-(void)slideUpMenuButtonPressed:(UIButton*)button;

-(void)cancelButtonWasPressed;

-(void)drawerWillClose;
-(void)drawerDidClose;
-(void)drawerWillOpen;
-(void)drawerDidOpen;
-(void)DrawerWillDrag;
@end
