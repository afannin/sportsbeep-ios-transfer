//
//  PickGameViewController.m
//  SportsBeep
//
//  Created by iOSDev on 3/27/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "PickGameViewController.h"
#import "Colors.h"
#import "DarkCover.h"
#import "MyContestsViewController.h"
@interface PickGameViewController ()
@property DarkCover *cover;
@end

@implementation PickGameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //cover
    CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    self.cover = [[DarkCover alloc] initWithFrame:frame];
    self.cover.hidden = YES;
    [self.view addSubview:self.cover];
    
    // view
    self.view.backgroundColor = PANEL_BG_BLUE;
    
    // labels
    self.pickGameLabel.backgroundColor = DARK_BLUE;
    
    //self.gamesCollectionVeiw.backgroundColor = [UIColor clearColor];
    self.gamesCollectionVeiw.backgroundColor = DARK_BLUE;
    
    NSArray *titles = [[NSArray alloc]init];
    titles = @[@"PICK A GAME",@"MY CONTESTS",@"CHANGE SPORT",@"PROFILE",@"HOW TO PLAY",@"SHARE",@"LOGOUT"];
    
    
    CGFloat btnHeight = 60;
    self.mSlideUpMenu = [[MenuBox alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - btnHeight, self.view.frame.size.width,btnHeight * ([titles count] + 1))];
    
    [self.mSlideUpMenu setDelegate:self];
    [self.view setClipsToBounds:YES];
    
    [self.view addSubview:self.mSlideUpMenu];
    
    [self.mSlideUpMenu setBackgroundColor: [UIColor colorWithRed:0.18 green:0.369 blue:0.651 alpha:1]];
    
    
    self.mSlideUpMenu.buttonTitles = titles;
    [self.mSlideUpMenu addButtons];
    
}



-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return 20;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"containerCell" forIndexPath:indexPath];
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat cellWidth = (screenWidth - 8)/2;
    return CGSizeMake(cellWidth, 100);
}


//delegate methods slide up menu
-(void)slideUpMenuButtonPressed:(UIButton*)button
{
    NSInteger tag = button.tag;
    //from here you can do some actions based on the button pressed
    switch (tag) {
        case 1:
            NSLog(@"Pick a Game");
            break;
        case 2:
            [self goToMyContests];
            break;
        case 3:
            NSLog(@"Change Sport");
            break;
        case 4:
            NSLog(@"Profile");
            break;
        case 5:
            NSLog(@"How To Play");
            break;
        case 6:
            NSLog(@"Share");
            break;
        case 7:
            NSLog(@"LOGOUT");
            break;
            
        default:
            break;
    }
}





-(void)goToMyContests
{
    
    //myContestsController
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    MyContestsViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"myContestsController"];
    [self presentViewController:vc animated:YES completion:nil];
    
}

//cancel was pressed
-(void)cancelButtonWasPressed
{
    
}
//will close
-(void)drawerWillClose
{
    self.cover.hidden = YES;
}
//did close
-(void)drawerDidClose
{
    self.cover.hidden = YES;
}
//will open
-(void)drawerWillOpen
{
    self.cover.hidden = NO;
}
//opened
-(void)drawerDidOpen
{
    self.cover.hidden = NO;
}
//drawer dragging add the barrier view
-(void)DrawerWillDrag
{
    
    
}

//end slide up menu delegate

@end
