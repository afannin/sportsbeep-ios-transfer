//
//  DarkCover.m
//  SportsBeep
//
//  Created by iOSDev on 5/29/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "DarkCover.h"

@implementation DarkCover

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor blackColor];
        self.alpha = 0.5;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
