//
//  PickGameViewController.h
//  SportsBeep
//
//  Created by iOSDev on 3/27/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSCollectionView.h"
#import "MenuBox.h"
@interface PickGameViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *gamesCollectionVeiw;
@property (weak, nonatomic) IBOutlet UILabel *pickGameLabel;
@property(nonatomic,strong)MenuBox *mSlideUpMenu;
@end
