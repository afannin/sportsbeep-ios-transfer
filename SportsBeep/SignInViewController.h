//
//  ViewController.h
//  SportsBeep
//
//  Created by iOSDev on 3/24/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHud.h"

@interface SignInViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *mainBG;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIView *blueBgBox;
- (IBAction)signInButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UIButton *createAccountButton;
- (IBAction)forgotPassword:(id)sender;
-(void)loginManagerLogin;
-(BOOL) formIsValid;
-(void)forgotPasswordSuccess;
-(void)forgotPasswordError: (NSString*) errorMessage;
-(void)toastMessage:(NSString *)message;
@end

