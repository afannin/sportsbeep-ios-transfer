//
//  QuizViewController.m
//  SportsBeep
//
//  Created by iOSDev on 5/26/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "QuizViewController.h"
#import "KAProgressLabel.h"
#import "Colors.h"

@interface QuizViewController ()
@property CGFloat timerDuration;
@end

@implementation QuizViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initializeScene];
    [self startTimer];
    
}

- (void) startTimer {
    self.timerDuration = 14.0;
    self.timerLabel.text = [NSString stringWithFormat: @"%f", self.timerDuration];
    self.timer = [NSTimer scheduledTimerWithTimeInterval: 0.1
                                                  target: self
                                                selector:@selector(countDown)
                                                userInfo: nil repeats:YES];
}

- (void) countDown {
    
    self.timerDuration -= 0.1;
    self.timerLabel.text = [NSString stringWithFormat: @"%.1f", self.timerDuration];
    
    if (self.timerDuration <= 0) {
        [self.timer invalidate];
        self.timerLabel.text = @"done";
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initializeScene {
    
    self.headLabel.backgroundColor = RED;
    //self.headLabel.frame.size.width = self.view.frame.size.width;
    self.questionLabel.backgroundColor = SKY_BLUE;
    
    self.progressLabel = [[KAProgressLabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width - 65, 8, 50, 50)];
    [self.progressLabel setProgress:1.0 timing:TPPropertyAnimationTimingEaseOut duration:15.0 delay:0];
    [self.progressLabel setProgressColor:RED];
    [self.progressLabel setTrackColor: [UIColor lightGrayColor]];
    
    self.timerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    self.timerLabel.textColor = [UIColor whiteColor];
    self.timerLabel.textAlignment = NSTextAlignmentCenter;
    [[self timerLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
    
    // add subviews
    [self.headLabel addSubview:self.progressLabel];
    [self.progressLabel addSubview:self.timerLabel];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
