//
//  scoreBreakdownPlayerView.h
//  SportsBeep
//
//  Created by Daniel Nasello on 6/3/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+Expanded.h"
@interface scoreBreakdownPlayerView : UIView
@property(nonatomic,strong)UILabel *playerNumber;
@property(nonatomic,strong)UILabel *playerTeamAbbr;
@property(nonatomic,strong)UILabel *playerName;
@property(nonatomic,strong)UILabel *playerStats;
@property(nonatomic,strong)UILabel *playerPosition;
@property(nonatomic,strong)UILabel *playerPoints;
@property(nonatomic,strong)UILabel *playerIngame;
-(void)setLabelColors:(NSString *)color;
-(void)setBG:(NSString *)color;
-(void)setStats:(NSArray *)stats;
@end
