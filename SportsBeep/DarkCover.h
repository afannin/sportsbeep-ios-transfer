//
//  DarkCover.h
//  SportsBeep
//
//  Created by iOSDev on 5/29/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DarkCover : UIView
- (id)initWithFrame:(CGRect)frame;
@end
