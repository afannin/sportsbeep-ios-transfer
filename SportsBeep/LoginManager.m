//
//  LoginManager.m
//  h6
//
//  Created by Daniel Nasello on 10/9/14.
//  Copyright (c) 2014 Combustion Innovation Group. All rights reserved.
//

#import "LoginManager.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "SocialLoginDelegate.h"
#import "User.h"
#import "Colors.h"

@interface LoginManager () {
    LoginManager *lim;
}
@end

@implementation LoginManager

-(void)login :(NSString *)username withPassword:(NSString *)password {
    
    NSString *url = @"http://sportsbeep.com/api/users/login";
    
    NSDictionary *dict =@{
                          @"email":[NSString stringWithFormat:@"%@",username],
                          @"password":[NSString stringWithFormat:@"%@",password],
                          };
    
    NSDictionary *params = @{
                             @"api_user":dict,
                             };
  
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

 
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    manager.requestSerializer = requestSerializer;
    
    [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
       
        User *user = [[User alloc] init];
        
        user.authenticationToken = responseObject[@"authentication_token"];
        //user.avatarUrl = responseObject[@"avatar_url"];
        user.createdAt = responseObject[@"created_at"];
        user.email = responseObject[@"email"];
        //user.firstName = responseObject[@"first_name"];
        user.userId = responseObject[@"id"];
        //user.lastName = responseObject[@"last_name"];
        user.updatedAt = responseObject[@"updated_at"];
        user.userName = responseObject[@"username"];
        
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setObject:user.authenticationToken forKey:@"authentication_token"];
        //[defaults setObject:user.avatarUrl forKey:@"avatar_url"];
        [defaults setObject:user.createdAt forKey:@"created_at"];
        [defaults setObject:user.email forKey:@"email"];
        [defaults setObject:password forKey:@"password"];
        //[defaults setObject:user.email forKey:@"first_name"];
        [defaults setObject:user.userId forKey:@"id"];
        //[defaults setObject:user.lastName forKey:@"last_name"];
        [defaults setObject:user.updatedAt forKey:@"updated_at"];
        [defaults setObject:user.userName forKey:@"username"];
     
        
        [defaults synchronize];
        

            
        [self.delegate PushUpdatedLogin];
   
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorMessage = operation.responseObject[@"error"];
        [self.delegate PushLoginError: errorMessage];
    }];
    
}

-(void)logout {
    
    // create the parameters to be sent to the API
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults stringForKey:@"authentication_token"];
    
    NSString *tokenString = [NSString stringWithFormat:@"Token token=%@", token];
    
    NSString *url = @"http://sportsbeep.com/api/users/logout/";

   
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    manager.requestSerializer = requestSerializer;
    
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:tokenString forHTTPHeaderField:@"Authorization"];
    
    [manager DELETE:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"in logout success");
        
        NSLog(@"JSON: %@", responseObject);
        
        // clear defaults
        [[NSUserDefaults standardUserDefaults]
        removePersistentDomainForName:@"com.SportsBeep.SportsBeep"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        // present login screen
        [self.delegate PushUpdatedLogout];
    
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"in logout error");
        NSLog(@"JSON: %@", error);
 
    }];
    
    
    

}

-(void)createAccount :(NSString *)username withEmail:(NSString *)email withPassword:(NSString *)password {
    
    NSLog(@"In Create Account");
    
    NSString *url = @"http://sportsbeep.com/api/users";
    
    NSDictionary *dict =@{
                          @"username":[NSString stringWithFormat:@"%@",username],
                          @"email":[NSString stringWithFormat:@"%@",email],
                          @"password":[NSString stringWithFormat:@"%@",password],
                          
                          };
    
    NSDictionary *params = @{
                             @"api_user":dict,
                             };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    manager.requestSerializer = requestSerializer;
    
    [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        
        User *user = [[User alloc] init];
        
        //user.authenticationToken = responseObject[@"authentication_token"];

        //user.avatarUrl = responseObject[@"avatar_url"];
        user.createdAt = responseObject[@"created_at"];
        user.email = responseObject[@"email"];
        user.firstName = responseObject[@"first_name"];
        user.userId = responseObject[@"id"];
        user.lastName = responseObject[@"last_name"];
        user.updatedAt = responseObject[@"updated_at"];
        user.userName = responseObject[@"username"];
        
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setObject:user.authenticationToken forKey:@"authentication_token"];
        
        
        //NSLog(user.authenticationToken);
        //[defaults setObject:user.avatarUrl forKey:@"avatar_url"];
        //[defaults setObject:user.createdAt forKey:@"created_at"];
        //[defaults setObject:user.email forKey:@"email"];
        //[defaults setObject:user.email forKey:@"first_name"];
        //[defaults setObject:user.userId forKey:@"id"];
        //[defaults setObject:user.lastName forKey:@"last_name"];
        //[defaults setObject:user.updatedAt forKey:@"updated_at"];
        //[defaults setObject:user.userName forKey:@"username"];
        
        
        [defaults synchronize];
        
        
        
        [self.delegate PushCreatedAccount];
        
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorMessage = operation.responseObject[@"error"];
        [self.delegate PushLoginError: errorMessage];
    }];
    
    
}

-(void)forgotPassword :(NSString *)username
{
    
    NSLog(@"In Forgot Password");
    
    NSString *url = @"http://sportsbeep.com/api/users/password";
    
    NSDictionary *dict =@{
                          @"email":[NSString stringWithFormat:@"%@",username],
                          };
    
    NSDictionary *params = @{
                             @"api_user":dict,
                             };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    manager.requestSerializer = requestSerializer;
    
    [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Forgot password JSON: %@", responseObject);
        [self.delegate forgotPasswordSuccess];
  
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorMessage = operation.responseObject[@"error"];
        [self.delegate forgotPasswordError: errorMessage];
        NSLog(@"Error forgot password JSON: %@", errorMessage);
    }]; 
    
}

-(void)addFavoriteTeam :(NSString *)username
{
    
    NSLog(@"In Forgot Password");
    
    NSString *url = @"http://sportsbeep.com/api/users/password";
    
    NSDictionary *dict =@{
                          @"email":[NSString stringWithFormat:@"%@",username],
                          };
    
    NSDictionary *params = @{
                             @"api_user":dict,
                             };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    manager.requestSerializer = requestSerializer;
    
    [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Forgot password JSON: %@", responseObject);
        [self.delegate forgotPasswordSuccess];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorMessage = operation.responseObject[@"error"];
        [self.delegate formError: errorMessage];
        NSLog(@"Error forgot password JSON: %@", errorMessage);
    }];
    
}

-(void)formError: (NSString*) errorMessage {
    
    // Login Manager variable
    lim = nil;
    
    // login error alert
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"RESET PASSWORD FAILED" message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

-(NSString *)isNull:(NSString *)str
{
    
    NSString *ret;
    if(str != nil)
    {
        ret = str;
    }
    else
    {
        ret = @"";
    }
    
    return ret;
    
    
}


@end
