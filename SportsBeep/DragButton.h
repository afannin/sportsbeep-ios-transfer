//
//  DragButton.h
//  SportsBeep
//
//  Created by iOSDev on 5/29/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "dragButtonDelegate.h"

@interface DragButton : UIButton<UIGestureRecognizerDelegate>{
    id<DragButtonDelegate>delegate;
}
@property(nonatomic,strong)id delegate;
//the last location of the object
@property(nonatomic,assign)CGPoint lastLocation;
//adds the recognziers

-(void)addRecognizer;


@end
