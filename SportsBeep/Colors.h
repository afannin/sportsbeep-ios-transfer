//
//  Colors.h
//  SportsBeep
//
//  Created by iOSDev on 3/28/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

/*
 
PANEL_BG_BLUE - Blue background of container boxes
 
DARK_BLUE
 
BABY_BLUE
 
BLUE
 
BLUE_GRAY
 
LAVANDER
 
SKY_BLUE
 
YET_ANOTHER_BLUE
 
GRAY
 
RED
 
LIGHT_RED
 
GREEN
 
ORANGE

OPTION_PRESSED_COLOR
 
HINT_COLOR

EDIT_TEXT_BG_DEFAULT_COLOR

*/

#ifndef SportsBeep_Colors_h
#define SportsBeep_Colors_h

#define PANEL_BG_BLUE  [UIColor colorWithRed:0.224 green:0.259 blue:0.388 alpha:1] /*#394263*/
#define HINT_COLOR  [UIColor colorWithRed:0.667 green:0.667 blue:0.667 alpha:1] /*#aaaaaa*/
#define DARK_BLUE  [UIColor colorWithRed:0.106 green:0.122 blue:0.18 alpha:1] /*#1b1f2e*/
#define GREEN  [UIColor colorWithRed:0.525 green:0.671 blue:0.4 alpha:1] /*#86ab66*/
#define OPTION_PRESSED_COLOR  [UIColor colorWithRed:0.314 green:0.349 blue:0.478 alpha:1] /*#50597a*/
#define RED  [UIColor colorWithRed:0.8 green:0.2 blue:0.278 alpha:1] /*#cc3347*/
#define BABY_BLUE  [UIColor colorWithRed:0.369 green:0.765 blue:0.922 alpha:1] /*#5ec3eb*/
#define EDIT_TEXT_BG_DEFAULT_COLOR [UIColor colorWithRed:0.318 green:0.353 blue:0.478 alpha:1] /*#515a7a*/
#define LIGHT_RED [UIColor colorWithRed:0.89 green:0.302 blue:0.412 alpha:1] /*#e34d69*/
#define BLUE [UIColor colorWithRed:0.216 green:0.404 blue:0.671 alpha:1] /*#3767ab*/
#define ORANGE [UIColor colorWithRed:0.922 green:0.635 blue:0.212 alpha:1] /*#eba236*/
#define GRAY [UIColor colorWithRed:0.522 green:0.549 blue:0.549 alpha:1] /*#858c8c*/
#define BLUE_GRAY [UIColor colorWithRed:0.302 green:0.361 blue:0.49 alpha:1] /*#4d5c7d*/
#define LAVANDER [UIColor colorWithRed:0.525 green:0.561 blue:0.678 alpha:1] /*#868fad*/
#define SKY_BLUE [UIColor colorWithRed:0.275 green:0.737 blue:0.949 alpha:1] /*#46bcf2*/
#define YET_ANOTHER_BLUE [UIColor colorWithRed:0.18 green:0.369 blue:0.651 alpha:1] /*#2e5ea6*/

#endif





