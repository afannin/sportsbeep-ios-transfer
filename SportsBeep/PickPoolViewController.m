//
//  PickPoolViewController.m
//  SportsBeep
//
//  Created by iOSDev on 4/3/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "PickPoolViewController.h"
#import "Colors.h"
#import "QuizAlertViewController.h"

@implementation PickPoolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initializeView {
    
    //initialize colors
    self.pickPoolHeadLabel.backgroundColor = RED;
    self.moneyTextLabel.textColor = SKY_BLUE;
    self.pickPoolScrollView.backgroundColor = PANEL_BG_BLUE;
    
    //pool options
    NSArray *poolOptions = [NSArray arrayWithObjects:
                            @"PLAY FOR FREEE",
                            @"$1.10 TO ENTER A $10 POOL",
                            @"$2.20 TO ENTER A $20 POOL",
                            @"$5.50 TO ENTER A $50 POOL",
                            @"$11.00 TO ENTER A $100 POOL",
                            @"$27.50 TO ENTER A $250 POOL",
                            @"$55.00 TO ENTER A $500 POOL",
                            @"$110.00 TO ENTER A $1,0000 POOL",
                            @"$275.00 TO ENTER A $2,500 POOL",
                            @"$550.00 TO ENTER A $5,000 POOL",
                            nil];
    
    CGFloat buttonHeight = 75.0;
    CGFloat numberOfOptions = poolOptions.count;
    
    //set scroll view content size
    CGRect scrollFrame;
    scrollFrame.size = CGSizeMake(self.view.bounds.size.width, buttonHeight * numberOfOptions);
    self.pickPoolScrollView.contentSize = scrollFrame.size;
    
    //add buttons to view
    for (int i = 0; i < numberOfOptions; i++) {
        
        //buttons
        UIButton *poolButton = [UIButton buttonWithType:UIButtonTypeCustom];
        poolButton.frame = CGRectMake(0, buttonHeight * i, self.view.bounds.size.width, buttonHeight);
        [poolButton setTitle:poolOptions[i] forState:UIControlStateNormal];
        [poolButton setBackgroundImage:[self imageFromColor:PANEL_BG_BLUE] forState:UIControlStateNormal];
        [poolButton setBackgroundImage:[self imageFromColor:GRAY] forState:UIControlStateHighlighted];
        [self.pickPoolScrollView addSubview:poolButton];
        poolButton.tag = i;
        [poolButton addTarget:self action:@selector(poolButtonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        //borders
        UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, poolButton.frame.size.height - 1.0f, poolButton.frame.size.width, 1)];
        bottomBorder.backgroundColor = DARK_BLUE;
        
        if (i < numberOfOptions - 1) {
            [poolButton addSubview:bottomBorder];
        }
    }
    
} //end initializeView

-(UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
    
} //ned imageFromColor



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)poolButtonWasPressed:(id)sender
{
    UIButton *b = sender;
    int tag = b.tag;
    
    //present confirmation dialogue box
    [self confirmPool];
    
}

-(void)confirmPool {
    NSLog(@"confirm pool");
    
    
    self.cover = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    self.cover.backgroundColor = [UIColor blackColor];
    self.cover.alpha = 0.5;
    [self.view addSubview:self.cover];
    
    self.poolConfirmBox = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width * 0.85, 200)];
    self.poolConfirmBox.backgroundColor = [UIColor blackColor];
    self.poolConfirmBox.layer.borderColor = [UIColor whiteColor].CGColor;
    self.poolConfirmBox.layer.borderWidth = 2.0;
    self.poolConfirmBox.layer.cornerRadius = 5;
    self.poolConfirmBox.center = self.view.center;
    
    //labels
    NSArray *boxText = [[NSArray alloc] initWithObjects:
                        @"YOU ARE ABOUT TO COMMIT TO THIS CONTEST",
                        @"YOUR ACCOUNT WILL BE CHARGED",
                        @"TIMED QUESTIONS ARE NEXT",
                        @"ARE YOU READY?",
                        nil];
    
    CGFloat labelHeight = 30.0;
    CGFloat padding = labelHeight/3;
    CGFloat yPos = padding;
    
    for(int i = 0; i < boxText.count; i++){
        if (i > 0) {
            yPos = padding + (labelHeight * i);
        }
        UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(0, yPos, self.poolConfirmBox.frame.size.width, labelHeight)];
        lb.text = boxText[i];
        lb.textColor = [UIColor whiteColor];
        lb.textAlignment = NSTextAlignmentCenter;
        if (i == boxText.count - 1) {
            lb.font = [lb.font fontWithSize:20];
        }else {
            lb.font = [lb.font fontWithSize:12];
        }
        
        [self.poolConfirmBox addSubview:lb];
    }
    
    //buttons
    CGFloat btnWidth = self.poolConfirmBox.frame.size.width/2 - 20;
    CGFloat btnHeight = 50;
    CGFloat btnPadding = 10;
    
    
    self.cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    self.cancelBtn.frame = CGRectMake(self.poolConfirmBox.frame.size.width - btnWidth - btnPadding, self.poolConfirmBox.frame.size.height - btnHeight - btnPadding, btnWidth, btnHeight);
    self.cancelBtn.backgroundColor = RED;
    self.cancelBtn.layer.cornerRadius = 5;
    [self.cancelBtn setTitle:@"CANCEL" forState:UIControlStateNormal];
    [self.cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.poolConfirmBox addSubview:self.cancelBtn];
    [self.cancelBtn addTarget:self action:@selector(cancelBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.yesBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    self.yesBtn.frame = CGRectMake(btnPadding, self.poolConfirmBox.frame.size.height - btnHeight - btnPadding, btnWidth, btnHeight);
    self.yesBtn.backgroundColor = RED;
    self.yesBtn.layer.cornerRadius = 5;
    [self.yesBtn setTitle:@"YES" forState:UIControlStateNormal];
    [self.yesBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.yesBtn addTarget:self action:@selector(yesBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.poolConfirmBox addSubview:self.yesBtn];

    
    
    
    
    [self.view addSubview:self.poolConfirmBox];
    
}


-(void)yesBtnPressed:(id)sender
{
    
    
    //present quiz view controller
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"QuizAlert"];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    //[self.modalViewController dismissViewControllerAnimated:NO completion:nil];
    
    [self presentViewController:vc animated:YES completion:nil];
    //[self presentViewController:vc animated:YES completion:NULL];
 
    
    
}

-(void)cancelBtnPressed:(id)sender
{
    [self.cover removeFromSuperview];
    [self.poolConfirmBox removeFromSuperview];
}




@end
