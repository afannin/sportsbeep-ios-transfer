//
//  NewGameViewController.h
//  SportsBeep
//
//  Created by iOSDev on 3/26/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlueDotToggle.h"
#import "sportToggle.h"
//toggle buttons


@interface NewGameViewController : UIViewController<sportToggleDelegate>
@property (weak, nonatomic) IBOutlet UILabel *selectLevelLabel;

@property (weak, nonatomic) IBOutlet UILabel *startGameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *mainBG;
@property (weak, nonatomic) IBOutlet UILabel *selectSportLabel;
@property(nonatomic,strong)BlueDotToggle *toggle;
@property (weak, nonatomic) IBOutlet UIView *blueBgBox;
@property (weak, nonatomic) IBOutlet UIButton *submitNewGameButton;
- (IBAction)submitNewGameButton:(id)sender;



@end
