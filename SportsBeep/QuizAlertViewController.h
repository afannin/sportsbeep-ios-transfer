//
//  QuizViewController.h
//  SportsBeep
//
//  Created by iOSDev on 5/25/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuizAlertViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@property (weak, nonatomic) IBOutlet UILabel *colorLabel;
- (void) initializeAlertView;
@end
