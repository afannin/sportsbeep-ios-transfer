//
//  GameRulesViewController.h
//  SportsBeep
//
//  Created by iOSDev on 3/27/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface GameRulesViewController : UIViewController

@property (nonatomic, weak) id<UIGestureRecognizerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *closeRulesButton;
@property (weak, nonatomic) IBOutlet UIWebView *rulesWebView;

@end
