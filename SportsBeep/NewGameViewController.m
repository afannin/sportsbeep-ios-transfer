//
//  NewGameViewController.m
//  SportsBeep
//
//  Created by iOSDev on 3/26/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "NewGameViewController.h"
#import "Colors.h"
#import "sportLevelToggle.h"

@interface NewGameViewController (){
    sportLevelToggle *levelToggle;
    sportToggle *sToggle;
    BOOL isfirstShowing;
}


@end

@implementation NewGameViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
    
    //set the enter button to not enabled

    
    // background
    UIImage *bgImage = [UIImage imageNamed:@"baseball.png"];
    [self.mainBG setImage: bgImage];
    self.mainBG.contentMode = UIViewContentModeScaleAspectFill;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"baseball.png"]];
    
    // blue container box
    self.blueBgBox.backgroundColor = PANEL_BG_BLUE;
    
    // labels
    self.startGameLabel.backgroundColor = RED;
    self.selectLevelLabel.textColor = RED;
    self.selectSportLabel.textColor = RED;
    
    // enter button
   // self.submitNewGameButton.backgroundColor = HINT_COLOR;
    
    
    [self.blueBgBox setFrame:CGRectMake(20, self.blueBgBox.frame.origin.y, self.view.frame.size.width-40, 120)];
    [self.blueBgBox setClipsToBounds:YES];
    
    //create the toggle to pick the sport level
    levelToggle = [[sportLevelToggle alloc]initWithFrame:CGRectMake(2, 80,self.blueBgBox.frame.size.width+4, 45)];
    [self.blueBgBox addSubview:levelToggle];

    
  
    
    //creates the sports toggle
    sToggle  = [[sportToggle alloc]initWithFrame:CGRectMake(0, 155, self.blueBgBox.frame.size.width+8, 70)];

    [self.blueBgBox addSubview:sToggle];
    sToggle.delegate = self;
    
    
    
    
    [self.submitNewGameButton setBackgroundImage:[self imageWithColor:RED] forState:UIControlStateNormal];
    
    [self.submitNewGameButton setBackgroundImage:[self imageWithColor:GRAY] forState:UIControlStateDisabled];
    [self.submitNewGameButton setEnabled:NO];
    
    self.submitNewGameButton.layer.cornerRadius = 5.0f;
    [self.submitNewGameButton setClipsToBounds:YES];
}


-(void)viewDidAppear:(BOOL)animated
{
    
    if(!isfirstShowing)
    {
    
        
        
        isfirstShowing = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submitNewGame:(id)sender {
}
- (IBAction)submitNewGameButton:(id)sender {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PickGame"];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self.modalViewController dismissViewControllerAnimated:NO completion:nil];
    
    [self presentViewController:vc animated:YES completion:NULL];
    
}



-(void)goToNewGames
{
   // NSString *sport = [sToggle getSelectedSports];

    
}

-(void)sportWasSelected
{
  
    

        [self.submitNewGameButton setEnabled:YES];
    
}


-(UIImage *)imageWithColor:(UIColor *)color {
    //makes an image out of a UI color
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


@end
