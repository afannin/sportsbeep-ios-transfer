//
//  MenuBoxViewController.m
//  SportsBeep
//
//  Created by iOSDev on 3/30/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "MenuBoxViewController.h"
#import "Colors.h"
#import "LoginManager.h"

@interface MenuBoxViewController (){
    LoginManager *lim;
}


@end

@implementation MenuBoxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    // background
    self.view.backgroundColor = [UIColor clearColor];
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.modalPresentationStyle = UIModalPresentationFormSheet;
    
    //menu box button
    self.menuBoxButton.backgroundColor = GREEN;
    
    //pick a game button
    self.pickGameButton.backgroundColor = PANEL_BG_BLUE;
    self.pickGameButton.layer.borderWidth = 1.0f;
    self.pickGameButton.layer.borderColor = [[UIColor blackColor] CGColor];
    self.pickGameButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //TODO - add images to buttons
    
    
    // my contests button
    self.myContestsButton.backgroundColor = PANEL_BG_BLUE;
    self.myContestsButton.layer.borderWidth = 1.0f;
    self.myContestsButton.layer.borderColor = [[UIColor blackColor] CGColor];
    self.myContestsButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    // change sport button
    self.changeSportButton.backgroundColor = PANEL_BG_BLUE;
    self.changeSportButton.layer.borderWidth = 1.0f;
    self.changeSportButton.layer.borderColor = [[UIColor blackColor] CGColor];
    self.changeSportButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    // profile button
    self.profileButton.backgroundColor = PANEL_BG_BLUE;
    self.profileButton.layer.borderWidth = 1.0f;
    self.profileButton.layer.borderColor = [[UIColor blackColor] CGColor];
    self.profileButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
  
    
    // how to play button
    self.howToPlayButton.backgroundColor = PANEL_BG_BLUE;
    self.howToPlayButton.layer.borderWidth = 1.0f;
    self.howToPlayButton.layer.borderColor = [[UIColor blackColor] CGColor];
    self.howToPlayButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    // log out button
    self.logOutButton.backgroundColor = PANEL_BG_BLUE;
    self.logOutButton.layer.borderWidth = 1.0f;
    self.logOutButton.layer.borderColor = [[UIColor blackColor] CGColor];
    self.logOutButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
}

-(void)PushUpdatedLogout
{
    NSLog(@"in push updated logout");
    // Log in Manager Variable
    lim = nil;
    
    // present Log in Controller
//    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SignIn"];
//    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    [self presentViewController:vc animated:YES completion:NULL];
    
    
    [self.view.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)dismissMenuBox:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)logOut:(id)sender {
    
    if(!lim)
    {
        lim = [[LoginManager alloc]init];
        lim.delegate = self;
    }
    [lim logout];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
