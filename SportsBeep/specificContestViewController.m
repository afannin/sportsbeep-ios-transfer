//
//  specificContestViewController.m
//  SportsBeep
//
//  Created by Daniel Nasello on 5/30/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "specificContestViewController.h"
#import "CSLinearLayoutView.h"
@interface specificContestViewController (){
    CSLinearLayoutView *linearLayout;
    NSMutableArray *players;
}

@end

@implementation specificContestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    players = [[NSMutableArray alloc]init];
    
    
    linearLayout = [[CSLinearLayoutView alloc]initWithFrame:CGRectMake(0, 280, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:linearLayout];
    [linearLayout setBackgroundColor:[UIColor greenColor]];
}


-(void)addGame:(individualGame *)game
{
    
    self.myGame = game;
    
    bigTriangleHolder *th =[[bigTriangleHolder alloc]initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, 180)];
    
    [th addGameTriangles:game.awayTeamObject :game.homeTeamObject :self.view.frame.size.width :game.gameTime :game.gameDay];
    
    [self.view addSubview:th];
    
    [self getGamePlayers:game.contestID];
    
  //  NSLog(game.gameId);
    
}

- (IBAction)doneButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)getGamePlayers:(NSString *)gameID
{
    NSString *url =[NSString stringWithFormat:@"http://sportsbeep.com/api/mlb/contests/%@/tickets",gameID];
    
    NSLog(@"game id %@",gameID);
    NSLog(url);
    
    NSDictionary *dict =@{
                          //  @"status":[NSString stringWithFormat:self.gameTimeType],
                          @"status":@"upcoming",
                          };
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:@"Token token=wgGZgmyUws5iGepQqWtB" forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer = requestSerializer;
    
    [manager GET:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        

        NSArray *array = [responseObject objectForKey:@"tickets"];
        
        
        int i = 0;
        
        for(NSDictionary *dict in array)
        {
            
            NSString *place = [dict objectForKey:@"placing"];
            
            NSDictionary *user = [dict objectForKey:@"user"];
        
            NSString *userName = [user objectForKey:@"username"];
            NSString *avatar = [user objectForKey:@"avatar_url"];
              NSString *gid = [dict objectForKey:@"id"];
            
            
        contestPlayerView *pv = [[contestPlayerView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
            pv.playerDict = dict;
            
        UITapGestureRecognizer *playerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playerTapped:)];
        [pv addGestureRecognizer:playerTap];
          
            [pv.icon sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",avatar]]
                             placeholderImage:[UIImage imageNamed:@"player_silhouette"]];
            
            
            CSLinearLayoutItem *item = [CSLinearLayoutItem layoutItemForView:pv];
            [linearLayout addItem:item];

            [players addObject:pv];
            
            pv.restorationIdentifier = gid;
            
            i++;
            
        }
        
        
        
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorMessage = operation.responseObject[@"error"];
        
        NSLog(@"err: %@", errorMessage);
 
    }];

}

//when a player is tapped
-(void)playerTapped:(UITapGestureRecognizer *)r
{
    
    NSInteger tag = r.view.tag;
    
    contestPlayerView  *pv = [players objectAtIndex:tag];
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    ScoringBreakdownController * vc = [storyboard instantiateViewControllerWithIdentifier:@"scoringBreakdown"];
    vc.game = self.myGame;
    NSLog(@"%@ my fucking game",self.myGame);
    vc.pv = pv;
    [vc addGameTeam];
    [vc getContestStats:pv:self.myGame:r.view.restorationIdentifier];
    [self presentViewController:vc animated:YES completion:nil];
    
    
}


@end
