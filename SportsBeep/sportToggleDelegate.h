//
//  sportToggleDelegate.h
//  SportsBeep
//
//  Created by iOSDev on 5/21/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol sportToggleDelegate <NSObject>
-(void)sportWasSelected;
@end
