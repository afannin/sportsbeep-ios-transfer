//
//  PlayerScoringController.h
//  SportsBeep
//
//  Created by Daniel Nasello on 6/1/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "teamRectangleView.h"
@interface PlayerScoringController : UIViewController

-(void)addTopView:(teamRectangleView*)tr;
-(void)addPlayerGameStats:(NSDictionary *)dict;
@end
