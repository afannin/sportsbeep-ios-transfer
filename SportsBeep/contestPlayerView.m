//
//  contestPlayerView.m
//  SportsBeep
//
//  Created by Daniel Nasello on 5/30/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "contestPlayerView.h"

@implementation contestPlayerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    
        [self createSubViews];
        
        
    }
    return self;
}




-(void)createSubViews
{
    /*
    @property(nonatomic,strong)UIView *topView;
    @property(nonatomic,strong)UILabel *placeLabel;
    @property(nonatomic,strong)UILabel *userLabel;
    @property(nonatomic,strong)UILabel *totalLabel;
    @property(nonatomic,strong)UIImageView *icon;
    */
    
    
    [self setBackgroundColor:[UIColor blackColor]];
    
    self.topView = [[UIView alloc]initWithFrame:CGRectMake(50,0,self.frame.size.width-50,25)];
    [self.topView setBackgroundColor:[UIColor blueColor]];
    
    [self addSubview:self.topView];
    
    self.placeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.topView.frame.size.width, self.topView.frame.size.height)];
    [self.topView addSubview:self.placeLabel];
    
    
    self.totalLabel = [[UILabel alloc]initWithFrame:CGRectMake(50, 25, self.topView.frame.size.width, self.topView.frame.size.height)];
    
    
    [self addSubview:self.totalLabel];
    
    
    
    
    self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    [self.icon setBackgroundColor:[UIColor orangeColor]];
    [self addSubview:self.icon];

}

@end
