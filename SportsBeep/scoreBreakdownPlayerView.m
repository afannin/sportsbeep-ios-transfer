//
//  scoreBreakdownPlayerView.m
//  SportsBeep
//
//  Created by Daniel Nasello on 6/3/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "scoreBreakdownPlayerView.h"

@implementation scoreBreakdownPlayerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self createSubviews];
    }
    return self;
}




-(void)createSubviews
{
    //   @property(nonatomic,strong)UILabel *playerNumber;
    //   @property(nonatomic,strong)UILabel *playerTeamAbbr;
    //   @property(nonatomic,strong)UILabel *playerName;
    //   @property(nonatomic,strong)UILabel *playerStats;
    //  @property(nonatomic,strong)UILabel *playerPosition;
    
    
    
    self.playerNumber = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    [self.playerNumber setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:self.playerNumber];
    
    self.playerTeamAbbr = [[UILabel alloc]initWithFrame:CGRectMake(0, 30, 50, 20)];
    [self.playerTeamAbbr setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:self.playerTeamAbbr];
    
    
    
    
    
    UIView *leftBarrier = [[UIView alloc]initWithFrame:CGRectMake(49, 0, 1, 50)];
    [leftBarrier setBackgroundColor:[UIColor blackColor]];
    [self addSubview:leftBarrier];
    
    UIView *topBarrier = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 1)];
    [topBarrier setBackgroundColor:[UIColor blackColor]];
    [self addSubview:topBarrier];
    
    self.playerName = [[UILabel alloc]initWithFrame:CGRectMake(60, 0, self.frame.size.width -60, 30)];
    [self addSubview:self.playerName];
    
    
    self.playerPosition = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width-60, 0, 60, 30)];
    [self.playerPosition setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:self.playerPosition];
    
    self.playerPoints = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width-60, 25, 60, 30)];
    [self.playerPoints setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:self.playerPoints];
    
    

    
    self.playerIngame = [[UILabel alloc]initWithFrame:CGRectMake(59, 30, self.frame.size.width-60, 20)];
    [self addSubview:self.playerIngame];
    
    
}


-(void)setLabelColors:(NSString *)color
{
    
    [self.playerNumber setTextColor:[UIColor colorWithHexString:color]];
    [self.playerTeamAbbr setTextColor:[UIColor colorWithHexString:color]];
    [self.playerName setTextColor:[UIColor colorWithHexString:color]];
    [self.playerStats setTextColor:[UIColor colorWithHexString:color]];
    [self.playerPosition setTextColor:[UIColor colorWithHexString:color]];
    [self.playerPoints setTextColor:[UIColor whiteColor]];
    [self.playerIngame setTextColor:[UIColor whiteColor]];
}


-(void)setBG:(NSString *)color
{
    [self setBackgroundColor:[UIColor colorWithHexString:color]];
}


-(void)setStats:(NSArray *)stats
{
    
    NSMutableString *string = [[NSMutableString alloc]init];
    
    for(NSDictionary *dict in stats)
    {
        
        NSString *type = [dict objectForKey:@"abbr"];
        NSString *amount = [dict objectForKey:@"amount"];
        
        NSString *formattedString = [NSString stringWithFormat:@" %@ %@ ",type,amount];
        
        [string appendString:formattedString];
        
        
    }
    
    [self.playerStats setText:string];
    
    
    
    
}

@end
