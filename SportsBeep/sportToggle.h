//
//  sportToggle.h
//  SportsBeep
//
//  Created by iOSDev on 5/21/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sportTab.h"
#import "sportToggleDelegate.h"
@interface sportToggle : UIView{
    id<sportToggleDelegate>delegate;
}

@property(nonatomic,weak)id delegate;
@property(nonatomic,strong)NSMutableArray *tabArray;
@property(nonatomic,assign)NSInteger selectedIndex;
@property(nonatomic,strong)NSArray *sports;

//will give you the sports name
-(NSString *)getSelectedSports;
@end
