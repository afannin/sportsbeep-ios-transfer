//
//  ScoringBreakdownController.m
//  SportsBeep
//
//  Created by Daniel Nasello on 6/1/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "ScoringBreakdownController.h"
#import "PlayerScoringController.h"
@interface ScoringBreakdownController (){
    NSDictionary *teamDict;
    teamRectangleView *tr;
    NSMutableArray *arrayOfPlayers;
}

@end

@implementation ScoringBreakdownController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arrayOfPlayers = [[NSMutableArray alloc]init];
    
    self.linearLayout = [[CSLinearLayoutView alloc]initWithFrame:CGRectMake(0, 120, self.view.frame.size.width, self.view.frame.size.height-120)];
    [self.view addSubview:self.linearLayout];
    
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)addGameTeam{
   tr= [[teamRectangleView alloc]initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, 60)];
    [tr addGameRectanges:self.game.awayTeamObject :self.game.homeTeamObject :self.view.frame.size.width :self.game.gameTime :self.game.gameDay];
    [self.view addSubview:tr];
}


- (IBAction)doneButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)getContestStats:(contestPlayerView *)pv:(individualGame *)game:(NSString *)rid
{
    
    self.game = game;
    NSDictionary *hometeam = self.game.homeTeamObject;
    NSDictionary *awayteam = self.game.awayTeamObject;
    NSString *home_team_id = [NSString stringWithFormat:@"%@", [hometeam objectForKey:@"id"]];
    NSString *away_team_id =[NSString stringWithFormat:@"%@", [awayteam objectForKey:@"id"]];
    
    
    
    
    

    
    
    //dicts that hold the teams informatiopn
    teamDict = [NSDictionary dictionaryWithObjectsAndKeys:
                hometeam, home_team_id,
                awayteam,away_team_id,
                nil];
    
    
    
   self.pv = pv;
    
   NSLog(@"player dict %@",pv.playerDict);
    
    NSString *ticket_id = [pv.playerDict objectForKey:@"id"];
    
    NSDictionary *contest_dict = [pv.playerDict objectForKey:@"contest"];
    NSString *contest_id = [contest_dict objectForKey:@"id"];
    
    NSString *url =[NSString stringWithFormat:@"http://sportsbeep.com/api/mlb/tickets/%@/players",rid];
    
    NSLog(@"the fuyck %@",rid);
    
    NSDictionary *dict =@{
                          //  @"status":[NSString stringWithFormat:self.gameTimeType],
                          @"ticket_id":ticket_id,
                          @"player_id": contest_id
                          };
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"2" forHTTPHeaderField:@"X-API-Version"];
    [requestSerializer setValue:@"Token token=wgGZgmyUws5iGepQqWtB" forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer = requestSerializer;
    
    [manager GET:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        
        NSArray *players = [responseObject objectForKey:@"players"];
        
        int ind=0;
        for(NSDictionary *d in players)
        {
            //UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0,299, 50)];
            
            scoreBreakdownPlayerView *v = [[scoreBreakdownPlayerView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
            
            
            NSDictionary *team_dict = [d objectForKey:@"team"];
            NSString *team_id = [team_dict objectForKey:@"id"];
 
            NSDictionary *to  = [teamDict objectForKey:[NSString stringWithFormat:@"%@",team_id]];
            NSLog(@"should i %@",to);
            
            
            
            NSString *mainColor = [to objectForKey:@"primary_color"];
            NSString *altColor = [to objectForKey:@"secondary_color"];
            
            
            [v.playerNumber setText:[NSString stringWithFormat:@"%@",[d objectForKey:@"number"]]];
            [v.playerName setText:[d objectForKey:@"name"] ];
            [v.playerPosition setText:[d objectForKey:@"position"]];
            [v.playerTeamAbbr setText:[to objectForKey:@"abbr"]];
            [v.playerPoints setText:[d objectForKey:@"score"]];
            
            [v.playerIngame setText:@"IN GAME SCORING"];
            
            
            [v setBG:mainColor];
            [v setLabelColors:altColor];
            v.tag = ind;
            
          //  [self.playerViews addObject:v];
            
            UITapGestureRecognizer *singleFingerTap =
            [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(playerWasTapped:)];
            [v addGestureRecognizer:singleFingerTap];
            
            //[self.linearLayout setBackgroundColor:[UIColor redColor]];
            CSLinearLayoutItem *i = [CSLinearLayoutItem layoutItemForView:v];
            [self.linearLayout addItem:i];
            
            
            [arrayOfPlayers addObject:d];
            
            ind++;
        }

        
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString *errorMessage = operation.responseObject[@"error"];
        
        NSLog(@"err: %@", errorMessage);
        
    }];

}





-(void)playerWasTapped:(UIGestureRecognizer *)r
{
    
    NSInteger tag = r.view.tag;
    
    teamRectangleView *f = (teamRectangleView *)[tr snapshotViewAfterScreenUpdates:YES];
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    PlayerScoringController * vc = [storyboard instantiateViewControllerWithIdentifier:@"playersc"];
    [vc addTopView:f];
    [vc addPlayerGameStats:[arrayOfPlayers objectAtIndex:tag]];
    [self presentViewController:vc animated:YES completion:nil];

    
    
}

@end
