//
//  liveContestsViewController.m
//  SportsBeep
//
//  Created by iOSDev on 5/22/15.
//  Copyright (c) 2015 iOSDev. All rights reserved.
//

#import "liveContestsViewController.h"
#import "individualGame.h"
#import "specificContestViewController.h"

@interface liveContestsViewController (){
    getMyContests *getContests;
    NSMutableArray *contestsHolder;
    CGFloat cellWidth;
}

@end

@implementation liveContestsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    
    contestsHolder = [[NSMutableArray alloc]init];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"contestsCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    
    self.tableView.separatorColor = [UIColor clearColor];
    cellWidth = self.view.frame.size.width;
    
    [self getLiveContests];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)getLiveContests
{
    NSString *locStatus = self.gameStatus;
    getContests = [[getMyContests alloc]init];
    [getContests setDelegate:self];
    [getContests getContests:locStatus];

    
}

//get contests protocol methods

-(void)getContestsHasStarted
{
    //progress spinner
}
-(void)getContestsDidSucceed:(NSDictionary*)result
{
    //end spinner
    
  
    
    NSArray *contests = [result objectForKey:@"contests"];
    
    for(NSDictionary *dict in contests)
    {
    
        individualGame   * g = [[individualGame alloc]init];
   
        
        NSDictionary *game = [dict objectForKey:@"game"];
        
        g.homeTeamObject = [game objectForKey:@"home_team"];
        g.awayTeamObject = [game objectForKey:@"away_team"];
        

        g.pool = [game objectForKey:@"pool"];
        g.ticket = [game objectForKey:@"ticket"];
        g.date = [game objectForKey:@"start_time"];
        g.gameId = [game objectForKey:@"id"];
        g.contestID = [dict objectForKey:@"id"];
        
        NSString *myDate = [game objectForKey:@"start_time"];
        
        NSDateFormatter *dateFormatter  =   [[NSDateFormatter alloc]init];
        
        NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [dateFormatter setTimeZone:gmt];
        
        
        [dateFormatter setDateFormat:@"yyyy-M-DD'T'HH:mm:SSSS'Z'"];
        
        NSDate *yourDate   =  [dateFormatter dateFromString:myDate];
        
        if(yourDate != nil)
        {
            [g changeDateToRightDate:yourDate];
            
        }
        
        [contestsHolder addObject:g];
    
        
        
        
    }
    
    
    [self.tableView reloadData];
    
}
-(void)getContestsDidFail:(NSString *)error
{
    //end spinner
}



//end




//tbale view delegatges

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [contestsHolder count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    

    individualGame  * g = [contestsHolder objectAtIndex:indexPath.row];


    NSString *CellIdentifier = @"cell";

    contestsCell *cell =  (contestsCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
         cell = [[contestsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
     
    
    }
    else
    {
      //  [cell changeTriangleColors:g.awayTeamObject :g.homeTeamObject :g.gameDay :g.gameTime];
    }
    
       [cell addGameTriangles:g.awayTeamObject :g.homeTeamObject:cellWidth:g.gameDay:g.gameTime];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 190;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    individualGame *g = [contestsHolder objectAtIndex:indexPath.row];
    [self.delegate contestWasSelected:g];
    
}


@end
